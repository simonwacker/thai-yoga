`npm start`

localhost:1313

# Links

- Sanskrit https://www.learnsanskrit.cc/
- HTML Symbols https://www.toptal.com/designers/htmlarrows/
- Hugo https://gohugo.io/
- Tailwind https://tailwindcss.com/docs/installation
- Themes
  - https://github.com/zerostaticthemes/hugo-serif-theme
  - https://github.com/wowthemesnet/jekyll-theme-memoirs
  - https://github.com/alshedivat/al-folio
  - https://github.com/startbootstrap/startbootstrap-creative
- Metadata validators
  - https://developers.google.com/search/docs/advanced/structured-data
  - https://validator.schema.org/#url=https%3A%2F%2Fsimonwacker.gitlab.io%2Fthai-yoga%2F
  - https://search.google.com/test/rich-results/result
  - https://metatags.io/
  - https://www.opengraph.xyz/url/https%3A%2F%2Fsimonwacker.gitlab.io%2Fthai-yoga/
- Google Search Console: https://search.google.com/search-console?resource_id=sc-domain%3Axn--einsprsam-u9a.de
- Google Business Profile: https://www.google.de/search?q=einsp%C3%BCrsam&sxsrf=ALiCzsapP5YHol5mIN9uxqGT79OKHrGtAw%3A1661149430856&ei=9iADY_ncM9Ph7_UP2tuSiAg&oq=ein&gs_lcp=Cgdnd3Mtd2l6EAMYADIECCMQJzIECCMQJzIECCMQJzIFCAAQkQIyBQguEJECMgUIABCABDIFCAAQgAQyBQguEIAEMgUILhCABDIFCAAQgAQ6CwguEIAEEMcBENEDOgsILhCABBDHARCvAUoECEEYAUoECEYYAFCFA1iUBWDDDWgCcAB4AIAB7QKIAYAGkgEHMC4xLjEuMZgBAKABAbgBA8ABAQ&sclient=gws-wiz
- Yandex Webmaster (for DuckDuckGo): https://webmaster.yandex.com/
- Bing Webmaster (for Ecosia): https://www.bing.com/webmasters
- Qwant: https://help.qwant.com/en/docs/qwant-search/survey-monkey/how-to-get-my-website-listed-on-qwant/

# Todos

- Calendly für Online-Buchungen: https://calendly.com oder Acuity:Scheduling: https://www.acuityscheduling.com/
- Werbung und Konzept
- Massagen in Ebringen bei Anne?
- Raum in Freiburg ueber Vroni?
- Angebote:
  - Behandlung: Wellness Thai-Yoga-Massage in Stille
  - Übungsbehandlung: Traumasensitive Thai-Yoga-Massage im verbalen Kontakt
      (Holistic Bodywork)
  - Abendkurs: Atemarbeit
  - Workshop: Thai-Massage
  - Selbstermächtigung durch Selbstberührung
  - Inverted Massage: Du berührst mich, bist unter Kontrolle, gibst das Tempo
      vor, die Qualität vor, lernst Berührung im Sinne von Thai-Yoga-Massage.
      Einzelsitzung/Paarsitzung.
  - körperorientierte/-zentrierte Erkenntnisarbeit/-begleitung. Prozessarbeit.
      Selbst-Erkenntnis/-Erforschung/-Entwicklung. Forschungsreise. Thema in
      Körper holen und von dort erforschen.
    Somatische/körperorientierte Processbegleitung, Wandelprozesse,
      Transformationsprozesse,
      Selbstentfaltung/-erkenntnis/-ermächtigung/-erfahrung, Wandelraum,
      Spürwandel, Körper-/Selbstgewahrsein
- soziale Ebene einweben
- Logo finden und als Favicon verwenden (Idee: stilisierte Hand)
- Zitate auf Startseite einpflegen
- Bessere Darstellung von "Erfahre mehr ueber ..."-Links auf Startseite
- Von Text
  https://www.myphysios.co.za/treatments/cranio-sacral-therapy/#:~:text=Very%20simply%20%E2%80%93%20the%20Cranio%20Sacral,all%20aspects%20of%20our%20lives.
  ueber cranio-sacrale Therapie inspirieren lassen.
- Von Text zum Behandlungsablauf unter https://www.terramassagen.ch/
  inspirieren lassen.
- Rezensionen
- Elementberuehrung: ThaiYoga-Aspekte zu denen vom Tantra gesellen
    (Elementberührung wurde von David und Takis in ThaiYoga integriert)
- Weitere Einfluesse: Gestalttherapie (Annina Engelhardt), Possibility Management (Markus Bork, Oliver Arnold, Eva Daubert)
- Powered by GitLab and BioHost
- GLS Bank

# Notes

- Favicon: SVG generated on https://formito.com/tools/favicon with font `Noto Sans` from the letter मु and PNGs generated on
  https://favicon.io/favicon-generator/ from the letters दि (these two letters
  are not overlaid properly for SVG generation)
- Vermeiden Sie die Begriffe „Diagnose“ oder „Behandlung“, geben Sie keine
  Heilversprechen ab. Vermeiden Sie die Bezeichnung „Massagepraxis“, verwenden
  können Sie etwa „Entspannungsmassage“ oder „Massagepraktikerin“. Die
  „ganzheitliche Körpertherapeutin“ erscheint zulässig, ist aber wohl
  grenzwertig, ähnlich wie „Massagetherapeutin“. Rechtlich nicht geschützt wäre
  etwa auch „Entspannungspädagogin“. Sehen Sie auch den Deutschen Wellness
  Verband e.V.
