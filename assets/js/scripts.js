const toggleCollapse = (elementId, show = true) => {
  const collapseElement = document.getElementById(elementId);
  if (show) {
    collapseElement.classList.remove("hidden");
  } else {
    collapseElement.classList.add("hidden");
  }
};

document.addEventListener("DOMContentLoaded", () => {
  document
    .querySelectorAll("[data-collapse-toggle]")
    .forEach(function (collapseToggleElement) {
      var collapseId = collapseToggleElement.getAttribute(
        "data-collapse-toggle"
      );
      collapseToggleElement.addEventListener("click", function () {
        toggleCollapse(
          collapseId,
          document.getElementById(collapseId).classList.contains("hidden")
        );
      });
    });
});
