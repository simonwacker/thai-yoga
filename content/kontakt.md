---
title: Kontakt
images:
  - images/portrait/landing.jpg
description: >
  Gehe unverblümt mit mir in Kontakt, ich freue mich auf dich und darauf dein
  Leben bereichern zu dürfen.
summary: |
  Gehe unverblümt mit mir in Kontakt, ich freue mich auf dich und darauf dein
  Leben bereichern zu dürfen.
address: |
  Simon Wacker  
  Breitenweg 11  
  79194 Gundelfingen (Breisgau)  
  Deutschland
phone: +49 174 2929943
email: simon@einspürsam.de
note: |
  **Ich schließe meine Praxis auf unbestimmte Zeit.**
quote: |
  > Man muss den Dingen die eigene, stille ungestörte Entwicklung lassen, die
  > tief von innen kommt und durch nichts gedrängt oder beschleunigt werden kann,
  > alles ist austragen -- und dann gebären ...
  >
  > Reifen wie der Baum, der seine Säfte nicht drängt und getrost in den Stürmen
  > des Frühlings steht, ohne Angst, dass dahinter kein Sommer kommen könnte.
  >
  > Er kommt doch! Aber er kommt nur zu den Geduldigen, die da sind, als ob die
  > Ewigkeit vor ihnen läge, so sorglos, still und weit ...
  >
  > Man muss Geduld haben. Mit dem Ungelösten im Herzen, und versuchen, die Fragen
  > selber lieb zu haben, wie verschlossene Stuben, und wie Bücher, die in einer
  > sehr fremden Sprache geschrieben sind.
  >
  > Es handelt sich darum, alles zu leben. Wenn man die Fragen lebt, lebt man
  > vielleicht allmählich, ohne es zu merken, eines fremden Tages in die Antworten
  > hinein.

  --- Rainer Maria Rilke (1875-1926)
date: 2022-02-10T13:05:52+0100
layout: contact
---
