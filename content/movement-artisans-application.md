---
title: Dance as a Research Practice
layout: single
---

Movement Background: I enjoy living in my body --- a lot. I juggled and manipulated objects for 24 years starting at the age of 10 and once in a while did some circus artistics, in my youth I did classical gymnastics, in my early adulthood I climbed creatively, I have had a solid yoga and animal/primal movement practice for the last 10 years and am a Hatha yoga teacher for about 3 years now, I practice Feldenkrais for about 2 years and intensely dance contact improvisation for about 1 year, I'm learning and practicing Thai-Yoga and OsteoThai massage for about 4 years now, I enjoy moving in general like walking, hiking, cycling, swimming, running and am always curious to discover new styles, either by playing myself or attending some new workshop like Gaga, Contemporary Dance, Laban/Bartenieff, slacklining and whatever catches my eye. I had two knee operations about 7 years ago and am nowadays very aware about moving in a sustainable way and I did lose some flexibility in the recovery phase that I haven't fully recovered yet and that I'm sometimes ashamed about.

Mind Oriented Interests: Meditating, listening to nature, healing processes in general and specific modalities like Holistic Bodywork, Gestalt, Somatic Experiencing, and NeuroAffective Relational Model, and for one decade research mathematics.

Trajectory: I'm in a 7 year process of leaving academia, currently working in research for 10 hours a week, during which I became a Yoga teacher, a Thai massage therapist, a Holistic Bodywork practitioner, fell in love with contact improvisation, am beginning a CI teacher training in September with Ecki, Dani and Meli, and I'd like to attend the basic programme of bewegungsART freiburg (formerly TIP).

Expectations and curiosities: Getting to know to my body and expanding its potential in a sustainable manner and bringing the joy of embodiment and movement into the world.
