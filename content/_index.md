---
title: einspürsam · Thai-Yoga-Massage von Simon
images:
  - images/landscape/body-wave-3.jpg
slideshow:
  - title: Körperwelle
    description: Körperwelle
    path: images/landscape/body-wave-3.jpg
    anchor: koerperwelle
  - title: Rückbeuge
    description: Rückbeuge
    path: images/landscape/backbend-2.jpg
    anchor: rueckbeuge
  - title: Bauchwelle
    description: Bauchwelle
    path: images/landscape/belly-wave-2.jpg
    anchor: bauchwelle
  - title: Busfahrer:in
    description: Busfahrer:in
    path: images/landscape/bus-driver.jpg
    anchor: busfahrer
  - title: Zentrieren
    description: Zentrieren
    path: images/landscape/centering.jpg
    anchor: zentrieren
  - title: Kopf umhüllen
    description: Kopf umhüllen
    path: images/landscape/head-envelope.jpg
    anchor: kopf-umhuellen
  - title: Kopfnest
    description: Kopfnest
    path: images/landscape/head-nest.jpg
    anchor: kopfnest
  - title: Nackendehnung
    description: Nackendehnung
    path: images/landscape/neck-stretch.jpg
    anchor: nackendehnung
  - title: Schultergürtel
    description: Schultergürtel
    path: images/landscape/shoulder-girdle.jpg
    anchor: schulterguertel
  - title: Seitdehnung
    description: Seitdehnung
    path: images/landscape/side-stretch-2.jpg
    anchor: seitdehnung-2
  - title: Seitdehnung
    description: Seitdehnung
    path: images/landscape/side-stretch.jpg
    anchor: seitdehnung
  - title: Taxifahrer:in
    description: Taxifahrer:in
    path: images/landscape/taxi-driver-2.jpg
    anchor: taxifahrer-2
  - title: Rumpfdehnung
    description: Rumpfdehnung
    path: images/landscape/torso-stretch.jpg
    anchor: rumpfdehnung
photographies:
  - title: Armtorero
    description: Armtorero
    path: images/portrait/arm-torero.jpg
    anchor: armtorero
  - title: Torero
    description: Torero
    path: images/portrait/torero.jpg
    anchor: torero
  - title: Wiegen
    description: Wiegen
    path: images/portrait/baby.jpg
    anchor: wiegen
  - title: Bauchwelle
    description: Bauchwelle
    path: images/portrait/belly-wave.jpg
    anchor: bauchwelle
  - title: Körperwelle
    description: Körperwelle
    path: images/portrait/body-wave-2.jpg
    anchor: koerperwelle-2
  - title: Körperwelle
    description: Körperwelle
    path: images/portrait/body-wave.jpg
    anchor: koerperwelle
  - title: Busfahrer:in
    description: Busfahrer:in
    path: images/portrait/bus-driver-2.jpg
    anchor: busfahrer-2
  - title: Kobra
    description: Kobra
    path: images/portrait/cobra.jpg
    anchor: kobra
  - title: Ankommen
    description: Ankommen
    path: images/portrait/landing.jpg
    anchor: ankommen
  - title: Beckenwelle
    description: Beckenwelle
    path: images/portrait/pelvis-wave.jpg
    anchor: beckenwelle
  - title: Hüftdehnung
    description: Hüftdehnung
    path: images/portrait/twist.jpg
    anchor: hüftdehnung
  - title: Wasserpumpe
    description: Wasserpumpe
    path: images/portrait/water-pump.jpg
    anchor: wasserpumpe
date: 2022-02-10T11:05:52+0100
layout: index
---
