---
title: Sitzung
images:
  - images/portrait/belly-wave.jpg
description: >
  In einer Thai-Yoga-Massage Sitzung bin ich vollkommen für dich da, berühre
  ich dich in voller Präsenz, Achtsamkeit und mit klarer Absicht aus dem Herzen
  voll liebender Güte, Mitgefühl, Mitfreude, Gleichmut und Dankbarkeit dafür
  dir diese Erfahrung schenken zu dürfen.
summary: Auf Anfrage, für eine bis zwei Stunden, bei mir oder dir, mit Energieausgleich deiner Wahl
quote: |
  > Entdecke Dein  
  > wahres Wesen.  
  > Es ruht.  
  > Wie ein Schatz.  
  > In tiefer Stille.  
  > Verborgen.  
  > In Dir.

  --- Vom buddhistischen Kloster [Buddhas Weg](https://www.buddhasweg.eu)
course_of_events: |
  Wir vereinbaren einen stimmigen Termin. Je näher dieser rückt um so besser
  sorgst du für dich und isst zwei Stunden vor der Massage höchstens eine
  Kleinigkeit. Zum Termin radle ich zu dir oder du kommst zu mir. Wir gehen in
  einen ruhigen Wohlfühlraum. In dessen Mitte liegt eine zwei auf zwei Meter
  große mittelweiche Unterlage, eine dünne Decke und etliche Kissen zum
  Unterfüttern.

  Wir führen ein kurzes Vorgespräch über deine momentane Befindlichkeit. Du
  legst dich auf den Rücken, ich knie mich an das Fußende, mache mich
  aufnahmebereit, gehe in die erste Berührung, massiere dich intuitiv
  liebevoll, einfühlsam, mitfreuend, wertfrei und hingebungsvoll, gehe aus dem
  Kontakt, bedanke mich in Stille für dein Vertrauen und gebe dir nach Wunsch
  etwa zwanzig Minuten Zeit zum Nachspüren.

  Falls du möchtest unterhalten wir uns über deine Erlebnisse, du gibst mir was
  du geben kannst und möchtest und wir verabschieden uns. Du gönnst dir Zeit
  für dich, trinkst hinreichend und integrierst das Erlebte in Körper, Geist
  und Seele.
boxes:
  - title: Energie&shy;ausgleich
    summary: In Form und Umfang deiner Wahl
    description: |
      Wir geben beide was wir geben können und möchten. Ich in Form einer
      Thai-Yoga-Massage und du in einer Form deiner Wahl. Beispielsweise in
      Form selbst&shy;hergestellter veganer regionaler saisionaler biologischer
      Lebensmittel, therapeutischer Betreuung jeglicher Art oder handwerklicher
      Hilfe. In Form von Geld ist ein Richtwert&shy;bereich für eine
      eineinhalb&shy;stündige Massage 60€ bis 120€ --- je nach persönlicher
      finanzieller Situation gerne mehr oder weniger.
    anchor: energieausgleich
  - title: Ort
    summary: An einem Wohlfühl&shy;ort bei dir, bei mir oder in der Natur
    description: |
      Bei mir oder bei dir zuhause, in einem sicheren, ruhigen und warmen Raum,
      auf einer mittelweichen etwa zwei auf zwei Meter großen Unterlage auf dem
      Boden, mit etlichen Kissen zur weichen Lagerung und einer Decke zum
      wärmenden Zudecken.
    anchor: ort
  - title: Termin
    summary: Individuell nach Vereinbarung zwischen einer und zwei Stunden
    description: |
      Gehe mit mir in [Kontakt](/kontakt) und wir suchen zeitnah einen
      stimmigen Termin für eine Sitzung zwischen einer und zwei Stunden.
      Erreichbar bin ich unter der E-Mail-Adresse
      [simon@einspürsam.de](mailto:simon@einspürsam.de). Ich freue mich über
      deine Anfrage und hoffe dein Leben bereichern zu können! [Vereinbare
      gleich einen Termin!](/kontakt)
    anchor: termin
date: 2022-02-10T13:05:52+0100
layout: treatment
---

[comment]: # (
Grober Ablauf
Du sorgst gut für dich und isst zwei Stunden vor der Massage höchstens eine Kleinigkeit
Ich radle zu dir oder du kommst zu mir
Wir führen ein kurzes Vorgespräch
Ich massiere dich liebevoll, einfühlsam, mitfreuend und wertfrei
Du gibst mir was du geben möchtest
Ich gehe und du integrierst das Erlebte
)

> Gefühle kommen und gehen wie Wolken am Himmel. Das achtsame Atmen ist mein
> Anker im Hier und Jetzt.

--- Aus dem Englischen nach [Thích Nhất
Hạnh](https://plumvillage.org/about/thich-nhat-hanh/) (1926-heute)

## Detaillierter Ablauf

Du schläfst ausreichend, trinkst hinreichend, isst in den beiden Stunden vor
der Massage höchstens eine Kleinigkeit und hältst deinen Körper warm.

Ich radle zu dir, komme bei dir an und wir führen ein kurzes Vorgespräch über
deine Historie, deine momentane Befindlichkeit, deine Hoffnungen, Erwartungen
und Wünsche.

Du zeigst mir den sicheren Raum, wir bereiten eine mittelweiche Unterlage auf
dem Boden vor, legen Kissen zur Unterfütterung bereit und eine Decke zum
Zudecken. Du trägst warme Kleidung, in der du frei beweglich bist und legst
dich auf die vorbereitete Unterlage.

Ich knie mich an das Fußende, schließe die Augen, erde mich, komme bei mir und
im Moment an, mache mich leer und empfänglich, öffne mich, gehe in
energetischen Kontakt mit dir, in visuellen Kontakt und in den ersten
körperlichen Kontakt mit den Händen --- wartend von dir akzeptiert und
angenommen zu werden.

Mit rhythmischen Bewegungen wandere ich ein erstes Mal mit den Händen deinen
Körper auf und ab, achtsam, annehmend, einfühlsam, interessiert, lernend und
verbindend. Aus tiefstem Herzen lausche ich durch meine Hände, höre zu mit
meinem ganzen Wesen und gehe in Resonanz mit dir.

Zurück an den Füßen beginnt ein zweiter Zyklus. Basierend auf dem Erfahrenen
behandle ich präzise lokal spezifische Körperräume und integriere eintretende
Änderungen global. Mal ist die Berührung dynamisch, mal statisch, mal tief und
fest, mal oberflächlich und sanft, mal aktivierend, mal beruhigend, mal
pulsierend, mal still, mal längend, mal komprimierend, mal verdrehend, mal
einsinkend, mal herausschnellend, immer liebevoll, mitfühlend, mitfreuend und
wertfrei.

Durch Präsenz, Achtsamkeit, Intention und liebevolle Berührung, berühre ich
dich auf der physischen, mentalen, emotionalen und spirituellen Ebene, wandert
dein Bewusstsein in deinen Körper, erwacht dein Herz zum Leben, kommen
Verarbeitungsprozesse in Fluss und gleicht sich dein Organismus aus --- es
entsteht ein Raum in dem Selbstheilung auf allen Ebenen möglich ist.

Die abschließende Berührung ist ruhig, warm und herzlich.

Ich gehe aus dem Kontakt, bedanke mich in Stille für dein Vertrauen und mein
Glück diese Berufung gefunden zu haben, gebe dir Zeit und Raum zur Integration.
Nach einer im Vorgespräch vereinbarten Zeit gebe ich dir Bescheid.

Falls du möchtest unterhalten wir uns über deine Erlebnisse, du gibst mir was
du geben kannst und möchtest und wir verabschieden uns.

Du gönnst dir Zeit für dich, trinkst hinreichend und integrierst das Erlebte in
Körper, Geist und Seele.

> Ich merkte, dass ich immer weniger zu sagen wusste, bis ich schließlich still
> wurde und zuzuhören begann. In der Stille entdeckte ich die Stimme Gottes.

--- Aus dem dänischen von Søren Aabye Kierkegaard (1813-1855)
