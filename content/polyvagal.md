# Polyvagal-Theorie und Yoga

[VAGUSAKTIVIERUNG UND STRESSREAKTION AUS SICHT DER OSTEOPATHIE](https://www.osteopathie-liem.de/blog/vagusaktivierung/) (suche nach "Selbsthilfeansätze")

[An introduction to yoga therapy and polyvagal theory](https://yogatherapy.health/2022/01/27/an-introduction-to-yoga-therapy-and-polyvagal-theory/)

[Applied Polyvagal Theory in Yoga](https://drarielleschwartz.com/applied-polyvagal-theory-in-yoga/)

[What is polyvagal theory and how can we apply it to meditation?](https://www.ekhartyoga.com/articles/practice/what-is-polyvagal-theory-and-how-can-we-apply-it-to-meditation)

[Yoga Therapy and Polyvagal Theory: The Convergence of Traditional Wisdom and Contemporary Neuroscience for Self-Regulation and Resilience](https://www.frontiersin.org/articles/10.3389/fnhum.2018.00067/full)

## Selbsthilfeansätze

[VAGUSAKTIVIERUNG UND STRESSREAKTION AUS SICHT DER OSTEOPATHIE](https://www.osteopathie-liem.de/blog/vagusaktivierung/) (suche nach "Selbsthilfeansätze")

https://www.sciencedirect.com/science/article/pii/S1615907121001118

„Vagusaktivierung und Stressreaktion aus Sicht der Osteopathie” von Torsten
Liem, Osteopathische Medizin, Volumen 22, Ausgabe 4, Dezember 2021, Seiten
10-15

Etliche Yogapraktiken stimulieren den Vagusnerv, das heißt sie wirken vagal
aktivierend und erhöhen den vagalen Tonus. Dadurch mindern diese Praktiken
Stressreaktionen. Im Allgemeinen werden Stressreaktionen vom zentrale
Höhlengrau reguliert. Das Neurovegetativum, von welchem der Vagusnerv ein Teil
ist, wirkt ebenso wesentlich regulierend.

Die Vagusaktivität kann zu niedrig und zu hoch sein.

- Yoga im Allgemeinen soll vagal aktivierend wirken.
- Atemtechniken (Pranayama) und körperliche Reinigungstechniken (Kriya)
  wie tiefe und verlangsamte Atmung, Ujjayi Atmung, Bhastrika und Sudarshan
  Kriya sollen parasympathisch aktivierend wirken.
- Meditation wie „Liebende Güte Meditation” erhöht den vagalen Tonus, da sie
  durch eine bewusste Wahrnehmung sozialer Beziehungen positive Emotionen
  steigert.
- Partner:innenübungen und therapeutisches Yoga, die/das Berührung beinhalten,
  verbessern/t die parasympathische Funktion.
- Mantrasingen (Kirtan) erhöht die Herzratenvariabilität was von vagaler
  Aktivierung kommen kann.
- Chanten des Mantras „Om“ wirkt vagal aktivierend, möglicherweise durch
  Stimulation der aurikulären Äste des Vagusnervs.
- Lachyoga hebt die Stimmung und erhöht die Herzratenvariabilität.
- Körperliche Bewegung (Asanas und Übergänge) stimulieren die Magenentleerung
  was von vagaler Aktivierung kommen kann.

Vagusdysfunktionen

Erniedrigte Vagusaktivität

- Erniedrigte Vagusaktivität kommt z.B. vor bei Autoimmunerkrankungen wie Colitis ulcerosa, bei erniedrigter Immunität, Malabsorption und Übergewicht.
- Eine hypotone vagale Aktivität mit einer normotonen Aktivität des enterischen Nervensystems kann bei Alkoholabusus und Diabetes mellitus Typ 2 auftreten. Neben Alkoholkarenz kann hier therapeutisch der N. vagus stimuliert werden (s.u.).
- Eine hypotone vagale Aktivität mit einer hypotonen Aktivität des enterischen Nervensystems birgt ein hohes Risiko für neurodegenerative Erkrankungsbilder. Hier sollte zunächst die niedrige Immunlage verbessert und die Viruslast vermindert [59], [60] und erst anschließend der N. vagus stimuliert werden. Zudem kann die Dünndarmregion gedehnt werden, um die myenterische Aktivität zu steigern [59], [60].

Erhöhte Vagusaktivität

Die Vagusaktivität kann dysfunktionell nicht nur erniedrigt, sondern auch erhöht sein.

- Erhöhte Vagusaktivität kommt z.B. vor bei Allergien, M. Crohn und Fettleibigkeit.
- Bei einer hypertonen Vagusaktivität können mittels OMT das Zwerchfell, die Radix mesenterii sowie die hochzervikale Region gedehnt werden.
- Erhöhte Vagusaktivität bei jungen Menschen kann zu einem Anstieg der Magensäureproduktion, verstärkter Magenentleerung und unter Umständen zu Durchfall führen. Hier sollte zusätzlich zu den manuellen Ansätzen die Mundregion gereinigt und die Pathogenbelastung reduziert werden.

Das zentrale Höhlengrau koordiniert Verhaltenszustände wie Kampf, Flucht und
Erstarrung -- mit den dazugehörigen motorischen, vegetativen und endokrinen
Auswirkungen. Dabei werden vagale Zuflüsse durch den Nucleus tractus solitarii
zum zentrale Höhlengrau, Hypothalamus (wichtigste Steuerzentrum des vegetativen
Nervensystems, wichtigste Hirnregion für die Aufrechterhaltung des inneren
Milieus (Synonym: Homöostase)), Amygdala (an der Furchtkonditionierung
beteiligt, spielt eine wichtige Rolle bei der emotionalen Bewertung und
Wiedererkennung von Situationen und der Analyse möglicher Gefahren) und zum
insulären, zingulären und präfrontalen Kortex weitergeleitet, wo sie in
emotionale und kognitive Prozesse integriert werden.

Das vegetative Nervensystem (autonomes Nervensystem) regelt die Abläufe im
Körper, die man nicht mit dem Willen steuern kann. Es ist ständig aktiv und
reguliert beispielsweise Atmung, Herzschlag und Stoffwechsel. Hierzu empfängt
es Signale aus dem Gehirn und sendet sie an den Körper.

Subdiaphragmale vagale Zuflüsse scheinen die angeborene Angst, erlernte Furcht
und andere Verhaltensweisen zu beeinflussen. Zudem modulieren vagale Zuflüsse
auf verschiedene Weisen spinale schmerzempfindliche Prozesse.

Das autonome Nervensystem regelt die wichtigsten Körperunktionen.
Beispielsweise mittels Erstarrung und Herunterfahren des Stoffwechsels bei
Gefahr. Dieses Verhalten, das notfalls über den Belangen der
Stoffwechselfunktionen steht, regelt der Parasympathikus.

Der Sympathikus entwickelte sich in Zusammenhang mit Flucht- und
Kampfverhalten. Dies äußert sich in Pupillenweitstellung (bessere
Dämmerungssicht und schärfere Sehfeldrandbeobachtung), Erweiterung der
Extremitäten- und Lungenblutgefäße (nötig für Flucht- und Kampfverhalten) und
Anstieg von Stresshormonen für schnellere Reaktionen und Glukosebereitstellung.
Auch hier steht der Gesamtorganismus im Fokus und nicht das Funktionieren
einzelner Organe.

Dabei wirken Parasympathikus und Sympathikus nicht zwangsläufig antagonistisch.
Unmyelinisierte Fasern, hauptsächlich vom Ncl. dorsalis nervi vagi ausgehend,
regeln die Durchblutung und Aktivität der Bauchorgane, während myelinisierte
Fasern, ausgehend vom Ncl. ambiguus, die Brustkorborgane Herz und Lunge sowie
die Sprache (Nn. laryngei superior und recurrens) und das Hören menschlicher
Sprache (unter anderen N. stapedius nach Verschaltung mit dem N. facialis) regulieren.
Auch vagale Einflüsse auf die Herzratenvariabilität, die
Blutzuckerkontrolle und das Immunsystem sowie Stimmlage, Appetit und
Bronchienfunktion bestehen. Der Vagusnerv wirkt als Bindeglied zwischen dem
peripheren autonomen Nervensystem und dem Gehirn. Er begünstigt auch die
Speicherung von Erinnerungen. Vagusnervstimulation erzielt hirnplastizitäts-
und gedächtnisfördernde Effekte.

Zuflüsse bestehen insbesondere vom Darm und anderen Bauchorganen. So
beeinflussen Zuflussfasern des Vagusnervs metabolisch die Mikroglia des
Gehirns.

Pathophysiologisch ist der Vagus beispielsweise bei Kopfschmerzen, Depressionen
und posttraumatischer Belastungsstörung von Bedeutung. Darüber hinaus hat der
Vagusnerv aber auch starken Einfluss auf das Immunsystem, die
Herzratenvariabilität, die Blutzuckerkontrolle, das Entstehen und die
Modulation von Kopfschmerzen, unsere Stimmlage, die Funktion unserer Bronchien,
die Appetitkontrolle, das Entstehen von Depressionen und vieles mehr.

## https://www.osteopathie-liem.de/blog/kritik-an-der-polyvagaltheorie/

Der Vagusnerv ist sicherlich ein wichtiger Faktor im System des sozialen Engagements (in das der N. hypoglossus einbezogen werden sollte), aber er ist nicht der Koordinator. Vielmehr koordiniert das mesenzephale periaquäduktale Grau zusammen mit dem limbischen System und neuronalen Hirnstammnetzwerken Verhaltenszustände wie Kampf und Flucht sowie Freezing – mit den dazugehörigen motorischen, vegetativen und endokrinen Auswirkungen. Letztlich wirken zahlreiche andere Hirnbereiche, wenn nicht gar das gesamte Gehirn als System des sozialen Engagements.

Der Begriff „polyvagal“ bezieht sich auf 2 vagale Schaltkreise. Das eine ist das phylogenetisch ältere unmyelinisierte System, das durch den motorischen Nucleus dorsalis n. vagi repräsentiert wird, der hauptsächlich subdiaphragmale Organe (v.a. den Gastrointestinaltrakt), aber auch das Herz innerviert und mit Immobilisierung und Dissoziation verbunden ist. Evolutionär jünger soll sich laut Porges ein 2. jüngerer vagaler Pfad entwickelt haben, der nur bei Säugetieren, nicht aber bei Reptilien beobachtet wurde und die Fähigkeit besitzt, Immobilisations- sowie Kampf- und Flucht-Verhalten herunterzuregulieren. Die anatomischen Strukturen dieser Komponente des Vagus interagieren laut Porges im Hirnstamm mit Strukturen, welche die quergestreiften Muskeln des Gesichts und des Kopfs innervieren, um ein integriertes System des sozialen Engagements zu schaffen [4]. Dieses jüngere System wird insbesondere durch den Nucleus ambiguus repräsentiert. In der PVT wird er mit den übrigen branchiomotorischen (speziell viszeroefferenten) Kernen des V., VII., IX. und XI. Hirnnervs als ventraler Vagalkomplex bezeichnet [5]. Dieses System reguliert über myelinisierte Nervenfasern Herz und Lunge, um Ruhezustände zu ermöglichen, und soll mit Sicherheits- und Sozialverhalten assoziiert sein [6].

Die PVT ordnet die Reaktionen auf wahrgenommene Risiken 3 Kategorien zu: sich sicher fühlen, in Gefahr sein oder eine Bedrohung für das Leben empfinden. Diese Kategorien folgen in der Phylogenese aufeinander. Sie stehen im Zusammenhang mit den adaptiven Verhaltensweisen der sozialen Kommunikation (Mimik, Sprechen, Zuhören), die vom Nucleus ambiguus kontrolliert werden sollen, sowie mit der Verteidigung im Sinne der Mobilisierung (Kampf, Flucht) und der Immobilisationsreaktion (vasovagale Synkope, Dissoziation, Immobilisations- bzw. Freezing-Zustand), die vom Nucleus dorsalis n. vagi kontrolliert werden [1][6][12][13][14].

Der Nucleus ambiguus der Säuger enthält neben den kardioinhibitorischen Neuronen v.a. allem die branchiomotorischen (speziell viszeroefferenten) Neurone für Larynx-, Pharynx- und quergestreifte Ösophagusmuskulatur [15], kontrolliert jedoch weder den Gesichtsausdruck (mimische Muskeln werden durch den N. facialis innerviert) noch das Hören über die Mittelohrmuskeln (M. tensor tympani, innerviert durch den motorischen Zweig des Trigeminusnervs, und M. stapedius, innerviert durch den N. facialis) noch andere Kopf- und Halsmuskeln, wie von der PVT vorgeschlagen. Auch beeinflusst der Nucleus facialis umgekehrt nicht den Nucleus ambiguus.

All diese motorischen Kerne, einschließlich des Hypoglossuskerns, werden durch prämotorische Netzwerke in der lateralen parvozellulären und intermediären retikulären Formation koordiniert [16][17][18][19][20]. Die zwischen dem medialen magnozellulären und dem lateralen parvozellulären Bereich gelegene intermediäre Formatio reticularis beherbergt darüber hinaus die neuronalen Netzwerke für die kardiovaskuläre Regulation (Kreislaufzentrum) und die zentralen Generatoren für Atemrhythmus (Prä-Bötzinger-Komplex, Atemzentrum) sowie für Schlucken und Erbrechen. Nucleus dorsalis n. vagi und Nucleus ambiguus sind in diese Netzwerke anatomisch und funktionell eingebettet, jedoch nicht als Koordinatoren, sondern als Output-Elemente. Vagale Afferenzen sind über den Solitariuskern (Nucleus tractus solitarii) nicht nur mit den motorischen Vaguskernen (Nucleus dorsalis n. vagi und Nucleus ambiguus) verbunden, sondern auch mit den prämotorischen Netzwerken der Formatio reticularis sowie dem Kreislauf- und Atemzentrum [20]. Ebenso wichtig für die Koordination der gesamten Kopf-Hals-Motorik sind aber auch trigeminale und obere zervikale spinale Afferenzen, die ebenfalls den prämotorischen retikulären Netzwerke zugeleitet werden.

Verhaltenszustände wie Kampf und Flucht, Immobilisations- bzw. Freezing-Zustand und Risikoabschätzung – mit den dazugehörigen motorischen, vegetativen und endokrinen Auswirkungen – werden durch das mesenzephale periaquäduktale Grau (PAG) koordiniert [22][23][24][25]. Das PAG ist mit dem Hypothalamus und dem limbischen System (v.a. der Amygdala und dem präfrontalen Kortex) [23][24] sowie mit verschiedenen prämotorischen und vegetativen Hirnstammkernen verbunden, welche die Atmung und das emotionale motorische System koordinieren [25]. Das PAG empfängt Afferenzen von fast allen sensorischen Systemen – nicht zuletzt vom nozizeptiven System – und moduliert deren Verarbeitung [24].

Zweifellos hat der Vagusnerv aufgrund seines großen afferenten Anteils einen signifikanten Einfluss auf Emotionen und verschiedene Verhaltenszustände. Vagale Afferenzen, die etwa 80% seiner Axone ausmachen, werden durch den Nucleus tractus solitarii zu PAG, Hypothalamus, Amygdala sowie zum insulären, zingulären und präfrontalen Kortex weitergeleitet, wo sie in emotionale und kognitive Prozesse integriert werden [26][27][28][29]. Neuere Studien deuten darauf hin, dass subdiaphragmale vagale Afferenzen die angeborene Angst, erlernte Furcht und andere Verhaltensweisen beeinflussen [30][31]. Darüber hinaus modulieren vagale Afferenzen in verschiedenen experimentellen Modellen spinale nozizeptive Prozesse [32][33].

Zwar wird bei Porges [34] die Darstellung von neuroanatomisch bereits lange bekannten Relationen des limbisches Systems und PAG mit den bidirektionalen Verbindungen zum Vaguskomplex erwähnt. Da aber nicht der ventrale Vaguskomplex, sondern das PAG im Verein mit limbischen und anderen Hirnstammnetzwerken als Koordinator für diese Verhaltenszustände verantwortlich ist und außerdem zahlreiche Hirnbereiche, wenn nicht das gesamte Gehirn als System des sozialen Engagements fungieren, erscheint der Begriff „polyvagal“ zu seiner Charakterisierung als eine irreführende Fehlbezeichnung.

Statt den Begriff des ventralen Vagalkomplexes auf alle branchiomotorischen Kerne auszudehnen, wäre es sinnvoller, ihnen ihre Eigenständigkeit zu belassen und ihre Koordination durch ein Netzwerk von Hirnstammneuronen zu betonen.

Das Konzept des Systems des sozialen Engagements ist plausibel und erscheint praxisrelevant. Zudem sollte ins Konzept des Systems des sozialen Engagements auch der N. hypoglossus, der zwar kein branchiomotorischer Nerv ist, doch die sozial wichtige Zungenmuskulatur innerviert, einbezogen werden.

Auch dem mesenzephalen Trigeminuskern und anderen sensorischen Trigeminuskernen kommen eine große Bedeutung für die Koordination der orofazialen Motorik zu. Der Vagusnerv ist efferent wie auch afferent sicher ein wichtiger Faktor im System des sozialen Engagements. Da der „neue“ Vagusnerv in Gestalt des Nucleus ambiguus jedoch keine koordinierende Funktion auf die anderen branchiomotorischen Kerne (V, VII, IX, XI) ausübt, auch wenn vagale Afferenzen über den Nucleus tractus solitarii in diese Koordinationsnetzwerke eingespeist werden, stellt die PVT die kausalen Zusammenhänge auf den Kopf.
