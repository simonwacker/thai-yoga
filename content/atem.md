---
title: Atem
summary: Heilsamer Atem
layout: single
table_of_contents: true
---

## Atembeobachtung

Worauf kannst du in einem Atemzyklus deine Aufmerksamkeit richten und worauf
über mehrere Atemzyklen hinweg? Was kannst du bereits wahrnehmen, wo kannst du
deine Wahrnehmung verfeinern und was kannst du noch wahrnehmen lernen? Was
interessiert dich, was macht dich neugierig, wohin zieht es dich? Wie kannst du
wertfrei beobachten, wie ohne Änderungswunsch und wie ohne zu verändern? Wie
ist es einfach mit deinem Atem zu sein, wie mit ihm zu strömen, wie mit ihm zu
verschmelzen, wie ihn zu durchdringen, wie ihn zu verkörpern und wie dein Atem
zu sein? Wie beeinflusst Atembeobachtung dein inneres Erleben?

- _Atemweg (Weg)_: Welchen Weg nimmt die ein- und welchen die ausströmende
  Luft? Wo strömt die Atemluft ein und wo aus (Ein-/Austrittsöffnung)? Welche
  Abschnitte des Weges nimmst du wahr? Wie sind die Bereiche ausgestaltet
  bezüglich Ausmaß, Auskleidung und Krümmung? Wie verändert sich die
  Ausgestaltung über einen Atemzyklus hinweg? Welche Funktionen erfüllen die
  Wegabschnitte? Wie kannst du dir Abschnitte des Atemwegs, die sich
  deiner bewussten Wahrnehmung entziehen, ins Bewusstsein holen? (Ideen:
  Geduld/Zeit/Langsamkeit, Berührung in nächster Nähe von außen mit Fingern
  oder Händen, Bewegung des Bereichs durch Haltungsänderung, Vibration des
  Bereichs durch Tönen/Summen/Brummen/Ozeanatmung, Stärke oder Geschwindigkeit
  des Atemflusses variieren durch Seufzen/Husten, Reibung des Bereichs durch
  Atemluft oder Schlucken)
- _Atemdauer (Zeit)_: Wie lange dauert ein vollständiger Atemzyklus? Wie lange
  atmest du ein, wie lange aus und wie lange sind die Atempausen im ein- und im
  ausgeatmeten Zustand? Wie unterscheidet sich in den einzelnen Phasen
  subjektives Zeitempfinden von objektiv gemessener Zeit?
- _Atemluftgeschwindigkeit (Weg pro Zeit)_: Wie langsam oder schnell strömt die
  Luft ein und aus? Wie variiert dies innerhalb einer Atemphase und wie über
  verschiedene Wegabschnitte? Wie ist der Zusammenhang zwischen
  Strömungsgeschwindigkeit und Lungenfüllung und wie zwischen
  Strömungsgeschwindigkeit und Beschaffenheit des Wegabschnitts? Wie
  geschmeidig/weich/sanft/gleichmäßig sind Geschwindigkeitsvariationen und
  Strömungsrichtungswechsel?
- _Atemzugvolumen (Tiefe)_: Wie viel Luft strömt in einem Atemzug ein und wie
  viel aus (Atemzugvolumen)? Wie viel Luft strömt in einer Zeitspanne wie
  einer Minute ein und wie viel aus (Atemzeitvolumen/Atemminutenvolumen)?
  Um welche mittlere Lungenfüllung pendelt deine Ein- und Ausatmung? Wie viel
  mehr als du auf natürliche Weise tust könntest du ein- und ausatmen
  (inspiratorisches/exspiratorisches Reservevolumen)? Wie groß ist dein
  Atemzugvolumen, wenn du vollständig ein- und ausatmest (Vitalkapazität)?
  Welche Füllung verbleibt stets (Residualvolumen)? Was machst du um
  absichtlich mehr ein- und auszuatmen und wie erlebst du dich dabei?
- _Atemfrequenz (Atemzuganzahl pro Zeit)_: Wie viel Atemzüge finden in einer
  Zeiteinheit wie einer Minute statt? Wie ist der Zusammenhang zum
  Atemvolumen? Was passiert bei großem Volumen und kleiner Frequenz, was
  bei kleinem Volumen und großer Frequenz und was wenn beide klein oder
  beide groß sind?
- _Atemluftqualitäten_: Welche Qualitäten hat die ein- und welche die ausströmende
  Luft? Wie variieren diese über verschiedene Wegabschnitte? Warum variieren
  diese? Wie spürst du deren Qualitäten? Welchen Einfluss haben verschiedene
  Qualitäten auf dein inneres Erleben? Welche Funktion erfüllt die Variation?
- _Verbundenheit_: Sind Ein- und Ausatmung verbunden oder liegt zwischen beiden
  eine Atemstille/-pause/-ruhe/-fasten? Wie erlebst du diese Atemstille nach
  der Einatmung und nach der Ausatmung? Ist vollständige muskuläre Entspannung
  im ausgeatmeten oder im eingeatmeten Zustand möglich? Was für
  Wünsche/Bedürfnisse tauchen am Ende der Atemstille auf? Ist da ein
  Atemhunger/Lufthunger? Wie sanft/weich/geschmeidig sind die Übergänge
  zwischen den Atemphasen?
- _Atemräume/Kammern/Strukturen/Form/Gewebe_: Welche Körperbereiche/-räume erreicht
  die Ein- und welche die Ausatmung? Welche Strukturen weiten sich und welche
  verengen sich? Welche Räume wölben/stülpen sich nach außen und welche nach
  innen? Welches Gewebe dehnt sich und welches zieht zusammen? Wo entsteht Raum
  und wo entsteht Enge? Wie stehen verschiedene Räume in Beziehung? Welche
  Drücke bauen sich in den Räumen auf und ab? Wie hängen die Drücke der Räume
  zusammen? Wie findet Druckausgleich statt? Was geschieht sichtbar im Außen
  (an der Peripherie, am Übergang zwischen dir und der Welt) und was verborgen
  im Innen (in dir, in deinem Körper, in deinem Geist) und wie hängen diese
  beiden zusammen? Welche Körperbereiche sind durch den Atem von innen
  ertastbar, komprimierbar oder dehnbar?
- _Atemwiderstand_: Wie klein oder groß ist dein Atemwiderstand bei der Ein- oder
  Ausatmung? Wie wandelt er sich über die Zeit? Woraus setzt er sich
  zusammen (elastischer und visköser Atemwiderstand)? Welcher Anteil muss
  wann überwunden werden und welche wirkt wann unterstützend? Wie variiert
  der Atemwiderstand über den Atemweg (Atemwegswiderstand/Strömungswiderstand)?
- _Körperhälften_: Was sind die Unterschiede zwischen den beiden Körperhälften?
  In welche strömt dein Atem leichter und in welche schwerer? In welche
  strömt weniger und in welche mehr? Strömt durch ein Nasenloch mehr oder
  weniger Luft als durch das andere?
- _Atemmuskulatur_: Welche Muskulatur ist bei der Ein- und welche bei der Ausatmung
  beteiligt und auf welche Weise, passiv oder aktiv, konzentrisch, isometrisch
  oder exzentrisch kontrahierend? Welche Muskulatur ist essentiell (Agonisten),
  welche unterstützend (Synergisten), welche hemmend (Antagonisten) und welche
  passiv/entspannt/locker und können deren Rollen wechseln? Wie ist die
  Muskulatur in welchen Atemphasen beteiligt und wie hängt ihre Beteiligung mit
  den ob genannten Eigenschaften zusammen?
- _Inneres Erleben_: Wie erlebst du dich gerade und wie hängt das mit der Art und
  Weise wie du ein- und ausatmest zusammen?

**TODO Aufteilung der Beobachtungsmöglichkeiten in: Anatomie (Mechanik),
Physiologie, Biochemie, Neurobiologie und Psychologie (Bewusstsein).**

Wie variiert dies alles über verschiedene (Lebens)umstände im Innen und Außen?
Welche Zusammenhänge kennst du aus deinem Leben? Welches Erfahrungswissen
bringst du mit? Was spielt eine kleine und was eine große Rolle?

- _Zeit_: Wie variiert dies alles über mehrere Atembewegungen hinweg, über den
  Tag und die Nacht, den weiblichen Zyklus, die Mondphasen, die Sonnenphasen, die
  Jahreszeiten und das Alter?
- _Raum_: Welcher Zusammenhang besteht zwischen deinem Atem und dem Raum im
  außen, beispielsweise geschlossen in einem kleinen Raum oder offen mit
  weitem Blick oder im Wald unter Blätterdach? Wie beeinflusst die Höhe über
  dem Meeresspiegel deine Atmung?
- _Muskulatur_: Wie beeinflussen muskuläre Spannungsmuster deine Atmung? Wie
  fließt dein Atem, wenn du vor der Einatmung deine Zwischenrippenmuskulatur
  anspannst oder deine Bauchwand gen Wirbelsäule ziehst oder beides
  gleichzeitig tust? Wie erlebst du dich von innen, wenn du in der Atemstille
  nach der Einatmung deine Muskelspannung so variierst, dass dein Bauchraum
  sich in alle Richtungen ausdehnt oder dein unterer Brustraum oder dein oberer
  Brustraum? Was erfährst du über dich, wenn du in der Atemstille nach der
  Einatmung innerhalb des Bauchraums Druck aufbaust --- indem du dein
  Zwerchfell beckenwärts ziehst, deine Bauchwand rückenwärts ziehst und deinen
  Beckenboden brustwärts ziehst --- und dann sanft ausatmest unter Erhöhung der
  Bauchspannung? Was geschieht, wenn du ohne einzuatmen in vollständig
  ausgeatmetem Zustand deinen Bauchraum groß und rund machst? Und was
  geschieht, wenn du dies in vollständig eingeatmetem Zustand tust und deine
  Lendenwirbelsäule dabei streckst, beugst, neigst, drehst oder spiralisierst?
- _Körperhaltung_: Wie beeinflussen Körperhaltungen deine Atmung? Wie atmest du
  im Liegen, im Sitzen, im Stehen, im Schulterstand, in Seitneigung oder in
  Beugung? Wie atmest du, wenn deine [acht
  Diaphragmen](https://ommeducation.com/2015/04/the-bodys-diaphragms/)
  übereinander ausgerichtet sind: Vereinfacht Fontanelle über Mundboden über
  Zwerchfell über Beckenboden über Knien über Fußsohlen.
- _Widerstände_: Wie beeinflussen Widerstände deine Atmung? Wie atmest du mit
  einem Gurt um den unteren Brustkorb oder mit einem Widerstand im Bauch- oder
  Rückenraum, beispielsweise in Rückenlage durch einen Sandsack auf deinem
  Bauch oder in Bauchlage durch ein Kissen unter deinem Bauch?
- _Bewegung_: Wie beeinflusst Bewegung deine Atmung? Wie kannst du deine
  Bewegung nutzen um Atem zu induzieren?
- _Blick_: Wie beeinflusst die Blickrichtung deine Atmung? Wie atmest du, wenn
  du nach unten, oben, hinten oder zur Seite blickst?
- _Stimme_: Wie beeinflusst der Stimmausdruck deine Atmung? Wie atmest du
  während du sprichst, schreist, summst, tönst oder singst?
- _Gefühle_: Wie beeinflussen Gefühle deine Atmung? Wie atmest du in Freude, in
  Angst, in Wut oder in Scham? Wie atmest du, wenn im Außen etwas
  unvorhergesehenes geschieht?
- _Gesellschaft_: Wie beeinflusst Gesellschaft deine Atmung? Wie atmest du in
  Ruhe mit einem Gegenüber, im Gespräch mit ihr/ihm oder während ihr euch
  umarmt oder küsst?

Was offenbart dir dein Atem über dich? Was teilt er dir mit? Wie kehrt er dein
Inneres nach Außen? Wie macht er dir deinen inneren Zustand bewusst? Wie können
dir diese Erkenntnisse im Alltag helfen? Und umgekehrt: Wie kannst du durch
absichtliches Atmen deinen inneren Zustand beeinflussen? Wie kannst du ihn dich
unterstützend einsetzen, ihn nutzbar machen, ihn zur Hilfe holen, ihn als
treuen Begleiter und Helfer zur Seite haben?

## Atemnutzung

_Wechselatmung_: Welche Wirkung hat die Wechselatmung auf dich, dein inneres
Erleben, deinen Seinszustand bei geringer, mäßiger und großer
Wiederholungszahl? Harmonisiert sie das vegetative Nervensystem? Gleicht sie es
aus? Welche Wirkung auf deine Gefühlswelt und Stimmung? Welche auf deinen
Bewusstseinszustand? Welche auf dein Sein? Wie ist das während und nach anderen
Atemübungen wie Feueratmung oder Brummatmung?

_Innenberührung_: Wie hilft dir dein Atem zur Erforschung deiner Innenwelt? Wie
kannst du deine Muskulatur von innen massieren, deine Sehnen und Bänder dehnen,
dein Bindegewebe anregen, deine inneren Organe aktivieren? Wie kannst du
Strukturen durch die Atmung einladen sich zu verändern? Beispielsweise die
Rippenbögen einladen sich aufzufächern und zusammenzuziehen?

_Stabilisierung_: Wie kannst du deinen Atem und deine Atemräume nutzen um deine
Lendenwirbelsäule zu stabilisieren? Wie baust du Druck in deinem Bauchraum auf?
Wie ziehst du deinen Beckenboden zusammen und kopfwärts, wie deine Bauchwand
rückenwärts und wie dein Zwerchfell steißwärts? Wie kannst du dabei entspannt
atmen? Wie kontrahierst du dein Zwerchfell wie in Zeitlupe exzentrisch bei der
Ausatmung? Wie verbinden sich dadurch Brustraum und Bauchraum? Wie umarmst du
bei jedem Ausatmen mit dem Brustkorb deinen Bauchraum und wie näherst du dabei
deine Rippenbögen deinem Becken an?

_Bewusstseinszustände_: Bewusstseinszustände sind Arten des bewussten Erlebens,
die sich durch die Merkmale Wahrnehmung, Selbstbewusstsein, Wachheit,
Handlungsfähigkeit und Intentionalität auszeichnen. Prominente Beispiele sind
Wachzustand, Hypnagogie (beim Übergang zum Wachsein oder in den Schlaf),
Schlafzustand, Traumzustand, Koma, Trance. Die vier Hauptbewusstseinsebenen des
Yoga sind Wachbewusstsein, Traumbewusstsein, Tiefschlafbewusstsein und
Überbewusstsein. In höheren Bewusstseinszuständen

- herrscht eine erweiterte Seinserfahrung (Sanskrit: सत् sat) wie
  Bewusstseinserweiterung, Verbundenheit und Liebe;
- gibt es ein höheres Wissen (Sanskrit: चित् chit) wie intuitives
  Verstehen von Sinn und Zweck des eigenen Lebens;
- erlebt mensch Glückseligkeit (Sanskrit: आनन्द ānanda) wie unendliche Freude
  verbunden mit Liebe.

Wie wandelt sich dein Atem beim Übergang von einem in einen anderen
Bewusstseinszustand? Wie atmest du in verschiedenen Bewusstseinszuständen? Wie
kannst du deinen Atem absichtlich nutzen um einen bestimmten
Bewusstseinszustand zu erreichen?

_Inneres Erleben_: Dein inneres Erleben ist das subjektive Empfinden deines
Seins, das heißt der Interaktion mit dir und der Umwelt aus der Perspektive
deiner Eigen-/Selbstwahrnehmung. Es besteht unter anderem aus und ist verwoben
mit

- Gefühlen, die das Erleben begleiten,
- Kognition, die innere Repräsentation des Erlebten,
- vegetative Nervensystemzuständen, die das Erleben filtern und modulieren,
- physiologischen Merkmalen/Begebenheiten, die einen Teil des inneren
  physischen Zustands bilden,
- Gehirnwellenfrequenzen, auf die geistige Aktivität überlagert ist.

Wie stehen inneres Erleben und Atem in Beziehung? Was wandelt sich beim
Aufkommen der Grundgefühle Freude, Angst, Wut, Trauer und Scham, beispielsweise
ausgelöst durch Aufkommen einer vermeintlichen Gefahr oder eines befriedigten
oder unbefriedigten Bedürfnisses? Was wandelt sich bei verschiedenen
Repräsentationen desselben Erlebens, beispielsweise durch andere Bewertung
derselben Begebenheit? Was wandelt sich beim Wechsel des vegetativen
Nervensystemzustands repräsentiert durch ventrovagale, sympathische und
dorsovagale Aktivierung, beispielsweise ausgelöst durch äußere Begebenheiten
wie das Aufkommen eines Sturms? Was wandelt sich beim Wechsel des
physiologischen Zustands, beispielsweise ausgelöst durch den Konsum von
Lebensmitteln wie Zucker oder Kakao? Was wandelt sich beim Wechsel der
Gehirnwellenfrequenz, beispielsweise durch Meditation oder ein Naturbad? Ist es
dir umgekehrt möglich durch bewusste Atemkontrolle dein inneres Erleben zu
beeinflussen?

Die Polyvagaltheorie unterscheidet ventrovagale, sympathische und dorsovagale
Aktivierung des vegetative Nervensystems. Der ventrovagale Zustand ist bekannt
als "Social Engagement" oder "Rest and Digest" oder "Feed and Breed". Er dient
der sozialen Vernetzung, Regeneration und Fortpflanzung. Der sympatische
Zustand ist bekannt als "Tend and Befriend" oder "Fight or Flight". Er dient
der Mobilisierung bei Gefahr zum Schutz von Nachwuchs und zur Suche von
Verstärkung oder zum Kämpfen oder Fliehen. Und der dorsovagale Zustand ist
bekannt als "Shutdown" oder "Freeze or Collapse". Er dient der Immobilisierung
zum Totstellen bei scheinbar ausweglosen Gefahrensituationen.

Schlucken, Gähnen, Seufzen, tiefes Einatmen, Verdauungsgeräusche und
Magengrummeln zeigen an, dass das parasympathische Nervensystem aktiviert
wurde, das heißt, dass sich das vegetative Nervensystem entspannt, was mit
ventrovagaler Aktivierung assoziiert ist.

**TODO [Von Verhaltenshinweise auf inneres Erleben schließen](https://www.nlp.ch/pdfdocs/innereserleben.pdf)**

**TODO [Subjective State](https://medium.com/@hanzifreinacht/the-scale-of-subjective-states-1b151e58fd3b)**

**TODO [Neuro-Linguistisches Programmieren](https://www.nlp.at/was-ist-nlp)**

**TODO [Buteyko-Methode (Atemhunger)](https://www.buteyko-deutschland.de/was-ist-buteyko/)**

## Einführung einer Atemübung

Einführung einer neuen Atemübung in mehreren Schritten

1. Wortlose Vorführung: Die Zuschauenden lernen durch Sinnes-, Gefühls-,
   Nervensystem- und Gedankenwahrnehmung. Welchen Eindruck macht das Bezeugen
   in dir? Wie resoniert es in dir? Was schwingt in dir mit? Was hallt in dir
   nach? Wie erlebst du die Teilhabe? Wie nimmst du Wechsel von
   Bewusstseinszuständen der übenden Person wahr?
1. Wortreiche Vorführung: Die Zuschauenden erfahren in Worten was die übende
   Person gleich tut und bezeugen dies dann. Wie nimmst du die einzelnen Phasen
   von außen wahr? Sind sie im Einklang mit dem Gesagten? Welche der genannten
   Aspekte erfährst du in dir, welche nicht genannten, und welche bleiben dir
   verborgen? Sind die erfahrenen Aspekte im Einklang mit dem Gesagten? Hast du
   eine Ahnung davon warum dir manches verborgen bleibt?
   - Welche Körperhaltung wird eingenommen? Wie ändert Haltung und Form über
     einen (Atem)zyklus hinweg (bei Wechselatmung über einen gesamten Zyklus,
     der aus zwei Ein- und zwei Ausatmungen besteht)? Wie wird die
     Körperhaltung und -form verändert? Durch welche muskuläre Spannung und
     Entspannung oder durch welche Gewebselastizität und Drucknterschiede?
   - Wo strömt der Atem ein und wo aus? Von wo nach wo strömt der Atem in
     welcher Phase? Wie wird der Atemstrom gelenkt? Wie ist die anfängliche
     Lungenfüllung? Wie schnell strömt der Atem über die Zeit hinweg? Wie
     groß ist das Ein- und Ausatemvolumen? Wann gibt es Atempausen? Wie lange
     dauern die einzelnen Phasen?
   - Worauf ist der äußere Fokus (Außenwahrnehmung/Exterozeption durch Sehen,
     Hören, Riechen, Schmecken, Gleichgewicht, Fühlen, epikritische und
     protopathische Sensibilität) und worauf der innere Fokus
     (Innenwahrnehmung/Interozeption durch Propriozeption, Viszerozeption und
     Nozizeption) in welcher Phase gerichtet?
1. Geleiteter Durchgang: Jede Person übt gemäß der Vorgabe. Wie gut findest du
   in die Übung hinein? Wie gelingt es dir die einzelnen Phasen umzusetzen? Wie
   nimmst du die Beziehung zwischen den Phasen wahr? Wie erlebst du die Übung
   im Ganzen? Was ist für dich stimmig und was würdest du anders machen? Was
   ist dir zu viel und was fehlt dir? Wie erlebst du dich vor und wie nach der
   Durchführung?
1. Persönliches Üben und Bezeugen: Eine Person übt und die andere bezeugt. Wie
   erlebst du dich vor der Ausführung? Wie erlebst du dich währenddessen? Was
   ist wie erwartet und was ist anders? Wie ist es für dich bei der Übung
   bezeugt zu werden? Wie erlebst du dich danach? Was ist beim Bezeugen in dir
   lebendig?
1. Gemeinsames Atmen: Jede Person für sich und alle synchron (aufeinander
   abgestimmt/eingestimmt/eingeschwungen). Welche Gruppendynamik entwickelt
   sich? Wie ist es für dich bei dir zu bleiben? Wie ist es für dich bei der
   Gruppe zu sein? Wie ist es für dich bei dir und der Gruppe zu sein? Wie
   entsteht Synchronität? Von alleine oder durch absichtliche Anpassung
   aneinander?
1. Theoretische Fundierung: Die lehrende Person erläutert philosophische,
   spirituelle, kulturelle, psychologische, anatomische und physiologische
   Hintergründe.
1. Spielerische Erforschung/Entdeckungsreise: Die Art der Ausführung variieren.
   Atemfrequenz und Atemvolumen, Geisteshaltung und Körperhaltung,
   Weichheit/Geschmeidigkeit und Kraft, Einklang und Ausklang, Atemräume,
   Rhythmus und Takt (beispielsweise mit musikalischer Unterstützung),
   Gleichmäßigkeit und Unregelmäßigkeit, Widerstände (beispielsweise in
   Bauchlage gegen den Boden atmen oder in Hände an den Rippenbögen), in Ruhe
   und in Bewegung, mit Körperübungen, Streckung und Beugung, Öffnung und
   Schließung/Schutz/Geborgenheit.
1. Vertiefung: Regelmäßige Praxis. Was wandelt sich über die Zeit und was
   bleibt gleich? Welche Veränderungsprozesse vollziehen sich?
1. Integration: In den Alltag übernehmen. Durch Atemwahrnehmung und
   -beobachtung etwas über sich erfahren (sich über den inneren Zustand in der
   Gegenwart zu informieren) und durch Atemkontrolle etwas ändern (den
   inneren Zustand modulieren, das Aktivierungsniveau des Nervensystems und den
   Bewusstseinszustand anpassen).

_Atmen in Bewegung und durch Bewegung_: Ist eine Atemübung mit einer
Körperbewegung verbunden, so ist es erkenntnisreich beide unabhängig
voneinander zu üben mit der Forschungsfrage ob das eine das andere einlädt und
ob es dies auf die gewünschte Weise tut. Leitet das Heraufschwingen der Arme
über vorne mit lockeren Gliedmaßen und lockerem Oberkörper an zwei Stellen die
Einatmung aus und das Herabschwingen an einer Stelle die Ausatmung? Ist es
natürlich bei Ein- oder Ausatmung durch Nase oder Mund zu atmen? Was geschieht
bei dir intuitiv? Wie ist es, wenn du das Gegenteil machst? Wie ist es, wenn du
die Übung wie vorgegeben ausführst? Ähnliche Fragen bei Rück- und Vorbeugen und
bei Seitbeugen und Drehungen.

_Spielen/Lernen_: Besteht eine Atemübung aus mehreren Teilen, so übe jeden Teil
für sich. Wie ist das? Ist eine Atemübung herausfordernd, so überlege wie du es
dir leicht machen kannst. Was ist herausfordernd? Warum ist das so? Wie kannst
du das verwandeln? Spiele, fabuliere, kreiere, schöpfe dein persönliches Yoga
--- yogiere für dich, für deine Gesundheit und deine Lebensfreude. Welche
Variante der Übung bereitet dich auf die ursprüngliche Übung vor?

_Äußere und innere Atmung_: Wie nimmst du deine äußere Atmung durch die Lungen
wahr? Und wie deine innere Atmung in den Zellen, das heißt die Aufnahme von
Sauerstoff der Zellen aus dem Blutkreislauf und die Abgabe von
Kohlenstoffdioxid in den Blutkreislauf? Mit dem aufgenommen Sauerstoff
oxydieren Zellen organische Verbindungen --- aerober Stoffwechsel.

_Atemlenkung_: Durch Selbst- oder Fremdberührung kannst du die Beatmung
bestimmter Körperräume erlernen.

_Atemraumvergrößerung_: Durch Selbst- oder Fremdmobilisation kannst du deine
Atemräume vergrößern.

_Zwerchfellstärkung_: Lege dich bauchlinks auf einen Gymnastikball, umrunde
diesen mit deinem gesamten Oberkörper und atme kraftvoll in den
Bauch-Becken-Raum. Oder praktiziere Pressatmung. Beides erleichtert Zugang zur
Wahrnehmung des Zwerchfells, schafft Bewusstsein dafür, macht es leichter
spürbar.

_Visualisierung_: Stelle dir vor du wirst geatmet. Stelle dir vor deine Lunge
fülle sich von unten nach oben auf. Stelle dir vor mit der Einatmung ziehst du
Energie über deinen Damm aus der Erde (Wurzelchakra) bis zu deiner Fontanelle
in den Himmel (Kronenchakra) und mit der Ausatmung lässt du Energie über deine
Fontanelle aus dem Himmel bis zu deinem Damm in die Erde sinken --- ein
immer währender Energiekreis (Micro-Cosmic Orbit)

_Vorstellung_: Durch Kapalabathi in Innenwelt nach Thema schnüffeln, das gerade
Raum möchte, sich zeigt und präsent ist.

_Ida und Pingala_: Ausatmung ist Ida, lunar, Yin, Ha, Parasympathikus,
langsamer Herzschlag/Puls. Einatmung ist Pingala, solar, Yang, Tha,
Sympathikus, schneller Herzschlag/Puls.

_Klang_: Woher kommt Klang und wohin geht er? Beispielsweise bei der
Ozeanatmung/Meeratmung/Uddjayi-Atmung. Folge dem Klang mit deinem Bewusstsein
vom feinsten Aufkommen bis zum letzten Versiegen. Entstehen und Vergehen. Wie
variiert der Klang? Kannst du ihn sanft/weich/gleichmäßig werden lassen?

_Anfänger:ingeist_: Übe mit Anfänger:ingeist. Jede Wiederholung ist neu und
bringt neue Einsichten. Bemerke die Feinheiten, die Nuancen. Alles, jeder
Moment ist neu.

## Wirkungen

Atemarbeit hat mannigfaltige Wirkungen/Effekte

- mental macht sie Zusammenhänge zwischen Atem und Seele, Geist, Nervensystem,
    Gefühlen und Körper bewusst; verändert die Gehirnaktivität; sensibilisiert
    für die Wahrnehmung feinstofflicher Prozesse; schafft Bewusstsein für den
    Innenraum des Oberkörpers;
- physiologisch moduliert sie den Säure-Basen-Haushalt im Blut durch Veränderung
    des Verhältnisses des Sauerstoffgehalts zum Kohlenstoffdioxidgehalt und
    sensibilisiert für verschiedene Gasdrücke im Herz-Kreislauf-System
    (beispielsweise vermehrtes Abatmen von Kohlenstoffdioxid bei
    Hyperventilation kann sich unter anderem durch Kribbeln in den Fingern,
    Füßen und im Mundbereich, durch Zittern, Schwindel und Benommenheit
    zeigen);
- physisch regt sie den Stoffwechsel (Verdauungstätigkeit/-feuer) durch Massage
  von innen an (Kompression und Dekompression, Druck und Dehnung);
- physisch und mental verbindet sie Rumpf ober- und unterhalb des Zwerchfells
- nervensystemisch hilft sie bei der Selbstregulation.

Atemübungen helfen die körperlichen Manifestationen von Prana unter Kontrolle zu
bringen und Meditation die mentalen.

Ausatmung befreit, erholt, regeneriert. Die Kontrolle der Ausatmung ist
wichtiger als jene der Einatmung. Verlangsamtes Ausatmen hilft dem Körper sich
anzupassen, beispielsweise beim Nehmen einer kalten Dusche oder beim Heben
einer Last (Druck im Bauch-Becken-Raum stützt Lendenwirbelsäule; Zwerchfell
kontrahiert dabei exzentrisch) oder beim Halten von Stress/Herausforderungen
(Seufzen ist ein Weg des Körpers die Lunge zu leeren, wenn du wegen Stress
nicht vollständig ausatmest). Eine sorgfältige Atemhandhabe und Atemhaushalt
trägt zu Gesundheit und Vitalität bei.

Atembewusstsein bringt Körperbewusstsein, da gelöstes Atmen in schlechter
Haltung herausfordernd ist.

Um deinen Atem zu beruhigen, spucke ihn mehrmals kraftvoll aus.

"Diese Atmungstechniken entblößen das Licht des puren Bewusstseins und bringen
mentale Klarheit." - Raja Yogasutras von Patanjali, Kapitel 2, Vers 49 und 52.

**TODO [Atmung als Regenerationsstrategie](https://physiotherapeuten.de/artikel/atmung-als-regenerationsstrategie/)**

## Forschungsfragen

**TODO [Own Your Breath](https://www.ownyourbreath.de/was-ist-breathwork) und
[Intesoma Breathwork](https://www.intesomabreathwork.com/)**

_Sinne_: Wie kannst du deinen Atem mit allen Sinnen wahrnehmen? Wie kannst du
deinen Atem hören? Wie kannst du tönen, summen und brummen? Wie kannst du
deinen Mundraum formen und deine Kehle schließen um deinen Atem zu hören wie
bei der Ozeanatmung? Wie kannst du deinen Atem spüren? Wie kannst du dieses
Spürerlebnis modulieren? Wie kannst du deinen Atem riechen? Wie riecht die
Atemluft bei der Ein- und wie bei der Ausatmung? Wie atmest du, wenn du
an etwas schnüffelst oder bewusst riechst? Wie kannst du dein Riecherleben
verfeinern? Wie kannst du deinen Atem schmecken? Wie kannst du die Atemluft
mit Geschmack versehen? Wie schmeckt salzige oder süßliche Atemluft? Wie kannst
du deinen Atem sehen? Wie ist es Dampf ein- und auszuatmen? Wie kannst du mit
Kälte deine Ausatmung sichtbar machen?

_Nervensystem/Steuerzentrale_: Was passiert in dir kurz bevor der Atem beginnt
ein- oder auszuströmen? Wie erfährst du Atemhunger (den Wunsch einzuatmen)? Was
passiert in deinem Gehirn kurz bevor und während du Nervenimpulse an die
Motoneuronen sendest um ein- oder auszuatmen? Was geschieht kurz bevor du
Wandel im Körper wahrnimmst? Kannst du Nervenimpulse an die Motoneuronen
wahrnehmen? Kannst du die Entscheidung deines Unbewusstseins wahrnehmen?

_Beziehungen_: Wie ist die Beziehung zwischen Atmung und vegetativem
Nervensystem, Gedankenwelt, Gefühlswelt, Körperwahrnehmung und
Bewusstseinszustand (Achtsamkeit, Aufmerksamkeit)?

_Herz_: Wie ist die Beziehung zwischen Atmung und Herz(kreislaufsystem),
Verdauungssystem, Bewertungssystem, Belohnungssystem, Sinneswahrnehmung,
Wünschen, Bedürfnissen, Kompensationsstrategien, Gelüsten, Sexualität und
Hormonsystem? Bei der Einatmung wird der Sympathikus aktiviert, die
Herzfrequenz steigt, und bei der Ausatmung der Parasympatikus, die Herzfrequenz
sinkt.

_Nasenzyklus_: Wie steht dies in Beziehung mit Atemstrom durch linkes oder
rechtes Nasenloch entweder absichtlich oder wegen des natürlichen Wechsels
genannt Nasenzyklus.

**TODO [Nasenzyklus](https://wiki.yoga-vidya.de/Atmung#Der_Nasenzyklus_-_Atmen_f.C3.BCr_das_Gehirn)**

_(Un)mittelbar_: Auf welche Instanzen hat Atmung unmittelbaren Einfluss und auf
welche mittelbaren durch beispielsweise das Vegetativum? Beispielsweise umgeben
efferente Nervenzellen des Vagusnervs Lunge und Rachen, welche durch Atmung
stimuliert werden, welche Teil des parasympathischen Nervensystems sind, dieses
stimulieren und dadurch beispielsweise Verdauungstätigkeit anregen, den
Blutdruck senken und so weiter.

_Koshas_: Wie ist die Beziehung von Atmung zu den 5 Koshas (Körperhülle,
Energiehülle, Emotionale Hülle, Intellektuelle Hülle, Wonnehülle)? Pranayama
ist der Faden, der die Körperhülle mit dem Astralörper verbindet. Wird der
Pranafaden durchtrennt, dann tritt der Tod ein. Pranayama verbindet den Körper
mit der Lebenskraftenergie. Mit jeder Einatmung vergrößert sich der Pranakörper
und mit jeder Ausatmung verkleinert er sich wieder. Wie weit kannst du deinen
Pranakörper ausdehnen?

_Verbindung_: Über deinen Atem bist du mit der ganzen Schöpfung verbunden. Mit
allen atmenden Wesen wie deinen Mitmenschen, den Tieren, den grünen
Photosynthese betreibenden Pflanzen, den Mikroorganismen.

## Aufatmen

Öffne die Tür zum spielerischen Erleben der acht Stufen des Rāja-Yoga.

**TODO: Allgemein zu Raja Yoga. Konkret zu: In diesem Kurs werden wir gemeinsam ...**

Rāja-Yoga (Sanskrit: राजयोग) ist der königliche Yoga der Selbsterkenntnis
bestehend aus acht Gliedern. Enthalte dich gemäß des Enthaltenskodexes, yamas
(Sanskrit: यम). Verhalte dich gemäß des Verhaltenskodexes, niyama (Sanskrit:
नियम). Reinige deinen Körper und bereite deine Atemmuskulatur vor mit
Körperhaltungen, āsanas (Sanskrit: आसन). Erkunde deine Atemräume und speichere
Lebensenergie mit Atemarbeit, prānāyāma (Sanskrit: प्राणायाम). Ziehe deine Sinne
zurück und tauche tief in dich ein in Sammlung, pratyāhāra (Sanskrit: प्रत्याहार).
Fokussiere deinen Geist und mache ihn einspitzig in Konzentration, dhāraṇā
(Sanskrit: धारणा). Fließe beständig und mühelos zu dem Objekt deiner Ausrichtung
in Meditation, dhyāna (Sanskrit: ध्यान). Verschmelze eins werdend mit dem Objekt
deiner Ausrichtung in Versenkung, samādhi (Sanskrit: समाधि).

Haṭha-Yoga (Sanskrit: हठ योग) reinigt die sechs Hüllen auf sechsfache Weise und
harmonisiert insbesondere Körper, Geist und Seele.

Begeistere deine Atemräume (Pranayama), tauche in dich ein in Atemstille
(Kumbhaka, Pratyahara), erfahre dich bei Körperübungen (Asanas), trete ein in
den ewig gegenwärtigen Raum inneren Gewahrseins (Konzentration und Meditation,
Dharana und Dhyana), verkörpere deine Lebendigkeit (Samadhi mit Form).

Erfahre/Erlebe/Entdecke deine Lebendigkeit, nehme sie wahr, hole sie zu dir, in
dein Bewusstsein.

Gemeinsam beleben wir unsere Körper, lernen entspannen und anspannen,
begeistern unsere Innenräume, lösen Blockaden, bringen sie in Fluss, mit
liebevoller Achtsamkeit. Wir ziehen unsere Sinne nach innen zurück, lenken sie
nach innen, beobachten unseren Atem, machen ihn bewusst, übernehmen sanft
Kontrolle, leiten ihn in verschiedene Körperräume, erkunden diese Atemräume,
laden uns kraftvoll auf, holen Lebensenergie zu uns und erfahren/genießen tief
in uns eingetaucht Atemstille.

In Atemstille offenbart sich nach und nach unsere subtile Lebendigkeit, feine
Ströme der Glückseligkeit, kommen wir tief in uns an, im Hier und Jetzt. Wir
stellen uns die Fragen: Welche Beziehung besteht zwischen Atem und Geist? Wie
sind Atem und Nervensystem verbunden? Welche Gefühlswelten tun sich uns auf?
Wie sind Atem und Innenwahrnehmung verbunden?

Holen/Begleiten unseren davon eilenden Geist immer wieder liebevoll zurück, bis
er nach und nach einfach bleibt, ein müheloser Fluss von Aufmerksamkeit, bis
wir mit dem Objekt unserer Meditation verschmelzen, mit ihm eins werden, darin
aufgehen, es verkörpern, es sind.

Und ganz unscheinbar lernen wir uns kennen, jede Facette unseres Ichs,
durchdringen, durchschauen, begreifen uns immer mehr, bis wir am Grund die
ersten Strahlen unseres Wesenskerns, unseres Lebensfunkens, unserer
Lebendigkeit erahnen und dahin vordringen. Dort angelangt verharren/warten wir
demütig auf Einlass, mit festem Willen und ohne Verhaftung, halten dem
abstrahlenden Einfluss der Psyche stand. Und plötzlich ganz unverhofft wird uns
Einlass gewährt.

## Buch

Atem verbindet wie Faden einer Perlenkette alle Momente des Lebens. Mit jedem
Atemzug reiht sich der nächste Moment an.

Atem ist Brücke zwischen Bewusstsein/Bewusstheit und körperlicher Lebendigkeit.

Übung zur Offenbarung der Verbindung zwischen Atem und Aufmerksamkeit und zum
Lösen von Spannungen und Blockaden: Stelle dir vor du atmest über ein
Körperteil, -bereich oder -organ wie deine Leber ein und aus, so als wäre
dieses deine Lunge. Visualisiere dabei ein pulsierendes Licht, das bei der
Einatmung heller und bei der Ausatmung dunkler wird.

Atem ist Ausdruck eines tieferen Seins, unseres eigentlichen Wesens, welches
bei der Geburt unseren Körper wie einen Mantel anlegt, diesen das ganze Leben
hindurch trägt und erst beim Tod wieder ablegt. Atem ermöglicht unserem
Wesenskern eine menschliche Erfahrung zu machen, in welcher wir unseren
Wesenskern entdecken und wahrnehmen können. Atem verbindet das Dieseits mit dem
Jenseits, ist die Regenbogenbrücke, die uns mit jedem Atemzug näher nach Hause
bringt.

Übung: Entspanne dich in die Ausatmung, lasse dich sanft darin fallen, lasse
los, lausche ihr. Lasse die Einatmung wie von selbst geschehen. Die Einatmung
ist eine Reise, die Atempause eine Rast und die Ausatmung Heimkehr.

Jeder Atemzyklus ist ein Schöpfungsakt. Ein Atemzyklus Brahmans ist ein
vollständiger kosmischer Zyklus, Beginn und Ende des Weltalls. Einatmung ist
Werden des Kosmos und Ausatmung ist Vergehen. Ein Atemzyklus eines Menschen ist
Beginn und Ende eines neuen Moments. Einatmung ist Entstehung des Moments,
Atemstille ist Sein in Präsenz und Ausatmung ist Loslassen des Moments. Das
Universum als großes Geschehen spiegelt sich im Kleinen, Makrokosmos in
Mikrokosmos, alles ist verbunden.

Atem ist essentiell für Gesundheit, Ruhe, Heilung, seelisches Wachstum, tiefe
innere Erfahrungen und Inspiration vom lateinischen "inspirare" was einhauchen
und einblasen bedeutet. Es atmet durch mich. Inspiration schenkt uns Leben und
macht uns zu Mitschöpfenden.

Atem ist Brücke ins Licht.

Übung (ein lösender Atemzug): Nehme einen befriedigenden Atemzug. Pausiere
solange es angenehm ist. Atme fließend, entspannt und langsam aus, eventuell
mit Ujjayi-Atmung oder Lippenbremse. Pausiere (halte inne) bis der nächste
Atemzug von selbst kommt.

Übung zum Erleben der Macht des Atmens (zirkulär verbundenes Atmen, also ohne
Pause): Atme für mehrere Atemzyklen so, dass keine Atemstille zwischen der Ein-
und Ausatmung ist. Lass anschließend ein Atemzug geschehen und verweile in
Atemstille solange kein natürlicher Atemimpuls kommt. Atme dann die restliche
Atemluft ab und lass den Atem geschehen.

Physiologische überatmest (hyperventilierst) du bei der vorangegangenen Übung.
Das heißt du lädst dich mit Sauerstoff auf und atmest viel Kohlenstoffdioxid
ab. Dies reduziert die Hirndurchblutung um bis zu 60% und reduziert dadurch die
Sauerstoffversorgung des Gehirns, insbesondere im präfrontalen Kortex, und
aktiviert das limbische System. Mögliche Effekte sind seelische Reinigung,
Lösen belastender Energien und spirituelle Durchbruchserfahrungen. Diese
Effekte sind abhängig von der persönlichen Geschichte auch wenn physiologisch
bei allen Menschen ähnliches geschieht.

Körper, Atem und Geist sind eine Einheit. Atem ist das Bindeglied zwischen
Körper und Geist.

Atem kehrt inneres nach außen und umgekehrt (jeder Gedanke, jedes Gefühl, jeder
innere Zustand, ... spiegelt sich im Atem). Durch Atemarbeit können wir Inneres
beeinflussen für

- mehr Ruhe und Gelassenheit
- größere psychische Resilienz (Widerstandskraft)
- körperliche Gesundheit und Leistungsfähigkeit
- mentale Stärke und Konzentration
- Entspannung und Regeneration und Heilung
- emotionale Erlebnisse und euphorische Erfahrungen
- Katharsis (psychische Reinigung) und seelische Durchbruchserfahrungen
- Meditation und spirituelle Ziele

Er ist auch ein psychologisches Phänomen.

Übung (Atemräume wahrnehmen): Oben, unten, Mitte. Nehme Hände zum Spüren und
zum Setzen taktiler Reize zur Hilfe.

Lebensenergie (Prana) folgt Aufmerksamkeit/Bewusstsein/Geist (Citta) und
umgekehrt. Worauf du deine Aufmerksamkeit lenkst, dahin fließt deine Energie
(beispielsweise bei Heilungsprozessen oder Wärmeerzeugung). Und wohin deine
Energie fließt, dahin zieht es deine Aufmerksamkeit (beispielsweise Schmerzen
und Jucken bei Heilungsprozessen oder Kälte/Hitze).

Atem ist Brücke zur inneren Stille.

Jeder Atemzug ist das Geschenk eines neuen Lebensmoments. Im gegenwärtigen
Atemzug bist du lebendig und die bist innerlich frei, wenn du den Atemzug
bewusst erlebst.

Übung (Atembeobachtung/-gewahrsein): Lasse Atmung zu wie sie ist, löse dich von
Vorstellungen wie sie sein sollte und nehme wahr wie sie ist.

Atem ist Brücke zur Präsenz, zum Gegenwärtigen und Wachsein.

be-sonne-nes Handeln

Übung (Nase versus Mund) zur Erkenntnis, dass Mundatmung mehr in Brustraum
fließt (schnell und viel) und Nasenatmung mehr in Bauchraum und untere
seitliche Rippen (langsam und wenig). Mund: Brustatmung
(Zwischenrippenmuskulatur). Nase: Bauchatmung (Zwerchfellatmung).

Physiologisch vernünftige Respirationsrate sind 8 bis 12 Atemzüge pro Minute.

Dysfunktionale Atmung (in "Social Engagement"/Rest & Digest/Feed & Breed/
/Ruhe/Entspannung): Mund, Brust, beschleunigt, unregelmäßig, geräuschvoll
(Zeichen für zu viel), weiche/passive/entspannte Ausatmung mit kurzer Pause
danach.

Gesund: rhythmisch-fließend, loslassende Ausatmung (keine Spannung im
Zwerchfell).

Äußere Dimension: Atemmechanik und Atemrhythmik (Geschwindigkeit,
Gleichmäßigkeit (eines Zyklus und über mehrere inweg), Pausen/Verbunden,
zeitliche Verhältnisse).

Innere Dimension: Atemvolumen (klein oder groß).

Äußere bestimmt innere Dimension.

Überatmung stört Säure-Basen-Haushalt.

Funktional/natürlich: Durch Nase, vom Zwerchfell getragen, leise, langsam,
rhythmisch, entspannt in Ausatmung (sanft, weich, fließend), etwa 5 Liter pro
Minute.

Höchstes Ziel von Atemarbeit ist funktionale Atmung wiederherzustellen. Dies
ist die Basis für weitere Atemarbeit.

Übung: Atme nur in den oberen Bauchraum ein so, dass der Brustkorb
eben/unbewegt bleibt. Entspanne dich in die Ausatmung hinein, lasse los, wie
ein kurzes Ausruhen. Lasse danach die Einatmung geschehen wie von selbst ohne
aktives Tun.

Sanfter Lufthunger ist gesund. Er zeigt an, dass der Kohlenstoffdioxidgehalt im
Blut leicht erhöht ist. Dadurch weiten sich die Blutgefäße, wird die
Durchblutung gesteigert, wird die Bereitschaft von Blut Sauerstoff ins Gewebe
abzugeben/zu entlassen erhöht und wird der Säure-Basen-Haushalt reguliert.

Du erkennst, dass das parasympathische Nervensystem aktiviert ist durch warm
werdende Hände, Gähnen, Schluckimpulse.
