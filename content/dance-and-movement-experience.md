---
title: Dance and Movement Experience
layout: single
---

I consider myself as a novice in the field of dancing and Contact Improvisation (CI).

# Dance Background

- A handful of times some years ago: CI at Monday Momentum with Eckhard Müller, Melanie Seeger, and Benno Enderlein
- Weekly for about 1 year: CI Basics at Moving Monday with Daniel Armbrüster and guest teachers (about 1 hour workshop and 1 hour jam).
- Weekly since end of April: CI with Melanie Seeger on Tuesdays (about 2 hours workshop)
- Weekly since end of May: Contemporary Dance with Melanie Seeger on Thursdays (about 2 hours workshop)
- 5 days: CI and embodiment workshops during the "Body & Heart Festival" with Daniel Werner, Muriel Jeanne Mollet, Patricia Baquero, Kabiro Eva Scheller, Heike Wegener, and Melanie Seeger
- 3 days: CI workshop "Moment to Momentum" with Katja Mustonen
- 3 days: CI workshops during the "Contango Festival", namely the intensive "Falling in Contact" with Adrian Russi, the intensive "Contact Basics" with Dirk Schuka, and Contango basics "Butterfly Contact" with Günter Klingler
- 5 days: CI workshop "Somatic Contact Improvisation" with Jörg Hassmann and Daniel Werner
- 6 days: CI workshops during the "TanzTageTempelhof", namely the intensive "Relational Forces" with Jonathan Brussolo and the intensive "Dancing Love Letters for the Planet Earth" with Nica Portavia, and I taught a 2 hour workshop on Steve Paxtons "Material for the spine" and "Small dance" with Nica Portavia
- 7 days: CI workshops during the "International ContactFestival Freiburg", namely the intensive "Set and Unset" with Rick Nodine and the workshops "Pelvis Pivot Point" with Angela-Mara Florant, "Diving into Backspace" with Elise Nuding, "Surfacing Layers" with Olivia Shaffer, "Weft Explorations" with Roxana Galand, and "Instinctual Flight" with Raul Saldarriaga
- 7 days: Axis Syllabus "Introductory Module: Ramp Construction Principles / Hips & Shoulder Anatomy /Senses & Perception - BMC" of the year-long program "Dance as a Research Practice" with Kira Kirsch, Antoine Ragot, and Nina Wehnert
- 1 day: CI workshop "Somatic Improvisation" with Irene Sposetti
- 2 days: Relating through movement and dance "Relating & Relying" with David Schäppi
- 2 days: Contact Tango with Javier Cura "From spirals into animal flow into contact tango"
- 7 days: Axis Syllabus "Module 1: Bio-Tensegral Dynamics /Contact Improvisation /Embodied Somatic History / Gait Cycle Analysis / Fascial Fantasies" with Kira Kirsch, Antoine Ragot, Katja Mustonen, and Peter Pleyer
- 2 days: Axis Syllabus "Workshop 1: Seeds" of a four-workshops cycle with Livia Kern
- For about half a year now: Once a month I hold a CI space that begins with a warm-up or workshop between 20 minutes and 1 hour and ends with a jam

I practice almost daily at home during the morning and a few times a week at CI jams during the evening. I take inspiration from various online workshops.

# Other movement modalities

Short: Besides dancing I practice Feldenkrais, Yoga, Primal Movement, intuitive ground movement, and am a Thai-Yoga massage therapist, a Hatha-Yoga teacher (though I haven't given classes for about a year now due to personal reasons), and a Holistic-Bodywork practitioner.

Besides dancing I practice

- Feldenkrais
- Yoga
- Animal and Primal Movement
- Intuitive ground movement

I am a

- Thai-Yoga massage therapist (for about 4 years)
- Hatha-Yoga teacher (for about 2 years now, though I haven't given classes for about a year)
- Holistic-Bodywork practitioner, a process-oriented bodywork that combines Thai-Yoga massage, somatic coaching, trauma sensitivity, and spirituality (for about 1 year)

In the past I

- juggled and did object manipulation and some circus artistic for about 24 years (I performed for about 10 years and gave workshops for about 3 years)
- practiced gymnastics for some years in my late youth and early adulthood
- free climbed intensively for some years in my early 20s
- mountaineered in my late 20s
- practiced various martial arts not sticking to one
- recovered from 2 knee operations 7 years ago thereby discovering various movement modalities like Feldenkrais, Laban/Barenieff, somatic groundwork, and various Yoga styles
