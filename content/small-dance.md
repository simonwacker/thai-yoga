Grundlagenworkshop und Jam "Small Dance into Contact Improvisation" am Sonntag, 20. Oktober, 20 bis 22 Uhr in "The MOVE", Habsburgerstr. 9, 79104 Freiburg, ein Bewegungsraum auf dem Gelände der FABRIK. Ich möchte mit euch den "Small Dance" erforschen als ein Zugang zu Improvisationstanz alleine und in Kontakt. Der "Small Dance" ist eine Form der Meditation im Stehen, die von Steve Paxton, dem Begründer der Contact Improvisation, entwickelt wurde. Was geschieht, wenn wir die feinen Bewegungen des "Small Dance" tanzend in den Raum tragen und wenn wir uns auf diese Weise improvisierend begegnen? Der Abendworkshop mit Jam richtet sich an alle, die diese grundlegende Praxis kennenlernen oder vertiefen möchten, mit oder ohne Erfahrung in CI. Der Raumbeitrag liegt zwischen 5 und 10 Euro. Der Workshopteil dauert etwa 37 Minuten und der Jamteil etwa 73 Minuten. Schreibe mir bei Interesse gerne vorher oder komme spontan vorbei. Liebe Grüße, Simon

---

[Warm-Up: The Small Dance Into Grazing](https://myriadicity.net/contact-improv/learning-contact-improvisation/warm-up-following-small-inspirations-into-grazing)

The Stand or the “small dance” — a simple meditation. First, find a comfortable standing position. Second, scan the body for excess tension and inefficient ways of holding the body and allow the body to become more easily aligned with less unnecessary muscle use. From here, with just a little attention, it will become obvious that the body is still in motion… the small dance of reflexes below the level of consciousness which keeps the body standing. One watches this small dance, perhaps for a minute, perhaps for an hour. Taken into the larger dance of bodies in contact, one realizes that within the larger dance, there is always the small dance which can be tuned to, related to with curiosity. This study of the small dance is one of the primary foundations of contact improvisation. All movement that we choose is in the context of the ongoing small dance.
--- [Fundamentals of Contact Improvisation](https://www.bodyresearch.org/contact-improvisation/fundamentals-of-contact-improvisation/)

"small dance," a form of meditation that is practiced standing, where attention is paid to postural adjustments and micro-weight transfers
--- [Why Standing?](https://en.wikipedia.org/wiki/Contact_improvisation#cite_note-10)

In 1967, Steve Paxton developed a movement practice based on the activity of standing: the Small Dance. What happens when my movements are inhibited, the Small Dance asks? What happens when I suspend volitional movement? Which symphonies of adjusting emerge from non-doing? These became some of the foundational questions & practices of a dance form known as Contact Improvisation, an exploratory movement inquiry that is now practiced by thousands around the globe.
--- [the small dance](https://cargocollective.com/sharingmovement/the-small-dance)

[The Small Dance, The Stand](https://myriadicity.net/contact-improv/learning-contact-improvisation/steve-paxtons-1977-small-dance-guidance)
by Steve Paxton

The text is to be delivered slowly, with pauses between each sentence.
Relax deep into the cone of the eye socket. Imagine a line that runs between the ears. That’s where the skull rests. Make the motion, very small, for “Yes.” This rocks the skull on the top vertebrae, the atlas. You have to intuit the bones. Like a donut. The sensation around it defines it. Do the motion for “No.” Between these two motions you can determine the length of the vertebrae.

… Ballooning of the lungs. Breathe from the bottom of the lung up to the clavicle. Can you expand the ribs out and up and back easily? Defining the diaphragm in terms of sensation. Bottom of the lung. Two domes of muscle. So with each breath you’re massaging the intestine… What the diaphragm is doing is a signal to the rest of the body. Sky above, earth below…

The head in this work is a limb. It has mass. Mass may be the single most important sensation. The feeling of gravity. Continuing to perceive mass and gravity as you stand. Tension in the muscle masks the sensation of gravity…

You’ve been swimming in gravity since the day you were born. Every cell knows where down is. Easily forgotten. Your mass and the earth’s mass calling to each other…

…Upward force of the bones. Shoulder blades fall down the back, relaxing the intestines into the bowl of the pelvis… In the direction the arms are hanging, without changing that direction, do the smallest stretch you can feel. Can it be smaller. Can you do less. The initiation of the stretch, along the length of bones, in the direction the force is already going. The small dance—you’re relaxing and it’s holding you up. The muscles keeping the weight throughout the skeleton. Shifting weight from leg to leg, interface, taking weight, compression. Stretching along the line of compression. Center of the small dance.

Upright position… spine erect… Feel the bottom of the lung, the diaphragm, feel it massage the organs, down into the bowl of the pelvis, relax your genitals and anus… breathe deeply… exhale slowly… feel the pause at the exhalation… watch for the beginning of the inhalation… This thing, time… full of rush and pause… feel time go by through the breath… don’t initiate the breath… just watch that period… try to catch your mind, the exact moment when the inhalation starts again…

Standing… Relax erect with the weight toward the back half of the knee, put some weight on the balls of the feet… relax the scalp… relax the eyelids… relax behind the eyes… deep into the cone of the eye socket… don’t spend any energy blocking or focusing… let your ideas flow… because certain things mask other things… and it’s better for this right now to have no concentration… feel the play of rush and pause of the small dance that holds you upright when you relax… through simple mass and balance… 60% on the ball of the foot, some to toes, rest back… knees a little relaxed… Let your breath guide your torso, make you symmetrical… let your ribs be open to the ballooning of the lungs… arms fall sideways… Feel the small dance… it’s always there… think of the alignment of the bones, limbs, towards the center of the earth… length of the bone…

…Take your weight over your left leg… what is the difference… in the thigh, in the hip joint… Calling this sensation “compression,” take compression over the right leg, feel the change… compression down the length of the bone… Take your body to neutral… lean forward… compression in front, stretch in back… back to neutral… lean backwards, stretch in front, compression in back… don’t have compression in the arms, there’s no weight there… lean forward again… feel the difference… relax… neutral… lean back, stretch along the length of your body… neutral… stretch up… let the spine rise through the shoulders… let the head be supported on a line between the ears… make the motion for “Yes,”… rock the head… the atlas… make a stretch connection, a long line of stretch between the ball of the foot and the atlas, between the toes, the ball of the foot up the leg to the spine, to the atlas… You’ve been falling in gravity since the day you were born…

Imagine, but don’t do it, imagine that you are about to take a step forward with your left foot. What is the difference? Back to standing…

Imagine but don’t do it, imagine that you are about to do a step with your left foot. What is the difference? Back to standing…

Imagine but don’t do it, imagine that you are about to take a step with your right foot… your left foot… your right… your left, right, left…… standing.

…Slowly let your body collapse into a squat… release into a voluntary fall. Breathe, squatting with hands on the floor, neck relaxed… see if you can relax in this position… and come up. [end]

