---
title: Backup
date: 2022-02-10T13:05:52+0100
layout: single
table_of_contents: true
---

[comment]: # "„“"

[comment]: # (
_[Pali]: Eigenbezeichnung „pāḷi“
_[Sanskrit]: Eigenbezeichnung „saṃskṛta“
_[Pinyin]: Eigenbezeichnung „Pinyin Hànyǔ Pīnyīn Fāng’àn“
_[Englisch]: Eigenbezeichnung „english“
)

## Quotes

> Tausende von Kerzen kann man am Licht einer Kerze anzünden ohne daß ihr Licht schwächer wird. Freude nimmt nicht ab, wenn sie geteilt wird.

--- Siddhartha Gautama (560-480 vor Christus)

> Du kannst die Wellen nicht stoppen, aber du kannst lernen zu surfen.

--- Aus dem Englischen nach Jon Kabat-Zinn (1944-heute)

> ... ein vollständiger Entwicklungsprozess, der ermöglicht ein intuitiv
> wahrnehmendes Instrument zu werden --- fähig in Resonanz mit dem/der
> Patient:in zu gehen und allem was tief in ihm/ihr liegt ... Kompetent in der
> Fähigkeit werden auf seine/ihre tiefsten Bedürfnisse einzugehen.

--- Aus dem Englischen nach Viola Frymann (1921-2016)

> Tu deinem Leib Gutes, damit deine Seele Lust hat, darin zu wohnen.

--- Aus dem Spanischen nach Teresa von Ávila (1515-1582)

> Die Frage ist nicht, ob es ein Leben nach dem Tod gibt. Die Frage ist, ob du
> vor dem Tod lebendig bist.

--- Aus dem Englischen nach [Osho](http://www.osho.de) (1931-1990)

> Nicht die Glücklichen sind dankbar. Es sind die Dankbaren, die glücklich
> sind.

--- Aus dem Englischen nach Francis Bacon (1561-1626)

> Wo immer du bist, sei ganz dort.

--- Aus dem Englischen nach [Eckhart Tolle](https://eckharttolle.com)
(1948-heute)

## ?

Interessante Punkte von https://thaiinflow.com/thai-massage/ :

For the GIVER, Dynamic Thai Massage is an opportunity:

to improve fluidity, precision and transitions in giving massage,
to be creative, joyful and happy while working,
to develop your touch while working on the mechanic, liquid and energetic levels.
For the RECEIVER, Dynamic Thai Massage is a chance:

to recover mobility and freedom in your musculoskeletal system,
to improve the circulation of fluids (blood and lymphatic systems),
to re-establish a good energetic circulation,
to release the nervous system.

Kurze praegnante Beschreibung von https://www.muditathaiyoga.com/about.html :

Thai Massage, or Nuad Boraen, or Traditional Thai Massage, or Thai Yoga Massage, is a unique and powerful healing art that has been an integral part of traditional Thai medicine for thousands of years. Thai Yoga Massage is a truly wonderful experience for both giver and receiver as energy blockages are released, boundaries dissolve and deep healing occurs in a space of presence and meditation. In a unique way, Thai massage blends elements of acupressure, yoga, reflexology, physiotherapy, meditation, energy healing and Ayurveda.

Und https://www.muditathaiyoga.com/thai-yoga-massage.html

## ...

Eine Thai-Yoga-Massage ist ein offener Erfahrungsraum für dich und für mich,
bei dem der Kontakt auf allen Ebenen dich dabei unterstützt dich selbst zu
erleben und kennenzulernen --- es geschieht was geschehen soll.

Das innere Erleben, persönliche Bedürfnisse und Wünsche während einer Massage
verbal in Kontakt zu bringen, sich mitzuteilen, das Geschehen zu beeinflussen
und Kontrolle zu übernehmen ist ein Akt der Selbstermächtigung (ich weiß was
ich will und ich stehe dafür ein) von der andere weiß es besser als ich zu ich
weiß es besser. Und zu lernen, sich selbst zu massieren, sich selbst zu
berühren, sich selbst gut zu tun, die eigenen Bedürfnisse zu erfüllen ist ein
Akt der Selbstwirksamkeit (ich kann mir geben was ich brauche).

Wesentlich (das Wesen) berühren: Eine Berührung zur richtigen Zeit am richtigen
Ort mit den richtigen Qualitäten genügt um das ganze Wesen zu berühren.

Co-Regulation und Neuroedukation: Wenn mein Nervensystem entspannt, dann kann
das der empfangenden Person mitgehen.

Psychoedukation: Die empfangende Person lernt sich selbst und innere
Zusammenhänge immer besser kennen.

## Geisteshaltungen

Diese Geisteshaltungen sind in der Berührungsqualität fühlbar. Liebevolle Güte
verleiht der Berührung eine liebevolle, offene, achtsame und warmherzige
Qualität. Mitgefühl macht die Berührung einfühlsam, achtsam, intuitiv und tief.
Mitfreude macht sie hingebungsvoll, pulsierend, rhythmisch und verspielt.
Gleichmut macht sie wertfrei, annehmend und akzeptierend.

## Loslassen/Hingabe

Du trägst mächtige Selbstheilungskräfte in dir. Gib Kontrolle ab, gebe dich
vertrauensvoll hin, gib der kreativen, schöpferischen, natürlichen Kraft in dir
Raum dazu Körper, Geist und Seele in Einklang zu bringen, zu reorganisieren,
auszugleichen, zu harmonisieren --- zu heilen.

Lass Fluss zu, steuere nicht, blockiere nicht, vertraue deinem
Unterbewusstsein, der Weisheit deiner ursprünglichen Natur, deines Selbsts,
jene Instanz, die sich unter all deinen erlernten, angeeigneten, sozialisierten
Hüllen befindet.

Erkenne dich als spirituelles Wesen, das eine menschliche Erfahrung macht.

> Wir sind keine menschlichen Wesen, die spirituelle Erfahrungen machen. Wir
> sind spirituelle Wesen, die eine menschliche Erfahrung machen

--- Aus dem Französischen nach Pierre Teilhard de Chardin (1881-1955)

## Übungen

- Geführte Praxis
- Rückmelderunden/-paare (shit sandwich)

alleine

- (Atem-)Meditation
- Intention setzen
- Palming/Footing des Bodens (rhythmisch, präsent, achtsam, absichtsvoll)
- Palming/Thumbing/Kneeing des eigenen Körpers
- Energiedusche oder Qualitätendusche (für sich selbst oder Partner:in)
- Gyoki (Hände reiben, Energiefeld spüren, damit spielen, mit Energiefelder
  anderer in Kontakt gehen, miteinander über Felder tanzen, Körperraum der
  danach verlangt damit durchströmen)

in Gruppe

- Eine Gruppenhälfte steht im Kreis mit geschlossenen Augen, andere
  Gruppenhälfte geht drum herum und berührt spielerisch. Am Ende sucht sich
  jede Person eine Partner:in berührt diese auf besondere Weise und reicht
  abschließend ihre Hände zum Fühlen. Nachher versucht jede Person anhand der
  Hände ihre Partner:in zu finden.
- Eine Gruppenhälfte liegt wild verteilt im Raum bäuchlings, andere
  Gruppenhälfte spielt Katzen und läuft mit Katzentatzen über liegende
  Körperrückseiten.
- Alle gehen wild durch den Raum, lenken erst Achtsamkeit nach innen, dann auf
  Fußsohle, dann auf Körper, dann auf Raum, dann auf Gruppe, gehen in
  Augenkontakt, halten diesen fest und lösen sich wieder, bleiben voreinander
  stehen, sehen einander in die Augen, lehnen sich Rücken an Rücken
  aneinander an, umarmen sich, streichen sich aus, schenken sich
  Aufmerksamkeit.
- Therapeutisches Fliegen (lunares Fliegen)
- Einander im Stehen streicheln, Gewebe ausdrücken, abklopfen

Beim Massieren: Achtsam in Handfläche und Blick für Ganzes haben, mit ganzem
Wesen lauschen (physische sanfter Blick zum Horizont).

## Einladung

Liebe Gemeinschaft,

kommenden Dienstag, 25. Januar, ist Raum für Thai-Yoga-Massage von 19 bis etwa 22:30 Uhr in der Naturheilpraxis Soluna, Münsterplatz 11. Gemeinsam schaffen wir einen Raum liebevoller achtsamer Begegnung durch Berührung. Wir erfahren das spirituelle Fundament von Thai-Yoga-Massagen, erleben die aktive, dienende, Raum haltende gebende Rolle und die passive, hingebungsvolle, geborgene empfangende Rolle.

Von außen betrachtet ist Thai-Yoga-Massage eine Kombination aus für die empfangende Person passiven vom Yoga entlehnten Streckstellungen, Dehnbewegungen, Gelenkmobilisationen, Druckpunkt- und Energielinienmassagen. Von innen betrachtet Tanzen gebende und empfangende Person miteinander einen heiligen Tanz, verschmelzen durch Berührung, gehen in Resonanz, behandeln einander, fließen miteinander durch Raum und Zeit, durch Gefühls-, Gedanken- und Körperwelten. Dadurch können Selbstheilungsprozesse auf allen Ebenen angestoßen und fortgesetzt werden.

Einen ersten Eindruck von außen könnt ihr euch von Krishnataki, einem Lehrer meiner Lehrer\*innen, unter https://www.youtube.com/watch?v=IQid9EsfMag verschaffen.

Ich freue mich über jede Person, die ich am Dienstag auf ihren ersten Thai-Yoga-Schritten begleiten darf.

Von Herzen
Simon

Am Rande: Am Dienstag unterrichte ich Thai-Yoga zum ersten Mal. Mogli wird mich unterstützen und Camillo bringt Erfahrung mit. Wegen des engen Zeitrahmens können etliche Themen nur im Ansatz gekostet werden.

## Über mich

Durch Geben und Empfangen entdeckte ich eine neue Art bei mir und im Kontakt zu
sein: Voll geistigem, körperlichem und seelischem Wohlbefinden, vollkommen
präsent im Moment, voll Achtsamkeit, Zufriedenheit, innerer Ruhe, innerem
Lächeln, Fröhlichkeit, ...

Nach einem Jahrzehnt verkopfter Tätigkeit und mehrjähriger Suche entdeckte ich
eine tiefe Glückseligkeit im Geben und Empfangen von Thai-Yoga-Massagen, im
liebevollen und achtsamen Körperkontakt, im nonverbalen Austausch zweier
Lebewesen, in der Verbindung von Herz zu Herz.

Durch Massagen kam ich wieder in Kontakt mit meinem ursprünglichen Wesenskern,
mit meinem inneren Kind, mit meinem Selbst, das unter etlichen Schichten
antrainierter Sozialisierung verborgen lag. Nach und nach vielen diese
Schichten von mir ab und es fühlte sich an wie eine Wiederauferstehung.

Schon von Kindesbeinen war ich begeistert von menschlichen Körpern:

- den vielfältigen unterbewussten Prozessen wie Herzschlag, Atmung, Verdauung
  und Heilkraft;
- der bewussten Wahrnehmung autonomer und willkürlicher Prozesse über
  Tiefensensibilität und Oberflächensensibilität wie die Lage des Körpers im
  Raum, den Spannungszustand von Gewebe und die Empfindung von Bewegung
  gezielte Bewegungen, das Halten von Gleichgewicht, das Empfinden von Druck,
  Berührung und Vibration, von Temperatur und von Schmerz;
- den mannigfaltigen Möglichkeiten den eigenen Körper einzusetzen, zu formen
  und anzupassen wie verbesserte Koordination, Kondition, Beweglichkeit,
  Ausdauer, Konzentration, Reaktionsschnelligkeit, Zeit-, Rhythmus-, und
  Gleichgewichtsgefühl.

Ich bewege mich von Herzen gerne. Zu Beginn meiner Jugendzeit begann ich Einrad
zu fahren, zu jonglieren und Akrobatik zu treiben. Ich rannte, kletterte auf
Bäume und Gebäude, wanderte, radelte, fuhr Skateboard und Inlineskates. In
meiner späten Jugend begann ich zu turnen. Im frühen Erwachsenenalter begann
ich zu klettern. Körperlichkeit vermittelt mir ein tiefes Gefühl von
Geborgenheit und Lebensfreude.

- Wunsch
- Vision
- Was tue ich dafür
- Was habe ich gelernt
- Was sind meine besonderen Qualitäten
- Welche Fertigkeiten nutze ich in meiner Arbeit für dein Wohl

## Cranio-sacrale Therapie

Cranio-sacrale Therapie erhöht die dem Körper zugrunde liegende Vitalität, die
Lebensenergie oder den Lebensatem, die das ganze Selbst durchdringt, und
erzeugt und erhält dadurch Gesundheit und Wohlbefinden auf allen Seinsebenen.
Sie fungiert von innen nach außen, vom Feinstofflichen zum Grobstofflichen, von
der Vitalität zum Bewegungsapparat.

Thai-Yoga-Massage fungiert von außen nach innen, vom Bewegungsapparat zur
Vitalität. Indem sie das Bewusstsein in den Körper lenkt, den Körper
geschmeidig macht, grobe Blockaden auflöst, vom Kreuzbein (Latein: Os sacrum)
zum Schädel (Latein: Cranium), werden feinstoffliche Bewegungen wahrnehmbar und
dadurch die cranio-sacrale Therapie vorbereitet.

Bei Menschen voll Tatendrang mit einem dominanten Sympathikus beginne ich mit
beruhigender cranio-sacraler Therapie. Bei antriebslosen Menschen mit einem
dominanten Parasympathikus beginne ich mit aktivierender Thai-Yoga-Massage.

Aufmerksamkeit ist der Schlüssel, ich fokussiere mich auf nichts im Besonderen,
bin umfassend präsent, wodurch magisches passiert, das ich im Raum und durch
die Hände wahrnehme.

Cranio-sacrale Anwendung entspannt auf allen Ebenen und begünstigt dadurch
befreiende spontane Bewegungen, das psycho-somatische System strampelt sich
frei.

Durch mentale Kraft beispielsweise durch Visualisierung wird Lebenskraft erhöht
und gelenkt und dadurch Vitalität gesteigert.

- vitality: the breath of life
- motility: inherent tissue motion

## Hintergrund

### Anekdotische Wirkungen {#anekdotische-wirkungen}

Eine Massage kreiert einen Raum, der die empfangende Person zur Selbstheilung
einlädt. Sie stößt innere Verarbeitungsprozesse an, begünstigt innere
Reorganisation, unterstützt innere Ausbalancierung und fördert inneren
Energieausgleich. Dadurch kann sich ein Zustand inneren Gleichgewichts der
sogenannten Homöostase einstellen.

Körper, Geist und Seele sind miteinander verwoben und stehen in ständigem
Austausch. Bei einer Massage wird dadurch über den Körper auch Geist und Seele
erreicht. Der Mensch ist ein psychosomatisches Lebewesen.

Der ganze Mensch ist im Fokus, kein psychosomatisches Symptom. Die persönliche
Erfahrung steht im Zentrum, keine klinische Diagnose. Der gegenwärtige Zustand
ist Gegenstand, keine Krankheitsgeschichte. Die Alltagswahrnehmung/-auswirkung
ist von Relevanz, keine medizinische Beschreibung. Massage und Diagnose sind
eins, sind miteinander verwoben, geschehen gleichzeitig und passen sich an die
während einer Sitzung auftretenden Änderungen an.

Gespräch zweier Lebewesen --- wahrhaftig, ehrlich, ehrfurchtsvoll, demütig.

Erkennen, zeigen, offenbaren.

Sei du selbst, lass los, lass fließen, befreie dich.

Durch Intention entstehen aus der äußeren Wahrnehmung innere Einsichten.

Achtsamkeit zu empfangen, gesehen zu werden wie man wirklich ist, ist ein
heilsames Geschenk.

Die empfangende Person darf sie selbst sein, darf sich zeigen, wird aufmerksam
und wertfrei als das vollkommene Lebewesen gesehen, das sie ist. Diese
liebevolle Achtsamkeit schafft Raum für Selbstheilung und ist ein großes
Geschenk.

Auf der körperlichen Ebene

- Flüssigkeiten in Fluss bringen
- Stauungen lösen
- Muskelentspannung / muskuläre Entspannung durch sanfte Dehnungen / Lösung von Muskel- und Faszienverspannungen
- Größere Dehnbarkeit und Gelenkbeweglichkeit
- Lymphentstauung / angeregte Blutzirkulation und Lymphfluss / Aktivierung der
  Durchblutung und des Lymphfluss
- Giftstoffausscheidung / Entgiftend
- Haltungsverbesserung / Verbesserung der körperlichen Ausrichtung
- Anregung der Tätigkeit innerer Organe / Stimulierung der inneren Organe
- Bessere neurologische Funktionstätigkeit / Verbesserung neurologischer Funktionen
- Mehr Vitalität
- Stärkung des Immunsystems
- Hilft bei langjährigen Verletzungen
- angeregte Verdauungstätigkeit und damit verbundene Stärkung des Immunsystems
- aktivere Organe durch Druck auf Energiepunkte (Akupressurpunkte
  oder Marmapunkte)
- verbesserte Flexibilität des Bewegungsapparates durch Behandlung des
  Bindegewebes (Faszien) / Verbesserung der Gelenkflexibilität und -mobilität
- Immunsystem stärken/kräftigen

Auf der mentalen Ebene

- Tiefenentspannung
- Beruhigung des Geistes
- tiefe Ruhe
- zur Ruhe kommen
- sich selbst kennenlernen/erfahren
- Körperbewusstsein schaffen, entdecken, wiederfinden
- Körpergefühl, Körperwahrnehmung und Körperbild verbessern
- Ankommen
- Präsenz
- mentale Entspannung durch achtsame Berührungen
- Harmonisierung des autonomen Nervensystems durch Ausgleich dessen zwei
  Bereichen, des sympathischen und parasympathischen Nervensystems (TODO
  Zusammenhang zwischen Energielinien/Meridianen/Shen Lines/Nadis Ida und
  Pingala nennen)
- Bewusstsein von Kopf in Körper bringen (Körperräume begeistern,
  Selbstwahrnehmung/Körperabbild verbessern)

Auf der emotionalen Ebene

- Emotionale Erleichterung / Lösen von Emotionen
- Lösung von Spannung und Stress / Spannungs- und Stressabbau
- Herz erwecken
- Gefühlsregulation
- Geborgenheitsgefühl durch Hebetechniken
- Resonanz (verstanden, gesehen, akzeptiert und angenommen fühlen;
  bekunden was ist)

Auf der spirituellen Ebene

- Lebendigkeit spüren
- Energieflussausgleich / Anregung des Energiefluss
- Verwobenheit/Einheit allen Seins erkennen

Auf mehreren Ebenen

- Kraft tanken
- Ganzen Menschen auf körperlicher, geistiger und seelischer Ebene berühren durch
  tiefes Einsinken
- Selbstheilung durch Aktivierung der Selbstheilungskräfte
- Homöostase/Harmonisierung/Reorganisation
- Verarbeitungsprozesse anstoßen
- verbessertes allgemeines Wohlbefinden
- körperliche und mentale Entspannung durch tiefen Druck
- beruhigter Atem und dadurch beruhigter Geist und ausgeglichene emotionale
  Welt
- aktiverer Parasympathikus in tiefer Entspannung, sodass die inneren
  Organe vermehrt durchblutet werden und der Stoffwechsel angeregt wird
- Energien/Emotionen/Flüssigkeiten in Fluss bringen, Blockaden/Stauungen lösen,
  Reinigungs- und Verarbeitungsprozesse anstoßen --- durch Reflexzonen,
  Energiepunkte, Energiekanäle (Shen Lines/Nadis/Meridiane; myofasziale
  Ketten)

Inspiriert von Abschnitt „Wirkungen der traditionellen Thai Massage“ des
Artikels [Thai Yoga Massage --- Der heilige
Tanz](https://www.yogamehome.org/yoga-blog/artikel/thai-yoga-massage-der-heilige-tanz).

Eine Massage berührt alle Energiehüllen (Sanskrit: koṣa), die das reine Selbst
(Sanskrit: ātman), die Seele, den Lebensfunken eines Menschen, umgeben. Sie
bilden die drei Körper (Sanskrit: śarīras), durch die sich das reine Selbst in
seinem/ihrem inkarnierten Leben (Sanskrit: jīvana) ausdrückt. Und sie
beinhalten verschiedene Aspekte subjektiver körperlicher, mentaler, emotionaler
und spiritueller Wahrnehmung.

[comment]: # "Diese Beschreibung ist unklar."

1. Die Nahrungshülle (Sanskrit: annamayakośa) bildet den physischen oder
   grobstofflichen Körper (Sanskrit: sthūla-śarīra). Er besteht aus den sieben
   Geweben (Sanskrit: dhātu), nämlich Mark, Knochen, Fett, Fleisch, Blut,
   Unterhaut und Oberhaut, welche sich aus den fünf Elementen Erde, Wasser,
   Feuer, Luft und Äther zusammensetzen.
1. Die Energiehülle (Sanskrit: prāṇamayakośa), geistig-emotionale Hülle
   (Sanskrit: manomayakośa) und intellektuelle Hülle (Sanskrit: buddhimayakośa
   oder vijñānamayakośa) bilden den astralen oder feinstofflichen Körper
   (Sanskrit: liṅga-śarīra oder sūkṣma-śarīra). Er besteht aus den fünf
   Lebensenergien (Atmen, Ausscheiden, Verdauen, Verteilen, Mitteilen), den
   fünf Handlungsvermögen (Sprechen, Gehen, Greifen, Ausscheiden und
   Fortpflanzen), den fünf Sinnen/Wahrnehmungsvermögen (Sehen, Hören, Riechen,
   Schmecken und Tasten), und den vier Funktionen des Geistes (einfaches Denken
   und Wahrnehmen und Fühlen anhand des Denkorgans (Sanskrit: manas),
   Analysieren und Bestimmen und Unterscheiden anhand der Vernunft und des
   Intellekts und des Willens (Sanskrit: buddhi), Erinnern und Aufmerken und
   Vergleichen anhand des Gedächtnis (Sansrkit: citta), Identifizieren als Ego
   und Ich (Sanskrit: ahaṃkāra)).

   [comment]: # "Die Funktionen des Geistes stammen von https://wiki.yoga-vidya.de/Antahkarana"

   Die Energiehülle besteht aus den fünf Lebensenergien, welche Körper und
   Geist beleben, und beinhaltet die Energiekanäle und Energiezentren. Die
   geistig-emotionale Hülle besteht aus dem Geist, dem Unterbewusstsein, den
   fünf Handlungsvermögen und den fünf Sinnen/Wahrnehmungsvermögen. Die
   intellektuelle Hülle besteht aus den vier Funktionen des Geistes, der
   Intelligenz, durch die das Selbst denkt, wahrnimmt, fühlt, analysiert,
   bestimmt, entscheidet, unterscheidet, sich erinnert, aufmerkt, vergleicht
   und sich identifiziert.

   [comment]: # "Diese Beschreibung ist unklar."

1. Die Wonnenhülle (Sanskrit: ānandamayakośa) bildet den Kausalkörper
   (Sanskrit: kāraṇa-śarīra). Er ist die Ursache des grobstofflichen und
   feinstofflichen Körpers.

Inspiriert von [Kosha](https://wiki.yoga-vidya.de/Kosha).

### Berührung (?) {#beruehrung}

Fläche

- _flächig_: Handfläche und Fußsohle
- _punktuell_: Daumen und Großzeh
- _geradlinig_: Speichengrübchen (Bereich zwischen Daumen und Zeigefinger, auch
  genannt Tigermaul), Handaußenkante und Fußkanten
- _?:_ Ellbogen und Knie

Techniken

... weniger Technik, mehr fühlen! Intuitive Berührung!

- Spazierender Handflächendruck (Englisch: palming): Ganze lockere Hand samt
  Fingern wandert rhythmisch einer Energielinie entlang, passt vor jedem
  langsamen tiefen Einsinken senkrecht zur Oberfläche durch Lehnen aus
  Körperschwerpunkt ihre Passform perfekt an, vermeidet Hautschmerzen durch
  Einholen von Hautkredit vor Einsinken falls nötig und gebende Person
  lauscht mit ihrem ganzen Wesen und allen Sinnen.
- Spazierender Daumendruck (Englisch: thumbing): Der weiche Teil des
  ganzen Daumenendglieds wander rhythmisch einer Energielinie entlang.
- Spazierender Fußflächendruck (Englisch: walking meditation oder footing): Das
  gebogene Längsgewölbe der Fußsohle wandert rhythmisch einer Energielinie
  entlang mit für erhöhte Stabilität auf dem Boden aufgesetzter Ferse.
- Schaukeln/Schwingen in Eigenfrequenz des Körpers (Englisch: rocking): Bringe
  den Körper durch rhythmisches und sanftes Schieben mit Händen oder Füßen in
  ein langsames harmonisches Schwingen mit einer festen maximalen Auslenkung,
  indem du das Zurückschwingen in eine Richtung durch sanften Druck
  verstärkst um die Amplitude zu erhalten --- eine stehende oder wandernde
  Welle entsteht senkrecht zur Auslenkungsrichtung. Die Hände oder Füße mit
  denen die Bewegung aufrecht erhalten wird, bleiben durchgängig in flächigem
  Kontakt und wandern möglicherweise den Körper entlang.
- Induziertes Schaukeln/Schwingen/Vibrationen oder Obertöne (Englisch:
  harmonics): Baue eine Vorspannung auf um präzise mit dem Zielraum in
  reziproken Kontakt zu treten. Induziere feine schnelle Wellen die quer- oder
  längs durch den Körper bis zu ihrem Ziel wandern. Durch den reziproken
  Kontakt von Ursprungs- und Zielraum wirkt sich eine Änderung in einem dieser
  Räume direkt auf den anderen aus.
- Rotation (Englisch: rotation): Beginne behutsam in Gelenk öffnender
  Drehrichtung mit kleinem Radius immer größer werdend, lote die
  Beweglichkeitsgrenzen aus, erweitere sie und verkleinere den Radius wieder
  bis zum Stillstand. Löse Reibungsstellen durch kleine Rotationen um jene
  Stellen in die entgegengesetzte Drehrichtung auf. Hat die empfangende Person
  Schwierigkeiten loszulassen, komprimiere das Gelenk zuvor um ihm Vertrauen
  und Geborgenheit zu geben.
- Kompression/Approximation (Englisch: compression): Drücke den Gelenkkopf in neutraler
  Position in die Gelenkpfanne. Höre zu ob und wie sich das Gelenk bewegen
  möchte. Sei dir dabei der unterschiedlichen potentiellen
  Bewegungsmöglichkeiten von Kugel-, Schanier-, Dreh- und Mischgelenken
  bewusst.
- Längung/Traktion (Englisch: decompression): Schaffe Raum in einem Gelenk, indem du
  behutsam den Gelenkkopf von der Gelenkpfanne wegziehst. Länge eine
  Energielinie, Muskelkette oder myofasziale Kette, indem du sie behutsam durch
  Zug in Dehnung bringst. Gehe langsam hinein und langsam heraus. Beim
  Herausgehen lasse Gelenke und Gewebe dahin gleiten wo sie oder es hin
  möchten. Lasse auf eine Längung eine Kompression folgen.
- Ausstreichen/Gleiten (Englisch: gliding)
- Abklopfen/Abklatschen (Englisch: thai chops)
- Induzierter Blutstau (Englisch: blood stop)
- Impuls (Englisch: impulse)
- Zurückschnellen (Englisch: recoil)
- Pumpen (Englisch: pumping)
- (Englisch: 1st and 2nd rebound)
- (Englisch: reciprocal tension and slack): Durch ziehen oder drücken.
- Fixpunkt (Englisch: fixed point)
- (Englisch: muscle-energy or contract-relax technique or proprioceptive
  neuromuscular facilitation)
- Faszienlauschen (Englisch: faciae listening): Gebende Person agiert als
  Drehpunkt, Angelpunkt, Stützpunkt (Englisch: fulcrum)
- Visualisierung (Englisch: visualization): Beispielsweise Visualisierung von
  Flüssigkeitsbewegung im empfangenden Körper.
- Integration lokaler Veränderungen in Gesamtsystem durch globale Arbeit:
  Beispielsweise punktuellen Druck mit flächigem Druck vor- und nachbereiten.
  Und flächigen Druck mit flächigem Ausstreichen und/oder quer- oder
  längsverlaufende Ganzkörperwellen.
- Mutter-/Kindhand: Mutterhand lauscht Veränderungen an festem Ort während
  Kindhand spaziert, spielt, frohlockt.
- Umhüllen/Umschließen/Umrahmen (Englisch: enveloping, sandwich):
  Beispielsweise mit den Händen das Schultergelenk umschließen. Spendet
  Geborgenheit, Sicherheit, Halt.
- Verbinden (Englisch: connect): Beispielsweise durch Berührung des
  Hinterhauptbeins (Latein: Os occipitale, kurz Occiput) mit einer Hand und
  des Kreuzbeins (Latein: Os sacrum, kurz Sakrum) mit der anderen Hand, die
  körperliche und energetische Verbindung dieser beiden Strukturen bewusst
  und erfahrbar machen. Oder durch Berührung des Bauchs (Latein: Abdomen) mit
  einer Hand und des Kreuzbeins (Latein: Os occipitale, kurz Occiput) mit der
  anderen Hand, deren Verbindung und den Raum dazwischen aufzeigen.
- Atem induzieren: Steuerung der Atmung der empfangenden Person durch rhythmische
  Kompression des Brustkorbs an mannigfaltigen Orten mit beiden Händen
  gleichzeitig oder alternierend Impulse gebend. Verwandt zu Impuls,
  Zurückschnellen und Pumpen am Brustkorb.
- Akupressur
- (Englisch: cavity dissociation): Beispielsweise der Lungen-Herz-Raum ist vom
  Bauchraum durch das Zwerchfell getrennt. Indem die gebende Person das
  Zwerchfell beim Ausatmen der gebenden Person nach unten drückt und beim
  Einatmen nach oben zieht, also beide Male entgegen der eigentlichen
  Bewegungsrichtung des Zwerchfells, werden diese beiden Räume weiter
  voneinander getrennt durch das Lösen faszialer Verklebungen zwischen dem
  Zwerchfell und umliegendem Gewebe und durch die Bewusstmachung dieser beiden
  Räume. Dies verbessert den Atem.

"harmonics" und "rocking" sind "skeleton symphony" und "oscillatory
techniques", basieren auf Pendel- und Federmechanik. Es gibt "oscillatory
stretching technique" (zyklisches Dehnen) zur Längung und "oscillatory pumping
technique" ("intermitent compression") um Flüssigkeiten in Bewegung zu bringen.

Der Effekt einzelner Techniken wird durch den gezielten Einsatz des Atems der
gebenden Person verstärkt.

Metaphern

- Hand ruht auf Bauchdecke wie Schiff auf See (sanft, absichtslos, anschmiegsam)
- Finger prasseln wie Wassertropfen auf den Körper (sanft, beruhigend, verspielt)
- Über Körper streichen wie beim Streicheln einer Lieblingskatze (sanft,
  neckisch, fließend)
- Körperteil anheben wie beim Aufheben eines Katzenwelpen (sanft, behutsam,
  achtsam)
- Handflächen wandern über Körper wie Katzentatzen (sanft, anschmiegsam,
  selbstbewusst)

- Wasser: Wasserberührung aktiviert das Sakralchakra, verkörpert Sinnlichkeit
  und Fürsorge. Ihre Qualität ist fließend, anschmiegsam und verspielt. Sie
  besteht aus langen, langsamen, zyklischen und fließenden Bewegungen. Sie
  wird ausgeführt mit der ganzen Handfläche ohne Druck.

  Techniken: Swinging, Rocking, Harmonics, Shaking, Ausstreichen

  Geschwindigkeit: Mittelschnell

  Strukturen: Flüssigkeit in Bindegewebe, Faszien, Organe
- Erde: Erdeberührung aktiviert das Kronenchakra, kombiniert Rauheit und
  Stabilität, berührt tiefe Gewebsschichten und Knochen. Ihre Qualität ist
  erdig, stabilisierend und tief. Sie wird durch langsames, allmähliches,
  eindeutiges, flächiges, selbstbewusstes und tiefes Einsinken mit der
  Handfläche oder dem Unterarm ausgeführt. Oder durch Greifen und Kneten
  fleischigen Gewebes.

  Techniken: Palming, Thumbing, Footing, Kneeing, Elbowing, Blood Stop,
  Einsinken

  Geschwindigkeit: Zeitlupe

  Strukturen: Muskeln, Knochen, Bindegewebe, Faszien (alles strukturgebende,
  feste)
- Luft: Luftberührung aktiviert das Herzchakra, regt Liebe und feine Sinnlichkeit
  an. Ihre Qualität ist luftig, feenhaft und leicht. Sie wird ausgeführt mit
  den Fingerkuppen, dem Atem, der Haare, einem Tuch, berührt und berührt
  nicht.

  Techniken: Fingerspitzen, wie Wasser welches von Fingern tropft (Tautropfen)

  Geschwindigkeit: Langsam

  Strukturen: Feine Berührungssensoren, Lunge?, Verdauungsapparat?,
  Bauchorgane?
- Feuer: Feuerberührung aktiviert das Bauchnabelchakra. Ihre Qualität ist
  schnell, intensiv, spitz und stetiger Wandel. Sie kann Reiben, Kratzen,
  Beißen, Kneifen, Ziehen und Klatschen sein. Energie baut sich rasant auf
  als Amüsement mit Kichern oder Lachen. Oder als Widerstand mit Abwehr und
  Bewertung.

  Techniken: Impulse, Recoil, Vibrationen, Thai Chops (Handkante),
  (Aus)Klopfen, Trommeln (Faust)

  Geschwindigkeit: Rasant

  Strukturen: Herz-Kreislauf-System, Vitalität von Organen, Aktivierung des
  sympathischen Nervensystems, Gehirnaktivierung
- Äther: Ätherberührung aktiviert das Kehlkopfchakra. Sie ist berührungslos auf
  der physischen Ebene und berührt tief auf der energetischen Ebene. Die
  Hände bewegen sich mit Abstand über dem Körper dessen Form folgend, bewegen
  Energie, versetzen sie in Kreise, füllen sie auf oder ziehen sie ab. Die
  Berührung kann als leichtes Kribbeln wahrgenommen werden.

  Techniken: Craniosakral, Kopfhalten, Kreuzbeinhalten, Energietechniken,
  energetische Berührung, Listening, fast unbewegt, über Körper schweben

  Geschwindigkeit: Stille

  Strukturen: Energetischer Körper

Solare/Lunare Berührung

Aus dem Shiatsu: Ein Bedürfnis (japanisch: kyō) entwickelt sich an einer Stelle
im psycho-somatischen System und sendet Suchboten (japanisch: jitsu) nach
Befriedigung aus. Finden die Suchboten den Gegenstand des Bedürfnisses, so
transportieren sie dieses zur bedürftigen Stelle und befriedigen sie damit.
Bedürftige Stellen können weich sein und ausgesendete Suchboten als von dieser
Stelle ausstrahlende Verhärtungen tastbar. Wenn die bedürftige Stelle durch
Berührung befriedigt wird, so löst sich die Verspannung von selbst auf. Suche
also tendenziell weiche Stellen, die sich nach Berührung sehnen und sinke tief
ein. Aufgepasst: Ein kyō kann sich oberflächlich auch als jitsu tarnen und
umgekehrt.

Art/Qualität

- rhythmisch, fließend, ohne Eile, ohne Pause
- tiefes behutsames Einsinken in Strukturen (beispielsweise bei
  Handflächendruck durch Lehnen aus Körperschwerpunkt langsam tief einsinken)
- anschmiegsam, voll, perfekte Passform (beispielsweise bei
  Handflächendruck mit ganzer Handfläche samt lockeren Fingern berühren)
- senkrecht zur Oberfläche
- Hautkredit holen
- beherzt und glühend
- klar, rein, eindeutig und selbstbewusst
- wertfrei und absichtslos
- weniger ist mehr
- von Herz zu Herz
- Ganzkörpereinsatz (auch Geist und Seele)
- Schwerkraft nutzen
- Aus Körpermitte, Körperschwerpunkt, unterem energetischem Zentrum (Pinyin: Xia Dāntián), vitaler/energetischer Körpermitte (Hara oder tanden).

Geisteshaltung

- Die vier unermesslichen Geisteshaltungen: Liebevolle Güte, Mitgefühl,
  Mitfreude und Gleichmut
- Präsenz/Geistesgegenwart
- Achtsamkeit (mit allen Sinnen achtsam und mit ganzem Wesen lauschen)
- Mit Intention/Eindeutigkeit
- Selbstbewusstheit
- Wahrhaftigkeit und Ehrlichkeit
- Ehrfurcht und Demut
- Bequemlichkeit
- Entspannung
- Meditation
- Empfänglichkeit und Offenheit

Aufgaben

- Heilsamen Raum schaffen und halten
- In Resonanz/Verbindung gehen
- Geborgenheit vermitteln
- Bedürfnisse erkennen und befriedigen
- Zu selbstbestimmtem Leben verhelfen (Selbsterkenntnis, Selbstentfaltung,
  Selbstentdeckung, Selbstwirksamkeit)
- Mit ganzem Wesen lauschen

Prinzipien

- Berührung: Die perfekte Berührung finden --- Zeit, Ort, Fläche, Qualität,
  Passform, Intensität, Tiefe, Fülle, Präzision, Fürsorge, Liebe,
  Sensibilität/Feingefühl, Sicherheit. Der empfangenden Person das Gefühl von
  Sicherheit und Liebe geben. Die ganze Hand aktiv mit entspannten und
  weichen Fingern.

  > ... sehende, fühlende, denkende Finger ...

  -- Aus dem Englischen nach William Garner Sutherland (1873-1954)

- (An-)Teilnahme: Vollkommen verbunden mit dem was wir tun. Mit unserem
  Verstand, unserem Herzen und unserem Körper. Im gegenwärtigen Moment sein.
  Offen sein für alles was untere unseren Händen geschieht. Bereit uns an die
  Bedürfnisse der empfangenden Person anzupassen.
- Körperhaltung: Eine bequeme Haltung einnehmen um achtsam sein zu können.
- Zentrierung: Die Verbindung zu uns selbst aufrechterhalten. Geerdet,
  verbunden, ausbalanciert.
- Schwerkraft: Mit unserem Körpergewicht durch die Schwerkraft in das Gewebe
  sinken statt Muskelkraft zu verwenden. Dadurch können wir tiefer mit Anmut
  und ohne Kampf sinken. Es ist nicht möglich zu tief zu sinken aber zu
  schnell!
- Tanz: Die Schönheit des Übergangs. Sich von einer Position zur nächsten in
  einer Einheit bewegen. Geschmeidig und flüssig. Mit so wenig Aufwand als
  möglich. Mit Rhythmus und Anmut.
- Gebet und Dankbarkeit: Frage nach Führung. Lass deine eigene Geschichte
  zurück. Halte den Raum und bleibe mitfühlend und gleichmütig. Lass dich
  nicht in die emotionalen Suppe hineinziehen. Beende jede Sitzung mit einem
  Gebet, Dankbarkeit und Demut.

Inspiriert von [7 Basic Principles of Thai
Massage](https://www.zoltangyorgyovics.com/the-principles).

Bequemlichkeit (bequeme Haltung ohne Spannung, Englisch: body comfortable),
Stabilität (der gebenden Person und des Kontaktpunkts), Leichtigkeit (durch die
Nutzung der Schwerkraft, von Hebeln und des Atems).

Selbstbewusstheit, Kraft und Erdung strömt aus der Erde in den Körper.
Intuition, Offenheit und Inspiration strömt vom Himmel in den Körper. Beide
vertikale Strömungen vereinigen sich im Herzen. Von dort strömen sie horizontal
von Herz zu Herz.
