---
title: Thai-Yoga-Massage
images:
  - images/landscape/side-stretch.jpg
summary: Ein heiliger Tanz zweier Lebewesen in Resonanz
description: >
  Thai-Yoga-Massage ist ein Dialog ohne Worte bei dem durch achtsame Berührung
  und aufmerksames Zuhören mit den Händen kommuniziert wird. In voller Präsenz,
  Achtsamkeit und mit klarer Absicht wird aus dem Herzen voll liebender Güte,
  Mitgefühl, Mitfreude, Gleichmut und Dankbarkeit Berührung geschenkt.
home:
  title: Liebevolle Achtsamkeit
  summary: Einfühlsam, meditativ, rhythmisch, tiefgehend, bodennah und angekleidet
short: |
  Thai-Yoga-Massage ist ein Dialog ohne Worte bei dem durch achtsame Berührung
  und aufmerksames Zuhören mit den Händen kommuniziert wird. In voller Präsenz,
  Achtsamkeit und mit klarer Absicht wird aus dem Herzen voll liebender Güte,
  Mitgefühl, Mitfreude, Gleichmut und Dankbarkeit Berührung geschenkt. Es ist
  eine einfühlsame, berührende, tiefgehende, rhythmische, ganzheitliche,
  bodennahe und angekleidete Ganzkörpermassage. Ein verbindender, inniger,
  meditativer, fließender und heiliger Tanz zweier Menschen. Kommunikation
  durch Berührung. Ein heilsamer Dialog.
quote: |
  > Der Weg liegt nicht im Himmel. Der Weg liegt im Herzen.

  --- Siddhartha Gautama (560-480 vor Christus)
effects:
  title: Anekdotische Wirkungen
  summary:
    Körperliche Geschmeidigkeit, mentale Klarheit, emotionale Lösung und spirituelles
    Wachstum
  description: |
    Bei einer Massage entsteht ein heilsamer Raum, der dich einlädt zu
    Tiefenentspannung, Ausgeglichenheit und innerer Ruhe. Dein Körper wird
    darin gefördert geschmeidiger, mobiler, dehnbarer und durchlässiger zu
    werden. Dein Stoffwechsel wird angeregt, Flüssigkeiten können freier
    fließen, Emotionen können sich lösen --- du fühlst dich lebendiger.
  quote: |
    > Blicke über deine Gedanken hinaus und trinke den reinen Nektar dieses
    > Augenblicks.

    --- Aus dem Persischen nach dem persischen Mystiker Rumi (1207-1273)
  anchor: anekdotische-wirkungen
  boxes:
    - title: Körperlich
      summary: Erhöhung der Beweglichkeit, Anregung des
        Flüssigkeits&shy;austauschs, Verbesserung der Organ&shy;tätigkeit und
        Stärkung des Immunsystems
      list:
        - Erhöhung der Beweglichkeit, Gelenkigkeit und Dehnfähigkeit,
          Entspannung der Muskulatur und des Bindegewebes und Verbesserung
          der körperlichen Ausrichtung
        - Anregung der Zirkulation, des Transports und des Austausches von
          Flüssigkeiten wie Blut, Lymphe, Gewebeflüssigkeit, Gelenkflüssigkeit
          und Gehirn&shy;rückenmarks&shy;flüssigkeit
        - Verbesserung der Tätigkeit innerer Organe wie des
          Herz-Kreislauf-Systems, des Blut- und Abwehr&shy;systems, des
          Hormon&shy;systems, des Atemtrakts, des Verdauungs&shy;systems und des
          Urogenital&shy;systems
        - Immunsystem&shy;stärkung, Vitalitäts&shy;erhöhung, Harmonisierung, Verbesserung
          neurologischer Funktionen und Entgiftung.
      anchor: koerperliche-wirkungen
    - title: Mental
      summary:
        Tiefen&shy;entspannung, Verbesserung der Körper&shy;wahrnehmung und
        Harmonisierung des Nerven&shy;systems
      list:
        - Tiefen&shy;entspannung, Beruhigung des Geistes, Erweckung von Erinnerungen
          und Anstoßen von Fantasie&shy;reisen
        - Verlagerung des Bewusstseins vom Kopf in den Körper und Verbesserung
          des Körper&shy;gefühls, der Körper&shy;wahrnehmung und des Körper&shy;bilds
        - Harmonisierung des autonomen Nerven&shy;systems durch Ausgleich dessen zwei
          Bereichen, des sympathischen und para&shy;sympathischen Nerven&shy;systems
      anchor: mentale-wirkungen
    - title: Emotional
      summary: Aufhellung der Stimmung, Erweckung positiver Emotionen und
        Lösung negativer Emotionen
      list:
        - Aufhellung der Stimmung, Erweckung des Herzens, Empfinden von
          Geborgenheit, Verbundenheit und Selbstwert
        - Eintreten positiver Emotionen wie Freude, Dankbarkeit, Zufriedenheit,
          Hoffnung, Vergnügen, Inspiration, Überraschung und Liebe.
        - Abbau von Stress, Anspannung und Besorgnis
        - Lösung, Verarbeitung, Abfluss und Ausgleich negativer Emotionen wie
          Angst, Wut, Trauer, Scham, Neid und Eifersucht
      anchor: emotionale-wirkungen
date: 2022-02-10T13:05:52+0100
layout: massage
---

> Die wichtigste Stunde ist immer die Gegenwart. Der bedeutendste Mensch ist
> der, der Dir gerade gegenüber sitzt. Das Notwendigste ist immer die Liebe.

--- Meister Eckhart (1260-1328)

## Geschichte {#geschichte}

Die traditionelle Thai-Massage wurde im fünften Jahrhundert vor Christus vom
nordindischen Arzt Jivakar Kumar Bhaccha (Pali: Jīvaka Komārabhacca; Sanskrit:
Jīvaka Kumārabhṛta) praktiziert. Er war ein Zeitgenosse des historischen
Buddhas (Pali: buddho; Sanskrit: buddha) Siddhartha Gautama (Pali: Siddhattha
Gotama; Sanskrit: Siddhārtha Gautama) und betreute diesen und dessen
Mönchsgemeinde ärztlich. Im dritten oder zweiten Jahrhundert vor Christus wurde
die Massage nach Südostasien überliefert. Ihre thailändischen Bezeichnung ist
„Nuat Phaen Boran“ was „Massieren nach uraltem Muster“ bedeutet.

Sie besteht aus passiven vom Yoga entlehnten Streckstellungen, Dehnbewegungen,
Gelenkmobilisationen, Druckpunkt- und Energielinienmassagen. Sie ist eine
Kombination aus assistierten Yogastellungen, ayurvedischen Prinzipien und
Akupressur. Yoga (Sanskrit: yoga), übersetzt „Joch“, ist eine philosophische
indische Lehre bestehend aus körperlichen und geistigen Übungen zur Sammlung,
Konzentration, Kontemplation und Meditation mit dem Ziel der Erleuchtung, der
Selbsterkenntnis und Selbstwirksamkeit; insbesondere zur Erkenntnis der Einheit
von Körper, Geist und Seele. Diese Lehre ist verwoben mit Ayurveda (Sanskrit:
āyurveda), übersetzt „Lebenswissen/-weisheit“, einer ganzheitlichen indischen
Heilkunst zur Erhaltung der Gesundheit durch physische, mentale, emotionale und
spirituelle Lebensweise und Praxis.

Unter dem Namen Thai-Yoga-Massage gelangte die traditionelle Thai-Massage in
den Westen. Dort wurde sie unter anderen von Asokananda (Harald Brust,
1955-2005) und seinem Team, genannt „Sunshine Network“, praktiziert, gelehrt
und weiterentwickelt. Sie wurde zu einem physischen und energetischen heiligen
Tanz zweier Menschen in inniger Verbindung.

Meine Grundausbildung bekam ich von [Julia Deka](https://yolaya.de),
osteopathische Techniken lernte ich von [David Lutt](http://lulyani.com/en/)
und cranio-sacrale Anwendungen von [Vangelis
Varis](https://www.facebook.com/vagelis.varis). Für meine Lehrer:innen und
deren Breitschaft ihr Wissen zu teilen empfinde ich tiefe Dankbarkeit.

> Die einzige Art zu leben besteht darin, jede Minute als unwiederholbares
> Wunder zu akzeptieren

--- Aus dem Englischen nach [Tara Brach](https://www.tarabrach.com) (1953-heute)

## Die vier unermesslichen Geisteshaltungen {#die-vier-unermesslichen-geisteshaltungen}

Die Basis einer Massage bilden die vier buddhistischen Geisteshaltungen anderen
Lebewesen gegenüber genannt „Die vier Wohnstätten Brahmas“ (Pali und Sanskrit:
brahmavihārā) und „Die vier unermesslichen Geisteshaltungen“ (Pali: appamaññā;
Sanskrit: apramāṇa):

1. Liebende Güte oder liebevolle Achtsamkeit (Pali: maitrī; Sanskrit: mettā):
   Wohlwollende und nicht-anhaftende Liebe aus Interesse am Glück aller
   Lebewesen.
2. Mitgefühl (Pali und Sanskrit: karuṇā): Anteilnahme an und Einfühlung in das
   Leben aller ohne mitzuleiden.
3. Mitfreude oder anteilnehmende Freude (Pali und Sanskrit: muditā): Die Gabe
   freudvolle Momente aller Lebewesen zu teilen unabhängig von der eigenen
   Gemütsverfassung.
4. Gleichmut (Pali: upekkhā; Sanskrit: upekṣā): Gelassen, ohne Anhaftung und
   ohne Wertung allen Lebewesen begegnen.

Diese Geisteshaltungen kultiviere ich durch regelmäßige Praxis von
Einsichtsmeditation (Pali: vipassanā-bhāvanā), insbesondere zu Beginn und
während einer Massage. Dadurch eröffnet und hält sich ein heilsamer Raum für
dich, der mich mit einschließt und das ganze Universum. Ein Raum, in dem du
einfach du selbst sein darfst, loslassen darfst, nichts tun musst, nichts
leisten musst, einfach sein. Ein Raum, in dem du tief in dich eintauchen
darfst, dich auf neue Art wahrnehmen, entdecken, erkunden und erleben darfst.
Ein Raum, in dem du als das wundervolle Wesen, das du bist, wahr- und
angenommen wirst. In diesem Raum gehen wir in Resonanz, verbinden uns von Herz
zu Herz, verschmelzen wir in einem heiligen Tanz.

Liebende Güte oder liebevolle Achtsamkeit macht mich wohlwollend und aufmerksam
lauschen auf alles was dein Sein in der Welt im jedem Moment mir mitteilen
möchte. Mitgefühl macht mich feinfühlig und einfühlsam gegenüber dem Ausdruck
deiner Prozesse auf allen Ebenen menschlicher Existenz. Mitfreude macht mich
freudvoll die inneren und äußeren positiven Lebensbewegungen wahrnehmen ---
deine ureigene Lebendigkeit, Lebensausdruck und Lebenssituation. Und Gleichmut
macht mich für die Dauer der Sitzung dir dienen, ohne selbst in den Strudel
deiner Prozesse zu geraten und ohne eigene Absichten für mich zu verfolgen.

Thai-Yoga ist die physische Manifestation liebender Güte. Liebende Güte
erwächst aus dem tiefen Verständnis der wechselseitigen Abhängigkeit aller
Lebewesen. Diese Abhängigkeit entsteht aus dem gemeinsamen Ziel ein
Lebensgefühl wahrer Glückseligkeit zu erreichen. Thai-Yoga zu geben ist gelebte
Spiritualität, etabliert Gefühle der Verbundenheit, der Verwobenheit, kurz der
Einheit aller Wesen --- ein gemeinsamer Körper, ein Geist, eine Seele.

> Erleuchtung bedeutet die Entscheidung, eher im Zustand der Gegenwart als in
> der Zeit zu verweilen. Es bedeutet, Ja zu sagen zu dem, was ist.

--- Aus dem Englischen nach [Eckhart Tolle](https://eckharttolle.com) (1948-heute)
