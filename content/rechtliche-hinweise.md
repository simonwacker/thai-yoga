---
title: Rechtliche Hinweise
layout: single
---

Bei meinen Wellnessmassagen handelt es sich _nicht_ um Heilbehandlungen. Der
Besuch bei mir ersetzt _nicht_ den Besuch bei Ärzt:innen oder
Heilpraktiker:innen.

Die Einzelanwendungen und Manualinhalte dienen nicht dazu, Diagnosen,
Behandlungen oder Therapieempfehlungen zu geben.

Die Inhalte sind kein Ersatz für eine medizinische oder psychologische
Behandlung und stellen auch keine Empfehlung dar, eine solche Behandlung zu
unterlassen oder abzubrechen.

Auch wenn es so scheinen sollte, als ob durch die Teilnahme oder Anwendung eine
Verbesserung eintritt, ist das nicht als Hinweis zu werten, dass eine
medizinische Behandlung unnötig ist.

Ich weise darauf hin, dass es ausschließlich Berufsgruppen mit Erlaubnis zur
Ausübung der Heilkunde vorbehalten ist, Diagnosen zu stellen, Medikamente zu
verordnen sowie Krankheiten zu behandeln.

Meine Angebote ersetzen keine medizinische oder therapeutische Behandlung,
sondern dienen nur der Entspannung und einer möglichen Aktivierung der
Selbstheilungskräfte.

Alle angegebenen Preise sind Endpreise, sie werden nicht von Krankenkassen
übernommen und sind gemäß § 19 UStG von der Umsatzsteuer befreit.
