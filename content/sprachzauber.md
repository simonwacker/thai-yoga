---
title: Sprachzauber
summary: Yoga sprachlich begleiten
date: 2024-05-21T08:05:52+0100
layout: single
table_of_contents: true
---

_Leitstern_: Die übergeordnete Frage lautet "Wer bin ich?". Diese Frage ist der
Kern des spirituellen Wegs, durchdringt das spirituelle Wachstum vom Anfang bis
zum Ende, hallt von allen Wänden wider, liegt überall in der Luft, ist der
Leitstern allen Übens. Der Weg durch das Ich führt zum höheren Selbst, zur
Erkenntnis von König "Raja".

_Der achtgliedrige Pfad des Yoga_: Durch Yamas kultivierst du eine
lebensbejahende Haltung nach außen. Durch Niyamas jene nach innen. Durch Asanas
erkennst du deinen Körper. Durch Pranayama erkennst du die Beziehung zwischen
Atem und Geist und erfährst deine Lebendigkeit als fortwährenden Fluss. Durch
Pratyahara erkennst du deine Innenwelt und die Beziehung zwischen Wahrnehmung
und Reaktion --- du kommst vom reaktiven zum reflektierten Handeln. In Dharana
erkundest du Geist und dessen Lenkung. In Dhyana erkundest du
Bewusstseinszustände.

## Begleitung

_Verbundenheit und Anbindung_: Die begleitende Person ist mit sich, dem
gegenwärtigen Moment, der gerade angeleiteten Übung und den Mitübenden
verbunden. Aus dieser Verbundenheit heraus, aus Urvertrauen und aus Hingabe ist
sie ein Kanal für Inspiration, Kreativität und Schöpfer:inkraft. Die richtigen
Worte aus dem Fundus des Universums strömen von selbst durch sie hindurch,
durch ihren Mund und erreichen durch den Raum die Mitübenden. Es benötigt diese
Anbindung an das höhere Selbst um wahrhaftig da zu sein und um das Gegenüber in
der Tiefe zu berühren. Wie erfährst du als Mitübende:r es, wenn du auf diese
verbundene Weise angeleitet wirst?

_Verbindende Grenzen_: Wenn ich mit mir verbunden bin und du mit dir verbunden
bist, dann können wir miteinander verbunden sein. Persönliche und soziale
Verbundenheit hängen zusammen. Wenn mir meine Grenzen bewusst sind und dir
deine Grenzen bewusst sind, dann können wir uns an unseren Grenzen begegnen ---
Grenzen verbinden.

_Verbale Begleitung_: Durch den bewussten Einsatz von Sprache begleitet die
begleitende Person die übenden Personen. Die Begleitung vermittelt der übenden
Person Wissen, erläutert ihr die Lehrmeinung, hilft ihr eine Körperhaltung
einzunehmen, verquickt Haltungswechsel mit Atemanleitung, reicht ihr
wohlwollend die Hand auf dem (unheimlichen) Weg nach innen und holt sie
liebevoll zurück zu sich. Gesprochene Worte dienen dazu die Übenden mit sich
selbst zu verbinden, sich selbst zu erkunden, zu erforschen, zu entdecken, zu
erkennen, zu ermächtigen und schließlich zu verwirklichen. Dieser letzte
Schritt der Selbstverwirklichung ist das eigentliche Ziel. Der Weg dort hin
verlangt Begleitung und möchte doch alleine gegangen werden. Am Ziel angelangt
ist Begleitung überflüssig.

_Persönlicher Weg_: Verbale Begleitung erleuchtet die innere Dunkelheit, zeigt
Wege auf, ebnet Pfade, unterstützt Entscheidungen an Gabelungen und schenkt Mut
weiter zu gehen. Die begleitende Person kann nur soweit mitgehen wie sie selbst
bereits gegangen ist. Ihr hilft ihre eigene Weisheit, ihr eigener innerer Weg
und ihre eigenen inneren Bilder. Diese Weg und diese Bilder können bei der
übenden Person andere sein. Und doch kann ihr die Weisheit der begleitenden
Person Selbstvertrauen geben ihren eigenen Weg und eigene Bilder zu finden.

_Erleuchtende Sprache_: Die übende Person ist eingeladen sich selbst zu
lauschen, ihrem inneren Dialog zuzuhören, ihren Weg zu sehen, ihre
(unbewussten) Entscheidungen zu verstehen und ihre eigenen Worte dafür zu
finden. Diese Worte bringen den Weg auf eine andere Ebene, helfen diesen zu
festigen, ihn in Kontakt zu bringen, ihn für ein Gegenüber greifbar zu machen
und sich darüber auszutauschen.

_Das richtige Maß_: Die begleitende Person sollte Sprache bewusst einsetzen
nach der Devise "so viel wie nötig und so wenig wie möglich". Sie sollte auch
begleiten durch Sprachpausen, durch Einstimmung, durch Zuhören, durch
Einschwingen, durch Dasein und durch Co-Regulation. Das richtigen Maß benötigt
feine Wahrnehmung des ganzen Raums. Welche Worte sind wie ausgedrückt, wann
ausgesprochen, in welchem Raum förderlich für die Prozesse? Und warum ist das
so?

## Sprachebenen, Kommunikation und Stimmbildung

**TODO [Angewandte Rhetorik](http://www.rhetorik.ch/Angewandt/Angewandt.html)**
**TODO [Die Anatomie der Sprache](https://www.dasgehirn.info/denken/sprache/die-anatomie-der-sprache)**

_Sprachebenen_: Sprache gibt es auf vielen Ebenen. Es gibt die Wortsprache der
Ratio, die Körpersprache des Unterbewusstseins, die Sprache der Gefühle, die
Sprache des Nervensystems und die Seelensprache des Überbewusstseins. Die
Wissenschaft kennt Lautsprache (gesprochene Sprache), Gebärdensprache,
Zeichensprache, geschriebene Sprache, bildliche Symbolsysteme, Parasprache
(sämtliche, die Sprache begleitende, vokale, das heißt an Sprachlaute gebundene
Mittel, die für die Kommunikation von Bedeutung sind). Die begleitende Person
nutzt diese Ebenen um die übenden Personen auf mannigfaltige Weisen zu
erreichen und ihnen zuzuhören.

_Kommunikation_: Es gibt vier Arten der Kommunikation, nonverbale, verbale,
schriftliche und visuelle. Nonverbale wird durch Mimik (Bewegungen im Gesicht),
Gestik (Bewegungen außerhalb des Gesichts), Körperhaltung und Bewegung im Raum
kommuniziert. Mimik offenbart (Un)Aufmerksamkeit, (Des)Interesse und die
Grundgefühle Freude, Wut, Angst, Trauer, Überraschung, Verachtung und Ekel.
Gestik offenbart Stimmung, Ermutigung, Dominanz und Schuld/Vermeidung.
Körperhaltung offenbart Offenheit/Verschlossenheit. Augenkontakt offenbart
Ehrlichkeit, Präsenz, Engagement und Respekt. Verbal wird durch Sprache
(gesprochenes/geschriebenes Wort) kommuniziert. Worte enthalten
Inhalt/Informationen und Haltung/Einstellung. Paraverbal wird durch die Art der
Ansprache/Artikulation, das heißt das Spektrum der Stimme wie Lautstärke,
Klangfarbe, Rhythmus, Sprachtempo, Betonung/Tonlage/Tonhöhe/Intonation und
Melodie kommuniziert. Sprachtempo lädt Interesse oder Langeweile ein. Tonlage
offenbart Gefühle. Tonfall offenbart Meinung über das Gegenüber.

Nonverbale und paraverbale Kommunikation

- unterstützt, bestärkt oder entkräftet verbale Kommunikation,
- ersetzt die Sprache (Schweigen, Weinen),
- drückt Gefühle aus,
- zeigt Einstellung zum Gegenüber und zur Situation.

Der/Die Sender:in verschlüsselt und der/die Empfänger:in entschlüsselt eine
Nachricht. Kommunikationspartner:innen interagieren miteinander und reagieren
aufeinander. Leitfragen um eine Kommunikationssituation zu beschreiben sind:

- Wer übermittelt? --- Sender
- Wie? --- Kommunikationsmittel
- Was? --- Nachricht
- An wen? --- Empfänger
- Mit welcher Wirkung? --- Effekt auf Seiten des Empfängers
- Mit welcher Reaktion? --- Empfänger wird zum Sender

Die begleitende Person hält durch Kommunikation einen relativ sicheren und
ermutigenden Erfahrungsraum für die übenden Personen. Sie übermittelt
Sicherheit und Ermutigung durch ihr bloßes Dasein, ihre aufmerksame und
interessierte Mimik, ihre gelassene und ermutigende Gestik, ihre offene
Körperhaltung, ihren ehrlichen, präsenten, engagierten und respektvollen
Augenkontakt, ihre reichen und inspirierenden Worte und die (selbst)sichere
und (selbst)verbundene Art ihrer Ansprache.

**TODO Kommunikationsmodelle: Sender-Empfänger-Modell, Vier-Ohren-Modell (Sachebene,
Selbstoffenbarung, Beziehung, Appell), Eisbergmodell (20% Sachebene, 80%
Beziehungsebene), ... https://studyflix.de/biologie/kommunikationsarten-4762)**

_Stimmbildung_: Die begleitende Person trainiert ihre Stimme um ihre ureigene
Stimmlage zu finden, Melodie und Betonungen passend einzusetzen, Lautstärke,
Stimmresonanz und Stimmklang zu stärken und Tempo und Pausen geschickt zu
variieren. Dazu praktiziert sie Körper-, Atem-, Stimm- und Artikulationsübungen
mit offener mentaler Einstellung / innerer Haltung. Und sie bildet ihre
Wahrnehmung und ihre Bewusstheit aus, denn nur was sie wahrnimmt kann sie
bewusst verändern. Die Aufmerksamkeit auf das Körpererleben ist wesentlich
sowie Experimentieren, Spielfreude, Persistenz und Zeit. Eine ausgebildete
Stimme ermöglicht ihr souverän und nachhaltig ihr Leben lang stimmlich zu
begleiten.

**TODO [Stimmbildung](https://de.wikipedia.org/wiki/Stimmbildung)**

## Menschliche Erfahrung

Was sind Bestandteile [bewusster menschlicher
Erfahrung](https://en.wikipedia.org/wiki/Experience#As_conscious_event)?
Gedanken, Sinneswahrnehmungen und Körperempfindungen, Gefühle und Stimmungen,
Sehnsüchte/Verlangen, Erinnerungen, Vorstellungen und Handlungen. Genaueres in
Kapitel 5 von "Hardwiring Happiness".

Gedanken: Faktisches Wissen, Ideen, Annahmen, Erwartungen, Meinungen,
Einsichten, Vorstellungen und Erinnerungen

Sinneswahrnehmungen: Das Reich von Anblicken/Sehen, Geräuschen/Hören,
Gerüchen/Duft, Geschmack, Berührungen und Interozeption. Das Medium für
Erfahrungen von Freude und Entspannung, Lebendigkeit/Vitalität und Stärke.

Emotionen: Gefühle und Stimmungen. Gefühle sind kurz und werden von inneren und
äußeren Stimuli hervorgerufen. Stimmungen sind diffuser, dauerhafter und
unabhängig von Stimuli. Traurigkeit ist ein Gefühl, Depression eine Stimmung.

Sehnsüchte: Hoffnungen, Wünsche, Begierden und Bedürfnisse, Motivationen,
Neigungen, Werte, Bestrebungen, Absichten und Ziele, Abneigungen,
Getriebenheit, Abhängigkeiten und jede Art von Sucht.

Handlungen: Im weiteren Sinne nach außen gerichtetes Verhalten – Benehmen,
Mimik, Körperhaltung, gesprochene und geschriebene Worte – sowie die nach innen
gerichteten Neigungen und Veranlagungen, die sichtbares Handeln produzieren,
wie beispielsweise die Fähigkeit, sich jemandem zuzuwenden, der leidet.

[Warhnehmung](https://de.wikipedia.org/wiki/Wahrnehmung): Exterozeption
(Außenwelt) und Interozeption (Propriozeption -- Körperlage und -bewegung im
Raum und Viszerozeption -- Organtätigkeit). Das Fühlen (Tastsinn) wiederum kann
einerseits nach der Wahrnehmung von Berührung, Schmerz und Temperatur
(Oberflächensensibilität), andererseits aber auch in das aktive Erkennen
(haptische Wahrnehmung) und das passive „berührt werden“
(Oberflächensensibilität) unterteilt werden. Weitere Sinne sind der
Gleichgewichtssinn, der Zeitsinn und der Magnetsinn. Die Psychologie kennt
daneben die Begriffe der Selbst- und Fremdwahrnehmung, wobei erstere die
Überzeugungen sind, die wir von uns selbst beziehungsweise unserem Empfinden
und Verhalten haben, während Fremdwahrnehmung die Eindrücke bezeichnet, die
andere von uns gewinnen. Wenn diese Wahrnehmungen nicht wenigstens ansatzweise
deckungsgleich sind, kann es zu Problemen in der zwischenmenschlichen
Kommunikation kommen. Empfindung ist das „primäre unmittelbare psychische
Korrelat einer Sinneserregung durch Reize“.

Sinneswahrnehmungen des Menschen:

- visuell (Gesichtssinn, Sehen): Helligkeit, Farbe, Kontrast, Linien, Form und
  Gestalt, Bewegung und Räumlichkeit
- auditiv (akustische, Gehörsinn, Gehör oder Hören): Schall, insbesondere
  Geräuschen, Tönen und Klängen. Lokalisation: Richtungshören und
  Entfernungshören.
- vestibulär (Gleichgewichtssinn): Lageveränderungen im Verhältnis zu einem
  Schwerefeld zur Wahrung des Gleichgewichts und der Kontrolle von Bewegungen,
  zusammen mit Augen und Muskelsinn. Gleichgewichtsorgan im Innenohr.
- Sensibilität (Tastsinn, Gefühl oder Fühlen): Vereinigung von Taktiler
  Wahrnehmung und Tiefensensibilität. Berührungen, Schmerz, Härte oder Hitze
  (siehe auch Haptische Wahrnehmung). Gesamtheit aller Tast-, Wärme- und
  Kälterezeptoren.
  - Tiefensensibilität: Dient der Wahrnehmung der Stellung der Körperglieder
    zueinander und damit der Körperhaltung
  - Taktile Wahrnehmung: Dient der Wahrnehmung von Druck, Berührung und
    Vibrationen sowie der Temperatur.
  - Trigeminale Wahrnehmung: Dient der taktilen Wahrnehmung im Gesicht
    (beispielsweise des Windes) und unterstützt die olfaktorische und die
    gustatorische Wahrnehmung.
- Olfaktorisch (Geruch oder Riechen): Riech- und Duftstoffe. Riechschleimhaut.
  Stark mit Emotionen assoziiert.
- Gustatorisch (Geschmack oder Schmecken): Chemische Qualitäten von Nahrung.
  Geschmacksknospen

Kognition:

- Wahrnehmung und Aufmerksamkeit,
- Erinnerung und Lernen,
- das Problemlösen, Kreativität und Vorstellungskraft,
- das Planen und die Orientierung,
- die Argumentation,
- die Selbstbeobachtung (Introspektion),
- der Wille,
- und das Glauben.
- [Zeitwahrnehmung](https://de.wikipedia.org/wiki/Zeitwahrnehmung)
  entsteht erst durch kognitive Vorgänge. Beim
  Menschen unterscheidet man die beiden Formen Wahrnehmung der zeitlichen
  Folge (Sequenz) und die Wahrnehmung von Zeitintervallen.

## Sprachgewalt/-macht/-vermögen

_Rhetorik_: Rhetorik ist das auf Erfolg, Effektivität und Überzeugung gerichtete,
praktische Kommunikationsverhalten von Menschen.

_Interesse_: Worte von außen bekunden Interesse der sprechenden zur übenden
Person und Worte von innen der übenden Person zu sich selbst. Sie bekunden,
dass Personen andere und sich wichtig nehmen.

_Neue Welten_: Worte können etwas einladen (bewusst machen) was vorher nicht da
(erlebbar) war --- "Gewahre dein Selbst". Indem die Möglichkeit durch Worte
vorweggenommen wird, manifestiert es sich mit größerer Wahrscheinlichkeit. Die
Energie fließt dahin wo das Bewusstsein hinfließt. Das Bewusstsein fließt dahin
wohin es durch Worte eingeladen wird.

_Selbsterkenntnisse_: Worte können den Weg weisen, die Türe öffnen,
Anhaltspunkte geben, sie können Erfahrungen verankern, vertiefen, auf andere
Ebene bringen, sie können die Landkarte des Ich verfeinern. Dazu sollen übende
Personen eingeladen werden ihre eigenen Worte zu finden, die ihr persönliches
Erleben beschreibt.

_Wachstum_: Worte unterstützen dabei dabei zu bleiben, sich selbst
herauszufordern (Tapas), länger durchzuhalten, eigene Grenzen zu versetzen,
höhere Berge zu erklimmen, reichere Früchte zu ernten, um über sich selbst
hinaus zu wachsen und dieses erwachsene Selbst zu sein.

## Sprachgebrauch

_So viel wie nötig und so wenig wie möglich_: Das begleitende Wort soll
einfach, gebräuchlich, knapp, präzise, poetisch, bildreich und den Übenden (die
Zielgruppe) angepasst sein. Es soll dazu dienen Freiraum zu halten ohne Raum zu
nehmen, dazu Wege aufzuzeigen ohne sie vorzugeben, dazu geborgenen Raum zu
geben für individuelles Erforschen und persönliche innere Wahrheit. Worte
können stören und verlangen maßvollen Einsatz.

_Offenheit und Freiraum_: Offene Fragen laden dazu ein persönliche Wahrheiten
zu finden. Intuitive, unverständliche, bildreiche Wahrheiten aus dem Erleben,
der Lebendigkeit, des Seins. Dazu bedarf es Fragen, die nicht überfordern, die
die Übende nicht in das konzeptionelle/rationelle/logische/schwarz-weiß Denken
bringt, nicht in die Landkarte, sondern die Übende nur herausfordert durch
behutsames Heranführen an eigene Grenzen, durch Belassen im
Selbstgewahrsein/Körper/Unbewusstsein, in der Landschaft. Insbesondere sollen
manipulative Aussagen und inhärent unvollständige Antwortlisten vermieden
werden wie "du spürst ...".

Beispiele offener Fragen sind

- Was hat sich gewandelt?
- Wie erlebst du dich?
- Was ist lebendig in dir?
- Wie zeigt sich im Hier und Jetzt deine Lebendigkeit?
- Was ist dir gerade zuträglich?
- Wo strömt und fließt es? Ströme und fließe mit. Wo entdeckst du Blockaden?
  Was schützen diese? Ist dieser Schutz gerade notwendig? Was benötigen die
  Blockaden um sich zu lösen? Kannst du ihnen das geben?
- Was möchtest du zu dir holen, in dein Leben einladen, willkommen heißen?

_Hypnotische Sprach(struktur)_: Eine Möglichkeit das Bewusstsein im umfassenden
Gewahrsein zu belassen statt ins ergebnisorientierte Denken zu bringen, ist
eine hypnotische und tranceartige Sprach(struktur); der Einsatz von somatischen
Markern für Körperräume, Gestik und Mimik, Sinneswahrnehmungen und inneren
Bildern; und eine an die Übung angepasste Sprechgeschwindigkeit,
Sprechrhythmus, Sprechpausen (Prosodie), Sprachmelodie (im Sinne von melodiöser
Sprache), Klangfarbe, Lautstärke und Lautmalerei.

[Prosodie](https://de.wikipedia.org/wiki/Prosodie) ist die Gesamtheit
derjenigen lautlichen Eigenschaften der Sprache, die nicht an den Laut oder ans
Phonem als minimales Segment, sondern an umfassendere lautliche Einheiten
gebunden sind. Dazu zählen

- Wort- und Satzakzent,
- der auf Wortsilben ruhende lexikalische Ton in Tonsprachen,
- Intonation (von Einheiten von mehr als Silbenumfang) und Satzmelodie,
- Quantität aller lautlichen Einheiten, vor allem derjenigen von mehr als Segmentumfang,
- Tempo, Rhythmus und Pausen beim Sprechen.

_Spannungsbögen_: Die Übungsauswahl selbst und auch die begleitende Sprache
beschreibt einen Spannungsbogen über die Unterrichtseinheit hinweg. Und auch
jede Übung, die Zeit dazwischen und Beginn und Ende hat einen eigenen
Spannungsbogen. Und bei fortlaufenden Kurseinheiten haben auch diese einen
Spannungsbogen.

_Positivität_: Verwende positive, bejahende und einschließende Sprache.
Bevorzuge "einfach" vor "mühelos", "entspannt" vor "anstrengungslos", "Held:in"
vor "Held" und so weiter. Statt durch eine Korrektur das Negative zu betonen,
wiederhole die korrekte Ausführungsweise einer Übung. Dies erlaubt der übenden
Personen ihre Ausführungsweise mit der angesagten abzugleichen und bei Bedarf
anzupassen ohne sie bloßzustellen.

_Wunschzustände_: Hebe mit Sprache Wunschzustände hervor statt Lösungsversuche
oder Herausforderungen/Probleme. Oder zeige den Weg von Herausforderungen über
Lösungsversuche zu Wunschzuständen auf, entfalte letztere und unterstütze deren
Verankerung.

_Persönliche Wahrheit_: Spreche Einladungen aus statt Vorgaben zu machen: "Ich
lade dich ein ...". Biete Möglichkeiten/Optionen an statt Erfahrungen
aufzudrücken/einzuimpfen und dadurch manipulativ zu sein oder den Eindruck von
"Ich bin falsch, weil das bei mir anders ist." hervorzurufen: "Bei mir
geschieht ... Was geschieht bei dir?" oder "In den alten Texte steht / Die
Theorie besagt / Die Wissenschaft hat herausgefunden oder behauptet ... Was
erlebst du bei dir? Was ist deine individuelle, persönliche Wahrheit?"

_Metaphern_: Finde erhabene poetische malerische Metaphern, innere Bilder,
Visualisierungen, Ideale und Vorstellungen aus den Bereichen

- 5-Elemente (feurig Feuer, fließend Wasser, geerdet/erdverbunden Erde,
  leicht/luftig Luft, durchdringend Äther)
- Raum (allumfassend, Himmelsrichtungen) und Zeit (allgegenwärtig, Zeitstrahl)
  und Schwerkraft (allmächtig)
- Erde (braun) und Himmel (blau) und Herzen (rot)
- Jahreszeiten: Frühling, Sommer, Herbst, Winter
- [Mondphasen und Sonnenphasen](https://de.wikipedia.org/wiki/Lunarkalender)
- Himmelskörper/Planeten: Sonne, Mond, Sterne, Sternschnuppen, Sternbilder
- Lebewesen (gemächlich/müßig wie die Schildkröte, faul wie das Faultier)
- Naturphänomene: Wind, Böen, Regen, Nieselregen, Platzregen, Sturm
  (stürmisch/wild), Hagel, Hagelsturm, Wolken, Blitze, Sonnenschein,
  Mondenschein, Sternenschein, Nordlichter/Polarlichter, Feuer, Wasser,
  Bäche, Flüsse, Seen, Meere, Wasserfälle, Eis, Eisberge, Gletscher, Schnee,
  Lawinen, Sand, Sandstrand (rieseln wie Sand, prasseln wie Regen)
- Mythen und Sagen wie Waldfeen
- Geometrie wie Dreieckshaltung

_Passive Formulierung_: Passive Formulierungen wie "Lass die Arme über die
Seiten aufsteigen" und vorgeschlagene Geisteshaltungen (Ausrichtugen) wie
"Entspanne dich in den Schulterstand hinein" suggeriert
Einfachheit/Leichtigkeit und "Sei dein/e Held:in", "Sei der/die Held:in deines
Lebens" oder "Verbinde dich mit der Held:in in dir" lädt zur Verkörperung von
Haltungen ein. Die Übung offenbart/zeigt sich durch die ausübende Person, wird
durch sie lebendig, verkörpert sich in ihr, geschieht einfach und nicht die
Person macht die Übung.

"richte dich aus" oder "sei ausgerichtet"?

_Kreativität_: Das bloße Nennen des Ziels wie "Komme in der Rückenlage an",
"Begebe dich in Rückenlage" oder "Finde einen Weg in die Rückenlage" lässt den
Weg dorthin offen und lädt dadurch zu Kreativität ein, zu Selbstverantwortung,
zu Selbstwirksamkeit und ermöglicht jeder einzelnen Person eine für sie im Hier
und Jetzt zuträgliche Art und Weise zu finden.

_Liebevoll_: Liebevolle Sprache wie "Küsse mit den Schultern deine Ohrläppchen"
lädt ein zu liebevoller Betrachtung des eigenen Selbst und liebevollem Umgang
mit sich selbst.

_Einheit_: Finde Worte jenseits von Dualität, solche die jene transzendieren,
die das Verbindende zwischen den scheinbare entgegengesetzten unvereinbaren
Polen offenbaren, die diese in einen größeren Rahmen einbetten, in ein großes
Ganzes, wie beispielsweise das Menschsein hinter dem genderspezifischen
Antagonismus Mann/Frau. Oder betone absichtlich zunächst die Dualitäten und
biete anschließend einen lösenden Perspektivwechsel an und ermögliche dadurch
eine tiefere Erkenntnis, wie beispielsweise von absichtslos (Losgelöst/Gelöst
von Absichten) zu offen (TODO besseres Beispiel finden).

_Passgenau_: An den Moment, die Übung, die Gruppe und so weiter angepasste
Sprache und Sprechweise. Ruhig in Ruhemomenten,
herausfordernd/motivierend/auffordernd in herausfordernden Momenten.

_Lagebezeichnungen_: Nutze anatomische Lage- und Richtungsbezeichnungen, die
unabhängig von der Position und Orientierung des Körpers im Raum sind wie
kopfwärts, steißwärts und fußwärts, hin zur und weg von der
Körpermitte/-zentrum, innenseitig/innenliegend/nach innen und
außenseitig/außenliegend/nach außen, bauchseits/vorderseitig und
rückenseits/rückseitig.

_Neue Übung_: Zur Einführung einer neuen Übung nenne deren Namen und erläutere
deren Bedeutung, erzähle von ihren möglichen Wirkungen und Wirkweisen,
demonstriere und versprachliche die Körperhaltung, die zugehörige Atemtechnik,
den Fokuspunkt und die Bewusstseins- und Sinnesausrichtung und nenne
unterstützende Idealvorstellungen und Visualisierungen.

_Theorie_: Zur Erläuterung von Theorie, Philosophie und Hintergründen lese aus
Texten vor, erläutere sie und mache deren Inhalt erfahrbar, beispielsweise aus
traditionellen spirituellen Texten wie den Vedas, Upanishaden, Vedanta,
Bhaghavad Gita, Hatha Pradipika, Yoga Sutras und Samskya-Philosophie oder aus
modernen wissenschaftlichen Texten zu Anatomie, Physiologie, Biochemie,
Neurobiologie und Psychologie.

_Begleitung_: Zur Begleitung während einer Übung

- Verbinden / In Kontakt gehen im Sinne von "Ich bin bei dir, deine Mitübenden
  sind bei dir, wir sind gemeinsam auf dem Weg"
- Erinnern an Übung (korrekte Ausführung)
- Zurückholen zum Selbstgewahrsein und zum Fokusobjekt
- Anker im Außen um sich nicht im Inneren zu verlieren und um verbunden mit der
  Außenwelt zu bleiben
- Kraft geben durch erinnern an eigene Stärken (vier unermesslichen
  Geisteshaltungen: Liebende Güte, Mitgefühl, Mitfreude und Gleichmut)
- Hürdenüberwindung (Kleshas)

## Ideenfetzen

Worte der Hinwendung/Zuwendung.

_Vorstellung_: Puppenspieler vs. Puppe: Befreit und spielerisch vs.
ausgeliefert und Spielball

_Bewusstseinslenkung_: Was ist im Vordergrund des Bewusstseins und was im
Hintergrund? Kannst du etwas im Hintergrund in den Vordergrund holen (mit dem
Schein/Scheinwerferlicht/Rampenlicht/Schlaglicht des
Bewusstseins/Aufmerksamkeit bestrahlen/beleuchten/erleuchten)?

_Raum_: Wo sind deine Grenzen? Wie zeigen sich diese? Wie nimmst du sie wahr?
Wie weit ragst du in den Raum? Wie drückt sich Raum in dir aus/ein? Wie lässt
dich der Raum frei (Freiraum)? Wie befreit erlebst du dich im Raum? Wie erlebst
du dich in Zeit und Raum? Wie stehst du im Leben? Wie ist lebendig sein für
dich? Wie vergeht Zeit in dir? Erlebst du sie als Zeitstrahl oder Zeitnetz oder
ist alles gleichzeitig (Gleichzeitigkeit)? Wie prägt sich der Raum in dir ein?
Wie bist du mit Raum und Zeit verwoben? Was ist in deinem Erleben das
Raumgefüge (the fabric of space)?

_Bedeutung_: Was bedeutet ... für dich, dein Leben, dein Erleben, dein Sein in
der Welt, deinen Alltag? Wo spürst du ... in dir, in deinem Körper?

_Resonanz_: Wie resoniert es in dir? Wie graviert es sich in dir ein? Wie
zeichnet es sich in dir ab? Wie hallt es in der wider? Wie schlägt es sich in
der nieder? Wie drückt es sich in dir ein/aus?

_Weg nach innen_: Bei Pratyahara bis Dharana begleitet Sprache auf dem Weg nach
innen in Form von Ideen zum Überwinden von Hürden und Wiedererinnerung an
Übungspraxis. Beispielsweise durch Worte der Verbundenheit im Sinne von "Du
bist in Begleitung, ich bin bei dir auf deinem Weg ins Unbekannte".

## Spruchsammlung

beheimaten, verkörpern, offenbaren, einladen, Widerhall, zu dir holen.

**Selbstermächtigung/-wirksamkeit**

- Erhebe dich, steige auf wie der Phönix, komme in deine Kraft.
- Bemeistere dein Leben
- Steh auf und sei frei.
- Gehe/Komme in deine Kraft!
- Entdecke deine schöpferische Kraft. Der Geist schöpft den Körper.
- Lass die kreative Kraft des Himmels durch dich wirken.
- Glaube an dich.
- Zeige dich in deinem Glanz.
- Erfinde dich neu, denke dich neu, erlebe dich neu, erlaube dir neu zu sein,
  wachse über dich hinaus, gehe aus dir heraus, glaube an dich, entfalte dein
  Potential. Lass die Kreativität von Vater Himmel, die Geborgenheit von
  Mutter Erde und die liebevolle Güte des Herzens durch dich wirken,
  offenbare sie, manifestiere sie in der Welt durch deine Hände, die
  Verlängerung deines Herzens.
- Sei fürsorglich mit dir selbst.

**Selbstgewahrsein**

Betrete/Schreite durch das Tor/Schreite durch die Pforte/Trete ein in den
universellen/zeitlosen/allgegenwärtigen/ewig gegenwärtigen Raum inneren
Gewahrseins.

_Nachspüren_: Wie zeigt/offenbart sich Lebendigkeit gerade in dir? Heiße alle
Körperwahrnehmungen, alle Gefühle, alle Gedanken, alle Stimmungen willkommen.
Nehme sie liebevoll, mitfühlend, mitfreuend und gleichmütig wahr als Ausdruck
deiner ureigenen Lebendigkeit.

Spüre nach. Was hat sich gewandelt? Wie bist du gerade hier? Wie zeigt sich
Lebendigkeit in dir? Wie erlebst du dich? Was ist in dir lebendig? Wo strömt
und fließt es, wo kribbelt und prickelt es? Wie offenbart sich im Hier und
Jetzt deine ureigene Lebendigkeit, dein ureigenes Sein, dein Wesenskern?

_Nicht-Anhaftung_: Beobachte deine Gedanken, Gefühle und Empfindungen. Lass sie
geschehen, sei mit ihnen, lass sie gehen, vorbeiziehen wie Wolken am Himmel,
abebben wie Wogen im Meer (Wellen auf See), sich auflösen wie Nebelschwaden am
Morgen, durchleuchte sie mit deinem inneren Glanz/Schein und gewahre/schaue
dein wahres Selbst jenseits des blauen Himmels und in den Tiefen des Meeres am
Meeresgrund.

_Gefühlsschau_: Lasse dich nicht von deinen Gefühlen überwältigen, bringe sie
auf eine bewusste Ebene, achte sie, betrachte sie, nehme sie wahr als das was
sie sind, lass sie ein Teil von dir sein, gewahre ihr Kommen und Gehen wie
Wellen/Wogen im Meer der Gefühle, behalte die Zügel in Händen.

_Ichhaftigkeit/-verhaftung_: Durchdringe, durchschaue und überwinde deine
Ichhaftigkeit. Lerne nicht in deinem Ich verhaftet zu sein. Erkenne dein
wahres Selbst, werde dir dessen bewusst.

_Sinneszurückziehung_:

- Schaue du deine Augen, während deine Augen die Welt
  schauen. Sei mit dir selbst verbunden, während du dich mit der Welt verbindest
  (während dich deine Sinne mit der Welt verbinden).
- Luge (blicke, schaue), schnuppere (rieche, schnüffle), schmecke, lausche
  (höre), spüre (fühle) in dich hinein, richte deine Sinnesorgane/Sinne
  (Augen/Sehsinn, Nase/Geruchssinn, Mund/Geschmackssinn, Ohren/Hörsinn,
  Haut/Spürsinn) nach innen, übe Zurückziehen der Sinne (Sammlung, Einkehr,
  Pratyahara) und gewahre/folge deiner inneren Bewegung, deiner Lebendigkeit,
  dem endlosen, nie versiegenden Fluss des Lebens.
- Zäume deine Sinne an. Werde Herr:in/Frau:in deiner Sinne.
- Versenke dich, sei umfassend da, ganz angekommen, versöhne/vertöchtere Shiva
  und Shakti, finde Zuflucht in dir selbst.
- Hole deine Sinne zurück. Kehre heim. Komme nach Hause. Ziehe in deinen Körper
    ein. Bewohne ihn. Sei in ihm zuhause. Meditation ist Heimkehr.

_(Ein)spitzigkeit_:

- Ruhe dich in deinen Augen aus, übe Dhyana (müheloses Dharana, Meditation),
  mache deinen Geist (ein)spitzig.
- Wenn dein Bewusstsein davon eilt. Hole die Lebensgeister zurück, sammle sie
  ein.

_Visualisierung_:

- Sende gleißendes Licht in jede Zelle.

_Körperbewusstsein_:

- Fließe/Ströme durch deinen Körper, sie umfassend/überall präsent, beleuchte
  jede Zelle
- Löse dich in dir auf, sei ganz bei dir, mit dir, in dir, ganz du selbst,
  wahrhaftig.
- Begeistere/Vergeistige deinen Körper
- Erfülle/Fülle aus den ganzen Raum vom Scheitel/Fontanelle bis in die
  Zehenspitzen/Fußsohlen mit Bewusstheit.
- Verbinde Fontanelle und Fußsohlen.
- Taste deinen Körper von innen ab, fließe durch ihn, langsam, von der
    Fontanelle bis zu den Zehen. Bei Blockaden, unzugänglichen und schwer
    spürbaren Bereichen halte inne und schenke diesen Aufmerksamkeit,
    durchdringe sie und lasse sie durchlässig werden.
- Meditation ist Innenschau: Schaue dein Inneres, ziehe ein in deinen Körper,
  begeistere dich, verbinde dich, sei verbunden.

_Höheres Selbst/König Raja_:

- Offenbare dein wahres/höheres Selbst als universelle/objektlose Liebe
- Entdecke/Offenbare das Göttliche/Königliche/Atman/Brahman in dir
- Inspiriere (spirit) dich (lass dich inspirieren; sei inspiriert), entdecke
  dein Selbst, weile in deiner Seele
- Erfahre dich als spirituelles Wesen, das eine menschliche Erfahrung macht.
- Gebe dich dem Moment hin, vertraue dich ihm an, schmelze in ihn, verschmelze
  mit ihm, lass Körper, Geist und Seele los, versenke dich in dein Selbst,
  gewahre dich.
- Dein wahres Selbst liegt wie ein Schatz tief verbogen in dir.
- Kultiviere Selbstgewahrsein. Kultiviere Verbundenheit. Kultiviere
  Einheit/Einssein.

_Hingabe_:

- Öffne dich durch Meditation dafür Gottes Wort zu empfangen.
- Gebe dich hin, lasse los, erlaube dir, deinem Körper, deiner Gedankenwelt,
  deiner Gefühlswelt, deinem Unbewusstsein, sich selbst neu zu ordnen, neu
  auszurichten, neu zu erschaffen, gib die Zügel aus der Hand, vertraue dich
  an, glaube an das Göttliche in dir, an dein höheres Selbst, an die dir
  innewohnende Lebensenergie, Lebendigkeit, kreative Kraft, Allmacht,
  Allwissenheit, Allgegenwart, Ewigkeit.

**Tönen und Lautmalerei**

- Finde deine Stimme. Zeige dich in deinem Glanz. Teile dich mit. Lass andere
  teilhaben an dir, an deinem inneren Erleben, deiner Wirklichkeit. Drücke
  aus was hier und jetzt heraus möchte. Befreie dich.
- Lass ausklingen (wie Schallwellen, Klang/Gehör-Metapher). Folge dem Klang bis
  zum Schluss. Wenn der Schall abgeebbt ist, was schwingt in dir nach, wie
  resoniert er in dir, auf welche Weise hallt er in dir wider?

**Atem**

_Gewahrsein_: Wie strömt dein Atem ein und wie aus? Welche Qualitäten hat er bei
der Ein- und welche bei der Ausatmung? Ströme mit deinem Atem.

_Gelöster Atem_: Atme auf, lass den Atem geschehen, gebe ihn frei, lass es
durch dich atmen.

_Anker_: Nutze deinen Atem als Anker in der Gegenwart. Verankere dich durch
Atemgewahrsein im Hier und Jetzt, im gegenwärtigen Moment. Und wenn deine
Gedanken davon eilen in Vergangenheit oder Zukunft, nehme dies wohlwollend wahr
und dann holt dich dein Atem mühelos ein, nimmt dich an der Hand und begleitet
dich liebevoll zurück in den Moment.

_Strömrichtung_: Stelle dir vor, dass dein Atem bei der Einatmung direkt nach
unten zum Beckenboden oder Zwerchfell strömt und bei der Ausatmung von dort
nach oben, so als würdest du durch deinen Beckenboden oder dein Zwerchfell ein-
und ausatmen.

_In- und Exspiration_: Lasse ich geschehen, entspanne ich mich in den Moment,
öffne ich mich der Inspiration (Einatmung), dann kommt sie zu mir, fließt sie
in mich, strömt sie mir zu. Öffne ich mich der Exspiration (Ausatmung), dann
kann altes gehen, sich von mir lösen, abfließen, ausgeschieden werden.

Einatmung: Introspection/Introspektion/Innenschau/Einkehr. Ausatmung:
Compassion/Güte/Mitgefühl/Mitfreude.

## Übungen

Sei die Übung (Samadhi mit Form), verkörpere sie, offenbare sie.

Erfahre in jeder Übung alle Wegabschnitte des achtfachen Pfades. Begleite jeden
Abschnitt sprachlich. **TODO Ausarbeitung für Atemübung wie Kapalabti. Dabei
schwingt das Zwerchfell frei.**

In jeder Übung kann durch verschiedene Ausführungsweisen jedes Dosha
angesprochen werden. Biete je diese konstitutionsspezifisch an.

_Ankommen_: Wie hallt der Tag in dir wider, wie klingt/schwingt er in dir nach
(Vergangenheit, damals, dort)? Wie erreicht dich die Zukunft (Zukunft, dann,
jenseits)? Wie wirkt der Raum auf dich (Raum, hier)? Wie wirkt die Tageszeit
auf dich (Zeit, jetzt)? Wie wirken die Mitübenden auf dich
(Gesellschaft/Verbundenheit, sozial)? Und wie erlebst du dich eingebettet in
Vergangenheit und Zukunft? Wie erlebst du dich im Hier und Jetzt? Wie erlebst
du dich in Gesellschaft? Und wie erlebst du dich selbst? Was ist gerade
lebendig in dir?

_Totenstellung_: Lege dich auf den Rücken. Komme auf dem Boden an. Gebe dich
der Schwerkraft hin. Vertraue dich Mutter Erde an. Lass dich in den (Unter)Grund
sinken. Schmelze in deine Unterlage. Ziehe dich in deine Mitte zurück. Stelle
dir vor du liegst am Strand, gebettet auf warmem und weichem Sand, der dich
behutsam trägt, deine gesamte Rückseite stützt, in den du mit jeder Ausatmung
tiefer sinkst. Nehme deine Kontaktfläche zur Erde wahr (Alternativen: Nehme die
Kontaktfläche von dir / von deinem Körper zur Erde wahr). Male sie in deiner
Vorstellung aus. Wo berührst du den Untergrund und wo hebst du dich von ihm ab?
Schenke/Erfahre/Gönne dir Ruhe und Erholung. Ruhe und Erholung für dein ganzes
Wesen.

Komme zurück, ziehe ein in alle Körperräume, übernehme behutsam Kontrolle,
belebe deinen Körper, bewege Finger und Zehen, Arme und Beine, recke und
strecke dich, tu was dir gerade zuträglich ist, was du gerade brauchst um
zurück zu kommen, um einen Übergang in den Alltag zu finden. Und sobald du
bereit bist finde einen Weg über die rechte Seite zum Sitz, blinzele, öffne
liebevoll deine Augen, verbinde dich mit der Außenwelt, dem Raum den
Mitübenden.

Fördere Wachheit indem du deine Handflächen fest aneinander reibst, ein Feuer
zwischen den Händen entfachst, es schürst, deine Hände mit Lebensenergie, mit
Prana auflädst, die Handflächen auf deine Augen legst, sie bestrahlst, sie
nährst. (Alternativer Stil: Fördere Wachheit. Reibe deine Handflächen fest
aneinander. Entfache und schüre ein Feuer zwischen deinen Händen. Lade sie mit
Lebensenergie auf. Bestrahle deine Augen. Nähre sie.)

_Totenstellung_ (Alternativen):

1. Ich lade dich ein dir vorzustellen du treibst/schwebst/schwimmst schwerelos
   im Wasser. (floating in water)
1. Ich lade dich ein dir vorzustellen du schwebst in der Luft. (hovering in
   air)
1. Ich lade dich ein dir vorzustellen du schwebst über dir, über deinem Körper,
   schaust dich von oben an, nimmst Spannungen wahr, deinen Gesichtsausdruck.
   Was vermittelt dieser?

_Schulterkreis_: Über vorne nach oben und über hinten nach unten.

_Rückbeuge im Fersensitz (oder Fisch)_: Stärke deine innere Sonne (Solar
Plexus, Macht-/Kraftzentrum), strahle Licht in jede Zelle, erleuchte dich,
erstrahle, offenbare deinen Glanz, bade in deinem Licht, leuchte auf. TODO
Welche Übung lädt das Sonnengeflecht auf? Es gibt Körperhaltungen und
Atemübungen, die das tun.

_Vom Berge in die Vorbeuge_: Einatmend lass deine Arme über die Seiten nach
oben aufsteigen. Beginne mit den Handrücken nach oben gekehrt und drehe auf
halber Höhe die Handflächen nach oben. Nehme wahr wie kühlende Luft an deinen
Fingern vorbei und zwischen ihnen hindurch streicht dich liebevoll streichelnd.
Du streichelst du Luft und die Luft streichelt dich. Küsse mit den Schultern
deine Ohren, mache dich lang, strebe mit deinem Scheitel gen Himmel, schaffe
Raum in deinen Gelenken von den Füßen bis zum Kopf, Freiraum zwischen deinen
Wirbelkörpern. Ausatmend lass deine Schultern sinken, gebe dich der Erde hin,
verwurzele dich in ihr, greife mit den Füßen liebevoll in den Boden und
aktiviere deine ganze Körpervorderseite. Einatmend beuge dich gleichmäßig und
genüsslich nach hinten während du dein Becken sanft nach vorne herausschiebst,
dein Körper fein gebogen, nach vorne gewölbt, wie ein Flitzebogen. Ausatmend
tauche mit über die Seite absteigenden Armen nach vorne unten ab. Sei dabei
nachgiebig in deinen Beinen. Beuge sie so weit, dass dein Oberkörper leicht auf
deinen Oberschenkeln zum Ruhen kommt ohne dass zu viel Spannung im unteren
Rücken entsteht. Atme in deinem natürlichen Atemrhythmus weiter. Genieße die
Dehnung. Nacken ist gelöst, Kopf baumelt frei. Schultergürtel ist gelöst, Arme
hängen frei. Scheitel strebt Richtung Erde und Steiß strebt gen Himmel. Füße
sind gleichmäßig belastet. Atem strömt bis in den Beckenboden.

_Pendeln/Kreisen im Stehen oder Sitzen_: Zentriere dich, mitte dich ein.

_Flügelatmung_: Stelle dich breitbeinig hin, Fußaußenkanten sind parallel,
Zehen zeigen leicht nach innen. Mache dich lang, Scheitel strebt gen Himmel,
Fußsohlen drücken in die Erde. Umfasse mit den Bögen zwischen Daumen und
Zeigefinger (Tabatière) deine Leisten (die Kante zwischen Oberschenkel und
Becken) leichten Druck ausübend. Beuge dich in den Hüftgelenken nach vorne und
schiebe dein Gesäß nach hinten raus so weit, bis dein Oberkörper in einer Ebene
zur Erde ist, Rücken ist gerade, Kopf ist in Verlängerung der Wirbelsäule,
Blick geht nach unten, Fußvorderseite ist betont. Verschränke deine Finger über
deinem Scheitel, schiebe die Hände von dir weg, spanne dich lang auf zwischen
Steiß und Knöcheln. Löse die Hände, lasse Arme, Oberkörper und Kopf nach unten
aushängen. Einatmend breite beide Arme seitlich aus wie zwei Flügel,
überstrecke Wirbelsäule leicht und gleichmäßig, richte deinen Blick nach vorne
unten. Ausatmend schwinge beide Arme über die Seiten nach unten durch, beuge
deine Wirbelsäule leicht und gleichmäßig, richte deinen Blick nach hinten
unten, überkreuze beide Arme vor dem Körper, federe mit dem Oberkörper behutsam
mit. Einatmend schwinge beide Arme über die Seiten nach oben und wiederhole die
Übung in deinem Atemrhythmus mehrere Male.

_Sitz_: Finde einen bequemen Sitz, richte dich ein, mache es dir gemütlich,
lehne dich zurück und finde Rückhalt in dir selbst. Du bist der/die König:in
deines Lebens, lass deine Würde dich auf deinem Thron aufrichten. Throne als
König:in deines Lebens auf deinem Thron.

_Tisch_: Lege dich auf den Rücken, strecke Beine und Hände gen Himmel und
strampel mit allen Vieren. Strampel dich frei!

_Armschwingatmung_: Schwinge deine Arme über vorne nach oben und unten,
rhythmisch und kraftvoll. Sei nachgiebig in deinen Gliedern (nutze das Prinzip
nachgiebiger Glieder.), entspanne deinen Brustkorb, lass Körper und Geist frei
mitschwingen. Fühle dich beschwingt. Nutze die Atemimpulse, die sich aus der
Bewegung ergeben, um beim Hochschwingen die Einatmung und beim Herabschwingen
die Ausatmung einzuleiten. Atme durch die Nase ein und durch den Mund aus.

_Berg_: Du bist ein Berg mit breitem, stabilem und mit der Erde verschmolzenen
Bergfuß. Über dieser sicheren Basis richtest du dich mühelos auf. Dein luftiger
Gipfel ragt weit in den Himmel. Von dort überblickst du deine ganze himmlische
Welt.

_Berg_ (Alternative): Nehme die Kontaktflächen deiner Fußsohlen zu Mutter Erde
wahr. Wie gestaltet sich diese gerade aus? Wo berühren die Sohlen den
Untergrund und wo wölben sie sich von ihm weg? (Male die Fläche in Gedanken
mit Lichtfarbe aus.) Verwurzle dich tief in der Erde, die Quelle deiner Kraft,
die dich trägt und dir Sicherheit schenkt. Stelle dir vor wie von Mutter Erde
durch deine Fußsohlen Geborgenheit in Form gleißenden Lichts in dich hinein
strömt, sich überall in dir ausbreitet, dich aufrichtet und zum Erstrahlen
bringt.

Nehme wahr wie sich über deinen stabilen Sohlen dein Stand aufbaut,
Sprunggelenke über Fersen, Unterschenkel, Kniegelenke über Sprunggelenken,
Oberschenkel und Hüftgelenke über Kniegelenken. In deinen Hüftgelenken ruht
dein Becken, das Verbindungsglied von Ober- und Unterkörper, die schalenförmige
Basis deines Oberkörpers, die Heimat der mystischen Kraft namens Kundalini. In
deinem Becken sitzt keilförmig das unterste Segment deiner Wirbelsäule, dein
Kreuzbein, auf welchem sich Wirbel für Wirbel die Lendenwirbelsäule leicht nach
vorne gewölbt aufreiht, gefolgt von der Brustwirbelsäule leicht nach hinten
gewölbt von der jeder Wirbel verbunden ist mit einem Rippenpaar, gefolgt von
der Halswirbelsäule leicht nach vorne gewölbt. Auf dem obersten Halswirbel, dem
Atlas, thront dein Kopf, der Scheitel, der oberste Punkt deines Kopfes, strebt
gen Vater Himmel, der dich inspiriert und dir Kreativität schenkt. Stelle dir
vor wie von Vater Himmel durch deinen Scheitel Schöpfer:inkraft in Form
gleißenden Lichts in dich hinein strömt, sich überall in dir ausbreitet, dich
aufrichtet und zum Erstrahlen bringt.

Und so stehst du strahlend da. Aufgespannt/-gerichtet zwischen Himmel und Erde.
Auf stabilem Grund dem Höchsten entgegenstrebend.

Nehme wahr wie dein Herz beständig für dich und dein Leben schlägt. Dein
Herzraum, die Heimat deiner Liebe, deines Mitgefühls, deiner Mitfreude und
deines Gleichmuts, der Raum in dem Gefühl und Intellekt vereint und
ausbalanciert werden, der Raum über den du mit allen Lebewesen auf dieser Erde
verbunden bist. Stelle dir vor wie von deinem Herzraum liebevolle Güte in Form
gleißenden Lichts in dich hinein strömt, sich überall in dir ausbreitet, dich
aufrichtet und zum Erstrahlen bringt.

Stelle dir vor wie sich Geborgenheit von Mutter Erde, Schöpfer:inkraft von
Vater Himmel und liebevolle Güte vom Herzen in dir vereinen, über deine Arme,
die Verlängerung deines Herzens, in deine Hände strömt und sich von dort in der
Welt manifestiert. Führe deine Handflächen vor deinem Bauchraum zusammen. Reibe
sie fest aneinander. Entfache und schüre ein Feuer zwischen ihnen. Nehme deren
Verbundenheit wahr, nehme wahr wie sie miteinander kommunizieren, wie Energie
zwischen ihnen hin und her strömt. Separiere deine Handflächen behutsam soweit
deren energetischer Kontakt noch wahrnehmbar für dich ist. Spiele mit diesem
Kontakt wie mit einer Energiekugel zwischen deinen Handflächen, die größer und
kleiner wird, still ist und pulsiert. Suche dir einen Körperraum, der gerade
nach liebevoller Aufmerksamkeit verlangt und umschließe diesen mit deinen
Händen, durchströme ihn mit liebevoller Güte.

_Held:in/Krieger:in II_: Du bist der/die Held:in. Beide Füße flächig geerdet.
Beide Beine kraftvoll und leicht. Ein fester und leichtfüßiger Stand. Der
Oberkörper ist zentriert und richtet sich einfach auf. Das Haupt strebt gen
Himmel. Über den hinteren Arm findest du Rückhalt in der Vergangenheit
(beispielsweise erlernte Fertigkeiten). Der vordere Arm greift voll Klarheit in
die Zukunft. Der Blick ist nach vorne in die Ferne ausgerichtet deiner Visionen
entgegen. Du selbst bist aufgerichtet zwischen dem dich inspirierenden Himmel
und der dich bergenden Erde und ruhst zwischen Vergangenheit und Zukunft in der
Gegenwart mit klarer Ausrichtung, voll Vertrauen, Zuversicht und Hingabe und
ohne Anhaftung an mögliche Erfolgsaussichten des gegenwärtigen Handelns/Seins.
Du bist fest verankert im Hier (Raum) und Jetzt (Zeit) und gleichzeitig
flexibel, offen und bereit für alles was kommen mag.

_Friedfertige/r Krieger:in_: Du offenbarst deine Stärke durch Verletzlichkeit.
Du gibst dich auf starke, selbstsichere, bewusste und absichtliche Weise preis.
Du öffnest dich deinen Herausforderungen. Exponiert.

_Krieger:in I_: Mit den Beinen bist du verbunden mit Vergangenheit und Zukunft.
Die Vergangenheit gibt dir Rückhalt. Die Zukunft liegt vor dir, du schaust sie
voll Mut und wagst einen Schritt in sie hinein. Du bist hoch aufgerichtet, groß
und majestätisch. Über die himmelwärts strebenden Arme empfängst du
Inspiration, Kreativität, Schöpfer:inkraft, kurz das göttliche Wort
(Weisungen). Du bist offen und bereit aus vollem Herzen zu leben.

_Krieger:in III_: Du bist in vollkommener Balance, körperlich im Gleichgewicht,
emotional in Ausgeglichenheit, mental in Ruhe, seelisch in Harmonie, kurz in
Synchronität mit dem Universum. Dein Standbein ist kraftvoll, geschmeidig und
feinfühlig (fein abgestimmt). Dein gehobenes Bein verbindet dich mit der
Vergangenheit und stößt dich von ihr ab. Dein Kopf schaut voraus und stößt in
die Zukunft vor. Deine aktivierte und stabile Rück- und Vorderseite ist mit
Himmel und Erde ausgerichtet und erfährt durch diese Schaffenskraft und
Geborgenheit.

_Beckenschaukel_: Lass Wellen der Entspannung von deinem Becken durch dich
hindurch wandern bis zum Scheitel und zu den Finger- und Zehenspitzen. Bespiele
deinen Körper wie die Saite eines Instruments. Variiere in Rhythmus und
Amplitude, schaukle mal rasch und fein und mal gemächlich und ausladend. Erlebe
die Freude des spielerischen Erkundens. Komme langsam zur Ruhe, lass die
Bewegung aus- und nachschwingen, verfolge sie bewusst bis zur Stille und
darüber hinaus. Wie schwingt es in dir in Bewegungsstille nach? Wo strömt es,
wo kribbelt es, wo ist es warm, wo kühl? Begreife dich als pulsierende Energie,
als Schwingung, als Vibration, als (stehende/wandernde) Welle.

_Strecken_: Stelle dir vor über dir breitet sich eine ausladende, saftig grüne
Baumkrone aus deren Zweige erlesene Früchte tragen mit einer Qualität, die du
in dein Leben holen möchtest. Greife nach diesen Früchten in alle
Himmelsrichtungen, mache dich ganz lang, hole die schönsten, am weit
entferntesten zu dir, fülle den ganzen Raum um dich mit ihnen an, baden in
ihnen und in der Qualität, die sie für dich tragen, übergieße dich damit, gieße
sie in dich hinein, hole sie zu dir, verankere sie in dir, beheimate sie in
dir, lass sie dich vollkommen ausfüllen, vollkommen erfüllen, lasse sie
erstrahlen, offenbare sie, mache sie dir zu eigen. Und wann immer du diese
Qualität fortan wünschst, wisse sie ist bei dir, dass du sie in dir findest und
hole sie.

_Strecken_ (Alternative): Stelle dir vor über dir erstrahlt ein funkelnder
Sternenhimmel und das jeder Stern eine Qualität trägt, die du in dein Leben
einladen möchtest. Erlaube dir das, hole die Qualität zu dir, greife nach den
Sternen. Fülle den ganzen Raum um dich damit auf, schöpfe mit vollen Händen
daraus, übergieße dich damit, reibe sie in dich hinein, beheimate sie in dir.

_Strecken_ (Metapher): Wachse über dich hinaus.

_Namaste_: Führe deine Handflächen vor deiner Stirn zusammen für klare
Gedanken, vor deinen Lippen/Mund für ehrliche Worte und vor deinem Herzen für
ein liebevolles, mitfühlendes, mitfreuendes und gleichmütiges Wesen.

_Übergang_ (Alternative): Gönne dir Stille, Zeit zum inneren Widerhall, zur
intuitiven Reflexion, zur Integration. Wie bist du jetzt hier? Was hallt in
dir wider? Welche Erfahrungen möchtest du hier lassen und welche mitnehmen?
Ich lade dich ein dir vorzustellen wie die Erfahrungen, die du mitnehmen
möchtest, in dich hinein tauchen während du in die Erfahrungen eintauchst, wie
sie einen Platz in dir finden, sich dort verankern und in dir beheimaten.
