---
title: Yoga
summary: Weg der Selbsterkenntnis
date: 2023-02-09T08:05:52+0100
layout: single
table_of_contents: true
---

Yoga (Sanskrit योग; von yuga ‚Joch‘, yuj für: anjochen, zusammenbinden,
anspannen, anschirren)

Raja Yoga ist der Weg der Selbsterkenntnis, der Wiederverbindung des
persönlichen Ich mit dem höheren Selbst, mit dem/der innere(n) König:in. Weg
aus Selbstversklavung in Freiheit, aus Unbewusstheit in Bewusstheit, aus
Isoliertheit in Einheit. Einheitsbewusstsein.

Wiederentdeckung und Verwirklichung des höheren Selbst.

1. Beobachte dein Ich, deine Gedanken und Gefühle, durch Selbstgewahrsein,
   durch Innenschau.
1. Bahne dir durch Innenschau einen Weg durch deine Gedanken. Nähere dich mit
   entschlossenem Willen deinem inneren Wesenskern.
1. Dringe durch Hingabe, durch Fügen in deinen Wesenskern ein, dem
   abstrahlenden Einfluss der Psyche entgegen. Sitze geduldig, demütig und
   ausdauernd vor der Tür des Überselbst.

Bewusstsein, Unterbewusstsein, Überbewusstsein.

Bringe dein Denken, dein Wollen (deine Absicht, deine Vision, deine
Ausrichtung, deine Intention, deine Haltung, deine Motivation, deine Ambition),
dein Fühlen, dein Spüren in Einklang, sei ein Klang des Universums, eine
vollkommener Ton, eine harmonische Symphonie.

Nach [Michael Bernard Beckwith](https://michaelbeckwith.com/) gibt es vier
Stufen spirituellen Erwachens: Das Leben passiert mir (Englisch: to me), ich
gestalte das Leben (Englisch: by me), das Leben entfaltet sich durch mich
(Englisch: through me), ich bin das Leben (Englisch: as me).

## Sitzung

### Prüfungsstunde

1. Fühle dich mit allem was du dabei hast willkommen.
1. Aufgespannt zwischen Himmel und Erde über Herz mit allen
   Lebewesen/Kreaturen/Schöpfung verbunden
   - Erde trägt und birgt und schenkt Boden und Geborgenheit
   - Himmel hält und inspiriert und schenkt Aufrichtung und Kreativität
   - Herz erquickt und schenkt Liebe
1. Alle Bewegungsrichtungen der Wirbelsäule ansprechen
   - Arme schwingen (hoch/runter)
   - Arme pendeln (links/rechts)
   - (Seitbeuge)
1. Atem erwecken
   - Klopfatmung (oder Ha-Atmung)
   - (Vitalisatmung (dazwischen aushängen lassen))
1. Erlesene Früchte, die Qualität/Kraftsatz bergen, die du in dein Leben
   einladen möchtest, aus Baumkrone pflücken und in dich hinein sprechen, in
   dir widerhallen lassen, sie verinnerlichen, sie in dir beheimaten.
1. Vorbeuge
1. Im Fersensitz
   - Kindhaltung
   - Seitbeugen
   - Drehsitz
   - Diagonale Öffnung
1. Atem
   - Atemgewahrsein (Atem als Anker in der Gegenwart, geleitet zurück ins Hier
       und Jetzt)
   - Atemräume erkunden
   - Kapalabati
   - Uddiyana-Bandha (Hände auf Kraft-/Macht-/Sonnenzentrum legen)
   - Wechselatmung
1. In Rückenlage
   - Schwerkraft hingeben, Boden anvertrauen, in Boden schmelzen,
       Licht/kosmischer Energie öffnen, Himmel vertrauen
   - Schulterbrücke
   - Knie/Beine von Seite zu Seite
   - (halber) Pflug und Schulterstand
   - Wirbelsäulenverdrehung mit übergeschlagenen Beinen
1. ...
   - Fersenwippe
   - Schlangenbewegung mit Armen über Schultergürtel
   - Beckenwellen
1. Shavasana
1. Klangteppich weben

Idee

- Ströme von Erde, Himmel und Herz visualisieren als gleißendes Licht
- Durch Hände, die Verlängerungen des Herzens, in die Welt tragen/manifestieren
- Energieball zwischen Händen formen und damit spielen
- Hände mit dazwischen strömender Energie auf bedürftigen Körperteil auflegen
    und bestrahlen
- Mit Pusten zwischen den Händen auflösen

### Körperstunde

1. Ankommen: Was bringe ich vom Tag mit? Wie bin ich hier und jetzt da? Wie
   erlebe ich den Raum? Wie erlebe ich die Gruppe? Wie erlebe ich mich?  Was
   benötige ich um anzukommen? Was möchtest du loslassen? Spreche es kraftvoll
   aus.
1. Verbinden: Befindlichtkeitsrunde/Namensrunde
1. Qualität: Du badest in ihr. Einatmung einströmen. Ausatmung verteilen.
   verinnerlichen/beheimaten
1. Löwenbrüllen: Mähne schütteln, Augen aufreißen, Gesichtsmuskeln spielen
1. Klangteppich weben (tönen): Welcher Ton möchte sich zeigen?
1. ...
   1. Arme-Schwing-Atmung (auf und ab)
   1. Arme seitlich schwingen
   1. Vorbeuge
   1. Vitalis-Atmung
1. Atembeobachtung
1. Atemkontrolle
   1. verlangsamen, rhythmisieren, in Bauch lenken
   1. in Körperräume lenken (taktile Reize helfen, insbesondere Bauch, Brust,
      Schlüsselbeine)
   1. Seitneigung und in Seite atmen
   1. aufsteigende Welle
   1. Drehung und in Seite atmen
1. Kapalabati (in verschiedenen Körperhaltungen)
1. Wechselatmung
1. In Rückenlage
   1. Bein Scheibenwischer
   1. überschlagene Beine ranziehen
   1. Rotation
1. Die drei Schaukeln/Wellen
1. Was möchtest du mitnehmen? Was hierlassen?

### Atemstunde

1. Atembeobachtung: Wie da?
1. Klangteppich weben
1. stehend
   - Armschwingatmung (davor strecken)
   - Vitalisatmung (davor drehen)
   - Ha-Atmung (davor beugen/aushängen)
   - Flügelatmung
1. sitzend
   - Kapalabati
   - Agni Sara Dauti
   - Udiyana
1. Umkehrhaltung
1. liegend
   - Scheibenwischerbeine
   - Rockings
   - Totenstellung

### Ideen

Counseling/Diade: Einseitiger Austausch, beispielsweise zur Frage "Wozu bin ich
hier?" Raum nehmen und Raum halten.

Verbinden: Augenpaar finden. Verbinden. Zurück.

Organreise und Verdauungstraktreise

## Forschungsfragen

Selbstbeobachtungskaskade: Die Instanz beobachten, die beobachtet, die
beobachtet, ... was im inneren geschieht.

Yoga der Gefühle: Innenwelt in Gefühlszuständen beobachten.

Augen in Übungen einbeziehen: Beispielsweise bei Drehungen in dieselbe Richtung
drehen oder in die entgegengesetzte oder sie im Verhältnis zum Kopf unbewegt
lassen oder auf einen Punkt fixieren. Augenbewegung ist verbunden mit
Interpretation des Nervensystems "Ich bin sicher". Kann auch unabhängig geübt
werden.

Welchen Einfluss hat die Haltung des Kopfes auf die Haltung des Körpers? Über
die Makulaorgane ist wird Kopfhaltung in Bezug zum Magnetfeld der Erde
wahrgenommen und entsprechend feine Veränderungen in der Haltung vorgenommen
(so steht es in "Anatomie des Hatha Yoga").

Wahrnehmung auf allen Ebenen bei Haltungen und Übergängen. Beispielsweise
könnte bei Gleichgewichtshaltungen (feine) Angst aufkommen oder Spielfreude.

Wie erlebe ich Balance mit auf einen Punkt fixierten Augen, wie mit
geschlossenen Augen und wie wenn ich meinen Kopf wild bewege? Augen vs.
Gleichgewichtsorgan vs. Propriozeption (Tiefenwahrnehmung).

Achgliedrigen Pfad erfahrbar machen:

- Samadhi (Fixierung) mit Form: Sein/Verkörperung einer Haltung
- Dhyana (Meditation): Müheloser Fluss von Aufmerksamkeit. Beispielsweise
  Ausruhen in Augenräumen.
- Dharana (Konzentration): (Ein)Spitzigkeit des Geistes. Beispielsweise
  Ausrichten auf Augenräume oder Atembeobachtung
- Pratyahara (Versenkung): Sinneszurückziehung.
  1.  Bewusst wahrnehmen: Details studieren.
  1.  Kontrolliert wahrnehmen: Wahrnehmung lenken, zwischen Sinnen wechseln,
      mehrere Sinne zugleich.
  1.  Vom Handeln entkoppeln: Vom reaktiven zum reflektierten Handeln.
      Zulassen, loslassen, lösen, gelöst.
  1.  Von Gedanken entkoppeln: Bewunderung/Begeisterung ("Wow") statt
      Kategorisierung, Konzeptionalisierung und Instrumentalisierung. Vom
      Reflektieren zum Wahrnehmen.
  1.  Sinne nach innen richten:
      1. Innere Bilder voll Formen, Farben und Licht/Glanz. Innere
         Landschaften, innerer Weitblick, innerer Ausblick. Licht, Helligkeit,
         Sättigung, Klarheit, ...
      1. Innerer Klang/Töne in verschiedenen Lautstärken, Melodien, Rhythmen,
         ...
      1. Innerer Geruch voll Intensität, Noten, Tiefe, ...
      1. Innerer Geschmack: süß, salzig, bitter, sauer und im Westen umami und
         im Ayurveda scharf und zusammenziehend.
      1. Inneres Gespür (Tiefensensibilität bestehend aus Lagesinn, Kraftsinn
         und Bewegungssinn): Atmung, Herzschlag, Puls und andere rhythmische
         Prozesse.
- Pranayama + Bandhas (Verschlüsse) + Mudras (Siegel): Atemstille und
  Pranaspeicherung. Arbeit mit Sonnengeflecht (Pranaspeicher, Kraft- und
  Machtzentrum).
- Asana: Schneidersitz mit einer Ferse am Damm (welche?) und andere am
  Geschlecht (energetische Wirkung, Kundalinierweckung).
- Yamas und Niyamas: Beispielhaft Tapas und Wahrhaftigkeit.

## Themengebiete

- Gelenke, Knochen, Muskeln, Sehnen, Bänder
- Wirbelsäule
- Gruppe, energetischer Kontakt (Nervensystem kommunizieren), Augenkontakt,
  physischer Kontakt (Berührung), ...
- Plexen
- Diaphragmen
- Drüsen und Hormone
- Organe
- Chakren: https://www.masterclass.com/articles/what-are-the-chakras und https://www.arhantayoga.org/blog/7-chakras-introduction-energy-centers-effect/
- Bandhas
- Mudras
- Koshas
- Pranas
- Kundalini
- Innere Anteile / Instanzen des "mind"
- Ausscheidungssysteme (Ausatmung, Enddarm, Schweißdrüsen, Mund/Hände für
  Gedanken-/Gefühlsausscheidung, Blase/Harnröhre)
- historische Texte wie die Vedas, Vedanta, Upanishaden, Bhagavad Gita, Yoga
  Sutras, Hatha Pradipika

Chakren oberhalb des Herzens: Denken. Unterhalb: Fühlen. Herz ist Ort, in dem
Denken und Fühlen Balance finden (in Balance sind). Herz ist Zentrum des
Chakrensystems.

Was ist urmenschlich? Das wir soziale Wesen sind. Wie kann ich dies
wiederentdecken? Das Gegenteil von urmenschlich ist Entfremdung von sich und
von anderen. Verbinde dich mit dir und mit anderen.

Übe Achtsamkeit für dich selbst, für die Schulung deiner Selbstwahrnehmung, für
deine Entspannung und _nicht_ um in Achtsamkeit besser zu werden.

Basis: Safety (no need to freeze), satisfaction (no need to fight or flee),
connection (no need to stay alone). Meditiere über (körperliche) Sicherheit
durch Frieden (peace), über Befriedigung durch Zufriedenheit (contentment) und
über Verbindung durch Liebe (love).

Verwandle flüchtige/vergängliche mentale Zustände (Englisch: fleeting mental
states) in anhaltende/andauernde/dauerhafte/bleibende/beständige neuronale
Merkmale/Strukturen (Wesenszüge/Charakterzüge; Englisch: lasting neural
traits), indem du dir die Zustände (Erfahrungen) ins Bewusstsein holst
(Englisch: have an experience), sie be-/anreicherst (Englisch: enrich), sie
verinnerlichst/aufsaugst/absorbierst/einsaugst/aufnimmst (Englisch: absorb) und
sie mit herausfordernden Erfahrungen
verwebst/verbindest/verknüpfst/koppelst/vereinigst (Englisch: link). Der Aufbau
neuronaler Strukturen wird gefördert durch Dauer (Englisch: duration),
Intensität (Englisch: intensity),
Multimodalität/Vielschichtigkeit/Vielartigkeit/Vielfältigkeit (Englisch:
multimodality), Neuartigkeit (Englisch: novelty) und persönliche Relevanz
(Englisch: personal relevance). Eine Erfahrung zu haben, anzureichern und zu
absorbieren ist wie ein Feuer zu entfachen/entzünden, es zu
schüren/füttern/erhalten und sich von dessen Wärme erfüllen/nähren zu lassen.
Die Phasen der Anreicherung und Absorption vermengen sich und überlappen oft:
Während das Feuer mit Holzscheiten geschürt wird, ist dessen Wärme erfüllend.
Visualisierung für Absorbieren: Hinein/Hinunter rieseln wie sanfter Nieselregen
oder weicher Goldstaub. Oder wie ein tropfen Lebensmittelfarbe, der sich in
einem klaren Glas Wasser gemächlich ausbreitet (oder Sahne/Milch in Kaffee) (im
Gegensatz zu einem tropfen Öl im Wasser --- flüchtiger Zustand). Das Mind (oder
Gehirn) um eine positive Erfahrung formen/modellieren/gießen/prägen. Offen
dafür sein von der Erfahrung geändert zu werden.

Mache dir deine Ressourcen bewusst. Starte mit erfüllten, freudvollen,
wohltuenden, leichten Mikromomenten.

Reguliere dein Nervensystem durch Präsenz, Körperachtsamkeit und soziale
Interaktion.

Wechselwirkung/Kontrolle/Einflussnahme: Das willkürliche und das
vegetative/autonome Nervensystem stehen in Wechselwirkung beispielsweise wirkt
sich das bewusste Denken an nährende Situationen auf das Vegetativum aus und
umgekehrt beeinflusst die Neurozeption, die unterbewusste Einordnung des
Nervensystems ob eine Situation sicher ist, das bewusste Denken.
Muskelanspannung gestreifter Muskulatur kann absichtlich oder reflexartig
geschehen. Glatte Muskulatur kann nur unabsichtlich angespannt oder entspannt
werden, aber durch mein Denken kann ich das eine oder andere befördern.

### Bandhas

See also https://www.arhantayoga.org/blog/the-four-bandhas/

https://www.masterclass.com/articles/types-of-bandhas

Körperlich-energetische Verschlüsse namens Bandhas sind muskuläre
Kontraktionen, die vorübergehend den Blutfluss in bestimmte Körperareale
einschränken. Auf energetischer Ebene lenken sie den Energiefluss hin zu oder
weg von bestimmten Körperarealen. Wenn du den Verschluss löst, stärkt dies den
Kreislauf, erhöht den Blutfluss, spült tote Zellen aus und verjüngt und stärkt
Organe des angesprochenen Areals. Außerdem können Bandhas die
Konzentrationsfähigkeit verbessern und die Verdauungs-, Stoffwechsel-, Hormon-
und sexuelle Gesundheit unterstützen. TODO Mit energetisch ist Prana Shakti
gemeint.

Damit Prana Shakti frei fließt, muss Pingala Nadi offen und rein sein. Mit
Bandhas kannst du den Pranafluss regulieren und kanalisieren und dadurch
folgende Ziele erreichen:

- Reinigung
- Energieblockaden auflösen
- Prana im Körper speichern/anreichern
- Prana von energiereichen zu energiearmen leiten/lenken
- Chakren stimulieren um das Erwachen von Kundalini (Schlangenkraft; ein
  erweiterter Bewusstseinszustand) vorzubereiten
- Das Selbst ausbalancieren und harmonisieren

Im Hatha Yoga gibt es sechs Bandhas, drei wesentliche/bedeutende/Haupt-, zwei
unerhebliche/niedere/geringere/Neben- und den großartigen Verschluss.

1. Mula bandha: This bandha, also known as the root lock, directs the energy
   flow to the rectum to stimulate the pelvic muscles and urogenital organs. To
   engage the root lock, contract the perineum muscles inward and upward. For
   men, this is the region between the testes and the anus. For women, it
   involves the pelvic floor muscles behind the cervix. You can also locate the
   Mula bandha by looking at the tip of your nose: Yogis may feel the root lock
   engage when doing this.
2. Jalandhara bandha: Also known as the throat or chin lock, this bandha
   inhibits the flow of Prana through channels in the neck area. Practitioners
   typically combine Jalandhar with other locks for relaxation and stress
   relief. To engage the throat lock, sit cross-legged with your back straight.
   Press your palms into your knees, straighten your elbows, and inhale through
   your nose, pulling your chin toward your neck. Magnify the effect by curling
   your tongue against the roof of your mouth, a movement known as Khesari
   mudra. Learn more about yoga mudras.
3. Uddiyana bandha: Sometimes referred to as the false inhale, the Uddiyana
   bandha involves lifting the diaphragm. To engage this bandha, stand and
   spread your feet shoulder-distance apart. Bend forward, keeping your back
   straight, and place your hands on your knees or shins, depending on your
   flexibility. Holding this position, pretend to inhale without taking in air.
   Your ribs should protrude over your abs, and you should feel your abdominal
   wall and internal organs pushing up and back. Hold the lock as long as
   possible, then release and take a deep breath through the nose. Yoga
   practitioners often consider this lock a remedy for stomach upsets and
   abdominal pains and use it to stimulate digestion.
4. Hasta bandha: This minor bandha, known as the hand lock, may reduce wrist
   pain during certain yoga positions such as downward dog or crow pose. To
   engage Hasta bandha, place your hands on the yoga mat, spreading your
   fingers far apart to provide a solid base of support. Slowly put weight onto
   your hands, letting the area where your thumb and pointer finger meet carry
   the most weight. Rotate your forearm, if necessary, to direct your weight
   onto this area. Then, lightly grip the mat with your fingertips. You should
   feel no pressure in the center of your palm. Practice this lock regularly to
   develop strength and comfort.
5. Pada bandha: This minor bandha, also known as the foot lock, connects your
   body with the earth. To activate the foot lock, place the soles of your feet
   on the ground so that your weight is supported by the triangle between your
   big toe, little toe, and ankle.
6. Maha bandha: The final seal, the Maha bandha, is considered the great lock
   because it engages when you simultaneously activate the three major locks.
   To begin Maha bandha, first, engage the Mula bandha. Exhale completely and
   then activate the Jalandhar bandha. Finally, bend forward and engage the
   Uddiyana bandha. To release the Maha bandha, disengage each bandha in the
   reverse order.

### Diaphragmen

[embodiment etudes: The Foot](https://embodimentetudes.com/2011/11/27/the-foot/)

[diaphragms](https://embodimentetudes.com/?s=diaphragm)

[stacking the diaphragms](https://embodimentetudes.com/2016/08/22/stacking-the-diaphragms/)

[Die 8 Diaphragmen](https://ommeducation.com/2015/04/the-bodys-diaphragms/)

https://flexikon.doccheck.com/de/Diaphragma Der Begriff Diaphragma beschreibt
eine Trenn- oder Scheidewand, im engeren Sinne bezieht er sich auf das
Zwerchfell.

- Diaphragma ([Zwerchfell](https://flexikon.doccheck.com/de/Zwerchfell)): Das
  Zwerchfell ist der zentrale Atemmuskel, der die untere Thoraxaperatur zum
  Abdomen hin verschließt.
- [Diaphragma pelvis](https://www.kenhub.com/de/library/anatomie/diaphragma-pelvis),
  Diaphragma urogenitale, Schwellkörper- und Schließmuskelschicht
  ([Beckenboden](https://flexikon.doccheck.com/de/Beckenboden) und
  https://de.wikipedia.org/wiki/Beckenboden): Als Beckenboden bezeichnet man
  die untere Begrenzung des Beckenkanals. Die anatomische Grundlage des
  Beckenbodens bildet die Beckenbodenmuskulatur (perineale Muskulatur).
- Diaphragma oris ([Mundboden](https://flexikon.doccheck.com/de/Mundboden) und
  https://www.thieme-connect.de/products/ebooks/lookinside/10.1055/b-0034-88216):
  Der Mundboden ist ein mehrschichtiges Weichteilareal zwischen dem
  Unterkieferkörper (Mandibula) und dem Zungenbein (Os hyoideum), das die
  kaudale Begrenzungsfläche der Mundhöhle bildet.
- Diaphragma sellae ([Duplikatur der Dura
  Mater](https://flexikon.doccheck.com/de/Diaphragma_sellae) und
  https://de.wikipedia.org/wiki/Keilbein#Innenrelief)
- Diaphragma styloideum
- Diaphragma des fenestrierten Endothels (Histologie)

[Das Zwerchfell: Der Dirigent im Atemorchester](https://stefan-hoeppner.net/media/files/vortrag-zwerchfell.pdf)

https://de.wikipedia.org/wiki/Zwerchfell Das Zwerchfell oder Diaphragma (von
altgriechisch διάφραγμα diáphragma, deutsch ‚Trennwand‘ bzw. ‚Zwerchfell‘) ist
eine Muskel-Sehnen-Platte, welche bei Säugetieren die Brust- und die Bauchhöhle
voneinander trennt. Es hat eine kuppelförmige Gestalt und ist der wichtigste
Atemmuskel. Die Muskelkontraktion des Zwerchfells führt zu einer Einatmung
(Inspiration). Beim Menschen ist es 3 bis 5 mm dick und leistet in Ruhe 60 bis
80 % der zur Inspiration benötigten Muskelarbeit. Beim Ausatmen (Exspiration)
entspannt sich das Zwerchfell. Durch die elastischen Fasern in der Lunge sowie
die Oberflächenkräfte in den Lungenbläschen (Retraktionskräfte) zieht sich die
Lunge zusammen und das Zwerchfell wieder in die Kuppelform zurück. Die
Ausatmung vollzieht sich während der Atmung in Ruhe also ohne aktive Mitwirkung
von Muskeln. Neben der Atemfunktion kann das Zwerchfell zusammen mit den
Bauchmuskeln zum Druckaufbau in der Bauchhöhle eingesetzt werden, nämlich wenn
sie sich gleichzeitig kontrahieren, die Vorwölbung des Bauches also unterbunden
wird. Dies findet z. B. beim Stuhlgang oder bei Presswehen statt. Bei
Ausatemtechniken wie der Atemstütze wirkt das Zwerchfell mit der übrigen
Atemmuskulatur zusammen. Im Sport hat die Zwerchfellmuskulatur eine doppelte
Funktion: Einerseits ist sie durch das intensive Atmen besonders gefordert,
andererseits trägt die angespannte Muskulatur zur Stabilisierung des
Oberkörpers bei.

https://www.myyogamaya.de/beckenboden-entspannen-ueber-deinen-kiefer-ja-mit-3-uebungen/
Dein Beckenboden hat aber auch reflektorische Verbindungspunkte in deinem
Körper. Das heißt, wenn Du diese Verbindungspunkte triggerst, erfährt dein
Beckenboden unwillkürlich einen Reiz. Dein Beckenboden spricht darauf an. So
kannst Du im Prinzip über andere Körperbewegungen auch deinen Beckenboden
beeinflussen. Und hier kommt dein Kiefer ins Spiel. Wenn dein Kiefer gut
entspannt ist, kann auch dein Beckenboden entspannen. Gerade deshalb ist es so
wichtig, unter einer Wehe beispielsweise den Kiefer und deinen Mund nicht
zusammenzubeißen oder zusammenzupressen. Zur Entspannung des Beckenbodens gibt
es drei Übungen für den Kiefer: 1. Den Kiefer massieren, 2. Mit der Zunge den
Innenraum ausfahren, 3. Kiefernseite in eine Hand legen.

[Beckenboden in der Yogapraxis](https://yoganathalie.com/der-beckenboden-kompass-in-der-yogapraxis/)

[Beckenbodentraining: Die Atmung – wie das Zwerchfell dich unterstützt](https://ergobaby.de/blog/beckenbodentraining-die-atmung-wie-das-zwerchfell-dich-unterstutzt/)

[Die Bedeutung der Atmung für den Beckenboden](https://om-site.com/bedeutung-der-atmung-fuer-den-beckenboden/)

[Die Atmung: Physiologie und Bedeutung für die Gesundheit](https://om-site.com/atmung/)

[Zusammenarbeit von Zwerchfell und Beckenboden](https://atematelier.ch/die-zusammenarbeit-von-zwerchfell-und-beckenboden/#:~:text=Beim%20Atmen&text=Die%20Atemdruckwelle%20pflanzt%20sich%20nach,das%20Zwerchfell%20und%20steigt%20hoch.):
Für die Atmung ist das Zusammenspiel von Zwerchfell und Beckenboden
antagonistisch. Für die Bauchpresse arbeiten Beckenboden und Zwerchfell
synergistisch zusammen. Durch die Atemmuskulatur, die an verschiedenen Stellen
der Wirbelsäule ansetzt, wird die Wirbelsäule im Einatem leicht gestreckt und
im Ausatem wieder in ihre ursprüngliche Stellung zurückgebracht; die
Atembewegung belüftet auf diese Weise mit jedem Atemzug die Bandscheiben und
flexibilisiert die Haltemuskulatur der Wirbelsäule.

[Atmende Helixspannung: Stabilität für deinen Körper](https://de.ashtangayoga.info/yoga-therapie/yogatherapie-verstehen/190730-atmende-helixspannung-s/)

Pascal Beaumart erläutert in einem Video den Zusammenhang zwischen Zwerchfell,
Psoas, (Neben)nieren und langem Sitzen, Stress, ...

[Pelvic Floor Release](https://pelvicphysiotherapy.com/pelvic-floor-release/):
Sniff, Flop, Drop

--

9. Vers: Mäßig und diszipliniert in allen seinen Aktivitäten, reguliert der
   Weise seinen Atem (im Sitz der Meditation) und wenn er sanft geworden ist,
   atmet er durch die Nasenlöcher aus. Wie er einen von wilden Pferden
   gezogenen Wagen lenkt, so hält er den Geist einspitzig und wachsam.

   Aus [Shvetashvatara Upanishad (Devanagari: श्वेताश्वतर उपनिषद; Śvetāśvatara)](https://wiki.yoga-vidya.de/Shvetashvatara_Upanishad)

Und nun Ujjayi: Der Mund wird verschlossen. Langsam wird der Lebenshauch durch
beide Nasenlöcher hineingesogen, | so dass er wahrlich von der Kehle bis nach
unten zum Herzen mit einem lauten Geräusch in Kontakt kommt. ||51||

Wie zuvor (beschrieben) soll (der Yogi) den Atem anhalten. Dann soll er durch das linke Nasenloch ausatmen. |
Dieses zerstört eine Neigung zur Trägheit in der Kehle und aktiviert das Körper-Feuer. ||52||

Die Atemübung, die als Ujjai bekannt ist zertört Disbalancen in Verbindung mit
den Energiekanälen, dem Wasserhaushalt und den körperlichen Grundeigenschaften.
| (Ujjai) kann sicherlich in Bewegung und in Ruhe praktiziert werden. ||53||

Aus der Hatha Pradipika: https://de.ashtangayoga.info/philosophie/quelltexte-und-mantren/hatha-yoga-pradipika/kapitel-2/

Der richtige Weg für dich berührt dein Herz (hat Herz), schafft Verbindung,
Mitgefühl, Mitfreude, ... --- Jürgen Ries

“Look at every path closely and deliberately. Try it as many times as you think
necessary. Then ask yourself and yourself alone one question. This question is
one that only a very old man asks. My benefactor told me about it once when
I was young and my blood was too vigorous for me to understand it. Now I do
understand it. I will tell you what it is: Does this path have a heart? If it
does, the path is good. If it doesn’t, it is of no use.”

Carlos Castaneda

The teachings are about finding such a path with heart, about undertaking
a path that transforms and touches us in the center of our being. To do so is
to find a way of practice that allows us to live in the world wholly and fully
from our heart.

[Jack Kornfield](https://jackkornfield.com/a-path-with-heart-3/)

The fourth type of pranayama transcends the external and internal pranayamas
and appears effortless and non-deliberate.

Pranyama removes the veil covering the light of knowledge and heralds the dawn
of wisdom.

Then the mind becomes fit for concentration.

Aus den Yoga Sutras: https://www.karineisen.com/blog/yoga-sutras-2-49-to-2-53-pranayama

Yogash Chitta Vritti Nirodhaha – Yoga ist das Zurruhe-Bringen der Bewegungen
des Geistes
Chitta Vritti (Sanskrit: चित्तवृत्ति cittavṛtti
Vritti (Sanskrit: वृत्ति vṛtti f.) Aktion, Bewegung; Verhalten
Chitta (Sanskrit: चित्त citta n.) das Aufmerken; das Denken

Aus [Raja Yoga Sutras, 1.2](https://wiki.yoga-vidya.de/Yogasutra)

Nachdem dieser [Meditationssitz (āsana)] (tasmin) vollkommen (sati) [ist],
[ist] das Anhalten (viccheda) des Laufs (gati) von Ein- (śvāsa) und Ausatmung
(praśvāsa) die Atemübung (prāṇāyāma).

Die Bewegung (vr̥tti) vom Ausatmen (bāhya), Einatmen (abhyantara) und Anhalten
(stambha) [des Atems soll] sehr fein gemessen [werden] nach Ort (deśa), Zeit
(kāla) und Anzahl (saṁkhyā).

Die vierte (caturtha) [Bewegung (vr̥tti) des Atems] betrifft den Bereich
(viṣayākṣepīn), an dem Aus- (bāhya) und Einatmung (abhyantara) [miteinander
verschmelzen].

Aus den Yoga Sutras: https://de.ashtangayoga.info/philosophie/quelltexte-und-sanskrit/yoga-sutra/yoga-sutra-2-ueber-die-spirituelle-praxis-ronald-laura/201211-yoga-sutra-249-51-ueber-die-atemkontrolle-pranayama-ronald-laura/

Pranayama is mentioned in verse 4.29 of the Bhagavad Gītā, which states "Still
others, who are inclined to the process of breath restraint to remain in
trance, practice by offering the movement of the outgoing breath into the
incoming, and the incoming breath into the outgoing, and thus at last remain in
trance, stopping all breathing. Others, curtailing the eating process, offer
the outgoing breath into itself as a sacrifice."[6][7]

Von https://en.wikipedia.org/wiki/Pranayama#Bhagavad_G%C4%ABt%C4%81

Wieder andere, die dazu neigen, den Vorgang der Atembeherrschung zu
praktizieren, um in Trance zu bleiben, üben sich darin, den ausströmenden Atem
in den einströmenden und den einströmenden Atem in den ausströmenden zu opfern.
So erreichen sie letztlich Trance, indem sie alles Atmen einstellen. Andere,
die das Essen einschränken, bringen den ausströmenden Atem ihm selbst als Opfer
dar.

Von https://vedabase.io/de/library/bg/4/29/

Weitere Pranayamas: https://de.wikipedia.org/wiki/Pranayama

Wechselatmung: https://de.ashtangayoga.info/philosophie/quelltexte-und-sanskrit/dattatreyayogashastra-ronald-nils-jacob/dys-59-65-pranayama-technik-ronald-nils-jacob/

Und andere Pranayamas: https://de.ashtangayoga.info/philosophie/quelltexte-und-sanskrit/dattatreyayogashastra-ronald-nils-jacob/

--

Ein Wagenfahrer ist, wisse Der Atman,
Wagen ist der Leib,
Den Wagen lenkend ist Buddhi
Manas, wisse der Zügel ist.

Know the atman as the lord of the chariot, the body as only the chariot, know
also intelligence as the driver; know the minds as the reins.

[Katha Upanishad Vers 3.3.](https://www.wisdomlib.org/hinduism/book/katha-upanishad-shankara-bhashya/d/doc145206.html)

Nachtrag: Angejochtes Ochsengespann sind Sinne.

Atman (Sanskrit, n., आत्मन्, ātman, Pali: atta, urspr.: Lebenshauch, Atem)
Manas (Sanskrit: मनस् manas n.) der innere Sinn, das innere Organ, Denkorgan, Geist, Sinn, Verstand, Wille, Denken, Gedanke, Denkprinzip, aber dazu gehört auch Denken und Fühlen; Gemüt, Seele, Gewissen, Herz; Erkenntnisvermögen.
Buddhi (Sanskrit, f., बुद्धि; Erkenntnisvermögen, Unterscheidungskraft)

--

Menschliche Evolution: Triebe -> Intellekt -> Intuition -> Inspiration (Genius,
Gottmensch, Atman)

--

### Klesha (Sanskrit: क्लेश kleśa m. "Leiden")

Übernommen von https://wiki.yoga-vidya.de/Klesha

Da gibt es zunächst mal Avidya, Unwissenheit, die erste der Kleshas. Als
zweites gibt es Asmita, Identifikation, wenn man sagt: „Ich bin dieses und
jenes.“ Asmita: „Ich bin das. Ich brauche das. Usw.“ Das ist Asmita,
Identifikation. Aus Asmita kommt die dritte Klesha, Raga. Weil man sich mit
etwas identifiziert, mag man etwas. Raga heißt Mögen und Wunsch. Und wenn du
etwas magst, logischerweise gibt es andere Sachen, die magst du nicht. Deshalb
gibt es Dvesha, Abneigung, Ablehnung. Und schließlich folgt dann Abhinivesha
und Abhinivesha heißt zunächst mal Furcht vor dem Tod, im weiteren Sinn ist es
jede Art von Angst und jede Art von Furcht. Das sind die fünf Leiden.

- Avidya (Sanskrit: अविद्या avidyā f.) Nichtwissen, Unwissenheit.
- Asmita (Sanskrit: अस्मिता asmitā f.) wörtl: 'Ich-bin-heit' (Skt. asmi 'ich
  bin'), Ichheit, Ichgefühl, Ichverhaftung, Identifikation mit dem Ich,
  Ego-Gefühl
- Raga (Sanskrit: राग rāga m.) das Färben, Färbung, rote Farbe, Farbe, Röte;
  Entzündung; Nasalierung; Reiz, Lieblichkeit; Leidenschaft, Liebe, hefiges
  Verlangen, Sympathie, Zuneigung, Liebe, Freude, Mögen; Melodie; Farbe;
  Gefühl, Emotion; Würze
- Dvesha (Sanskrit: द्वेषः dveṣa m.) Ablehnung, Abneigung, Bosheit, Zorn, Haß
- Abhinivesha (Sanskrit: अभिनिवेश abhiniveśa m.) Anhaften am Leben, Lebenswille,
  Lebensdrang, Hang zu, Selbsterhaltungstrieb, (daher auch: Todesfurcht),
  Furcht vor dem Vergehen

--

OM der Bogen, der Geist ist der Pfeil und Brahman, das Absolute, ist das Ziel. Spanne den Bogen, indem du das OM konzentriert wiederholst um Brahman zu erreichen. Dann lasse los. Durch die Kraft des Bogens OM wird dein Denken und Fühlen eins mit Brahman

Dhyanabindu Upanishad

--

Ich bin, das sollst du wissen, [...], der ewige Same aller Wesen, ich bin der Verstand der Verständigen, bin die Kraft der Kraftvollen.

Bhagavadgita wird zu den Shrutis (Offenbarungsschriften) gezählt (6. Buch), ist wichtigster philosophischer Text des indischen Lehrgedichts Mahābhārata und entstand vermutlich zwischen dem 5. und dem 2. Jhdt. v. Chr.

Quelle: Deussen/Strauß (Übers.), Vier philosophische Texte des Mahâbhâratam. Sanatsujâta-Parvan, Bhagavadgîtâ, Mokshadharma, Anugîtâ, 1906. VII, 10

--

Höher als die Übung steht das Erkennen, höher als das Erkennen die Meditation, höher als die Meditation die Entsagung in betreff des Lohnes der Werke, der Entsagung folgt der Friede auf dem Fuße.

Bhagavadgita wird zu den Shrutis (Offenbarungsschriften) gezählt (6. Buch), ist wichtigster philosophischer Text des indischen Lehrgedichts Mahābhārata und entstand vermutlich zwischen dem 5. und dem 2. Jhdt. v. Chr.

Quelle: Deussen/Strauß (Übers.), Vier philosophische Texte des Mahâbhâratam. Sanatsujâta-Parvan, Bhagavadgîtâ, Mokshadharma, Anugîtâ, 1906. XII, 12

--

In der Musik bin ich die Melodie.

Bhagavadgita wird zu den Shrutis (Offenbarungsschriften) gezählt (6. Buch), ist wichtigster philosophischer Text des indischen Lehrgedichts Mahābhārata und entstand vermutlich zwischen dem 5. und dem 2. Jhdt. v. Chr.

--

Yoga ist das zur Ruhe bringen der Bewegungen im Geist – Patanjali, Verfasser der Yogasutra

--

So wie der Grund eines Sees deutlich sichtbar wird, wenn die Wellen an der Oberfläche sich legen, so kann das wahre Selbst wahrgenommen werden, wenn sich die Erscheinungsformen des Geistes legen – Swami Sivananda Saraswati
