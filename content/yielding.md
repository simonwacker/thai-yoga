[Contact Improvisation](https://myriadicity.net/contact-improv)

[The Importance of Yielding](https://integratedbody.com/uncategorized/the-importance-of-yielding/)

Warm-Up und Jam "From Yielding into Contact Improvisation" am Sonntag, 3. November, 20 bis 22 Uhr in "The MOVE", Habsburgerstr. 9, 79104 Freiburg, ein Bewegungsraum auf dem Gelände der FABRIK. Ich möchte mit dir "Yielding" erforschen, ein bewusster Kontakt mit Mutter Erde, eine aktive Hingabe deines Gewichts an deinen Untergrund, eine feinspürige Kommunikation durch physischen Kontakt, ein aufmerksames In-Beziehung-Treten mit dem was dich trägt. "Yielding" kommt aus dem Body-Mind Centering (BMC), ein somatischer Zugang zu Bewegung, Anatomie und Bewusstsein der von Bonnie Bainbridge Cohen entwickelt wurde, dessen Perspektiven das Tanzen von CI beflügeln können. Wie erlebe ich mich, wenn ich mein Gewicht bewusst in die Erde gieße, wenn mir ein Mensch ein mich bergendes Nest bildet, wenn ich mich an einen Menschen anschmiege und wenn sich diese lebendige Landschaft mit mir bewegt? Ich freue mich über alle, in denen dieses Thema resoniert, mit oder ohne Erfahrung in CI. Der Raumbeitrag liegt zwischen 5 und 10 Euro. Das Warm-Up dauert etwa 37 und die Jam etwa 73 Minuten. Schreibe mir bei Interesse gerne vorher oder komme spontan vorbei. Liebe Grüße, Simon

---

Die Beziehung zwischen Contact Improvisation (CI) und Body-Mind Centering (BMC) ist vielschichtig und bietet eine interessante Grundlage für die Erforschung von Bewegung und Wahrnehmung.

- Körperbewusstsein: BMC legt großen Wert auf das Bewusstsein für den Körper, seine Strukturen und Bewegungsmuster. In der Contact Improvisation ist das Körperbewusstsein entscheidend, um mit einem Partner zu interagieren und die Balance zu finden.
- Wahrnehmung und Präsenz: Beide Praktiken fördern die Entwicklung von Präsenz und Sensibilität. In CI ist die Fähigkeit, auf den Partner und die Umgebung zu reagieren, essenziell. BMC unterstützt diese Fähigkeit durch Achtsamkeit und somatische Übungen.
- Bewegungsqualität: BMC erforscht verschiedene Bewegungsqualitäten und -dynamiken, die auch in CI Anwendung finden. Das Verständnis von Schwingungen, Schwere und Leichtigkeit kann das Spiel und die Interaktion in CI bereichern.
- Selbst- und Körperwahrnehmung: BMC fördert ein tieferes Verständnis für den eigenen Körper und seine Funktionen, was in CI zu einem intuitiveren und flüssigeren Spiel führt.
- Kreativität und Improvisation: Beide Disziplinen betonen die Bedeutung von Improvisation. CI ist eine Form der kreativen Bewegung, die stark von der Individualität und Intuition der Tänzer abhängt, während BMC Techniken bietet, um kreative Bewegungen aus verschiedenen Körperteilen zu erkunden.

Insgesamt können CI und BMC sich gegenseitig bereichern, indem sie verschiedene Perspektiven auf Körper, Bewegung und Interaktion bieten. Die Kombination beider Ansätze kann das Verständnis von Körperlichkeit und Bewegungsfreiheit vertiefen.
