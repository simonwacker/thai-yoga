---
title: Aufatmen
summary: Erfahre deine Atemräume
images:
  - images/aufatmen-x.jpg
date: 2023-02-12T08:17:28+0100
layout: breathe
---

_Hinweis_: **Der Kurs pausiert gerade. Melde dich gerne bei Interesse.**

Rāja- und Haṭha-Yoga inspirierter Forschungs*frei*raum für Atemarbeit und
Selbstgewahrsein.

- Jeden Dienstag von 20:00 bis 21:30 Uhr mit Ankommen ab 19:45 Uhr
- Im [Yogazentrum Freiburg](http://yogazentrum-freiburg.de), [Marie-Curie-Straße 3, 79100 Freiburg-Vauban](https://www.openstreetmap.org/directions?from=&to=47.97667%2C7.82602)
- Für 8 bis 16 €uro Energieausgleich nach Selbsteinschätzung
- Mit oder ohne Voranmeldung
- Mit Herz begleitet von [mir](/kontakt/)

Komme im Raum, in der Gruppe und bei dir an. Beobachte deinen Atem. Verankere
dich durch ihn im gegenwärtigen Moment. Lenke deine Sinne nach innen, luge,
lausche, schnuppere, schmecke und spüre in dich hinein. Tauche tief in dich
ein. Entdecke deine ureigene Lebendigkeit. Betrete den ewig gegenwärtigen Raum
inneren Gewahrseins. Und erkenne dich selbst.

Übernehme Kontrolle über deinen Atem. Spiele mit ihm, erforsche deine
Atemräume und erfahre die Kraft deines Atems. Lade dich mit Lebensenergie auf,
genieße Atemstille, entdecke subtile Ströme deiner Lebendigkeit. Verankere
diese nährenden Erfahrungen in dir und trage sie in deinen Alltag.

Und auf liebevolle, demütige, hingebungsvolle Weise praktizierend lernst du
dich immer besser kennen, wirst du deiner mehr und mehr selbst bewusst, räumst
du nach und nach innerlich auf, beginnst du deine Innenwelt auf allen Ebenen
feiner und feiner wahrzunehmen und ermöglichst dir dadurch authentischer und
wahrhaftiger zu leben.

*ein*atmen, *aus*atmen, *auf*atmen --- *ein*tauchen, *durch*tauchen,
*auf*tauchen --- *auf*decken, *zu*decken, *ent*decken --- *be*kennen,
*aus*kennen, *er*kennen
