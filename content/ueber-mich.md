---
title: Simon (er/ihm)
images:
  - images/portrait/portrait.jpg
summary: Ruhiger Herzensmensch
description: >
  In einem munteren Kreis berührungsfreudiger Menschen gab und empfing ich
  meine ersten Thai-Yoga-Massagen. Ich tauchte ein in eine mir neue Welt voll
  körperlichem, emotionalem, mentalem und spirituellem Wohlempfinden — ich
  berührte voll Herzensfreude und empfing voll Hingabe. Seit diesen ersten
  Berührungen auf allen Ebenen beseelt mich Thai-Yoga und ich verspüre den
  Herzenswunsch, das Geschenk, das ich empfangen durfte, vielfältig
  weiterzugeben und erfahrbar zu machen.
short: |
  In einem munteren Kreis berührungsfreudiger Menschen gab und empfing ich
  meine ersten Thai-Yoga-Massagen. Ich tauchte ein in eine mir neue Welt voll
  körperlichem, emotionalem, mentalem und spirituellem Wohlempfinden --- ich
  berührte voll Herzensfreude und empfing voll Hingabe. Seit diesen ersten
  Berührungen auf allen Ebenen beseelt mich Thai-Yoga und ich verspüre den
  Herzenswunsch, das Geschenk, das ich empfangen durfte, vielfältig
  weiterzugeben und erfahrbar zu machen.
vision: |
  Mein Herzenswunsch ist es heilsame Räume für Menschen wie dich zu schaffen, in
  denen du vertrauensvoll alle Kontrolle abgeben darfst, dich wahrlich hingeben
  darfst, dich der Intelligenz deiner unbewussten Anteile anvertrauen darfst und
  einfach sein darfst. Ich träume von einer Weltengemeinschaft, in der Berührung
  liebevoll, einfühlsam, mitfreuend und absichtslos einander geschenkt wird, als
  alltägliche Interaktion. Für diese Vision schenke und tausche ich Massagen,
  besuche Fortbildungen, forsche darüber mit Freund:innen und gebe selbst
  Fortbildungen in verschieden großen Kreisen für interessierte Personen.
quote: |
  > Du siehst die Welt nicht so wie sie ist, Du siehst die Welt so wie Du bist.

  --- Aus dem Englischen nach [Mooji](https://mooji.org) (1954-heute)
training:
  title: Ausbildungen
  anchor: ausbildungen
  summary: Thai-Yoga-Massage, Shiatsu und Holistic Bodywork
  description: |
    Meine Grundausbildung in Thai-Yoga-Massage bekam ich von [Julia
    Deka](https://yolaya.de), osteopathische Techniken lernte ich von [David
    Lutt](http://lulyani.com/en/), cranio-sacrale Anwendungen von [Vangelis
    Varis](https://www.facebook.com/vagelis.varis) und [Zoltan
    Gyorgyovics](https://www.zoltangyorgyovics.com), Bauchmassagetechniken des
    Chi Nei Tsang von [Krishnataki](https://www.thaimassage.gr) und Zoltan
    Gyorgyovics, den Einsatz von Armen und Beinen von [Till
    Heeg](https://www.tillthai.com), intuitive Berührung von [Siawasch
    Peyman](http://www.siapeyman.at) und ich vertiefte meine Fertigkeiten unter
    Anleitung von [Bojan Babić](https://thaiinflow.com).

    Meine Grundausbildung in Shiatsu, japanische Heilmassage, bekam ich von
    [Birgit
    Mary](http://shiatsuzentrum-freiburg.de/praxisgemeinschaft/birgit-mary/) am
    [kiCollege](https://www.kicollege.de), Meridianarbeit lernte ich von Birgit
    Mary, Gelenkarbeit von [Michaela Knorr](https://www.kitao-freiburg.de) und
    Sei-ki, intuitive Berührung aus der Leere, von [René
    Fix](https://renefix.de), dem Schulleiter des kiCollege, und [Alice
    Whieldon](https://alicewhieldon.com).

    Seit Oktober 2022 befinde ich mich in Ausbildung zum [Holistic
    Bodyworker](https://holistic-bodywork.org), eine ganzheitliche
    Körpertherapie, in der Körperarbeit, Coaching, Traumatherapie und
    Spiritualität verwoben sind, geleitet von [Pascal
    Beaumart](https://holistic-bodywork.org/pascal-beaumart/), begleitet von
    [Raphaela Spohn](https://move-bemoved.com) und [Oliver
    Kohs](https://holistic-bodywork.org/ueber-oliver-kohs/) und assistiert von
    etlichen [Teammitgliedern](https://holistic-bodywork.org/ueber-uns/).

    Für meine Lehrer:innen und deren Breitschaft ihr Wissen zu teilen empfinde
    ich tiefe Dankbarkeit.
  quote: |
    > Eine Reise von tausend Meilen beginnt mit dem ersten Schritt.

    --- Aus dem Chinesischen nach [Laozi](https://de.wikipedia.org/wiki/Laozi) (vermutlich 6. Jahrhundert vor Christus)
  timelines:
    thai_yoga:
      title: Thai-Yoga
      anchor: thai-yoga-aus-und-fortbildungen
      entries:
        - from: 2020-10-02
          to: 2020-10-06
          field: Thai-Yoga-Massage
          what: Grundausbildung (Modul 1)
          who: "[Julia Deka](https://yolaya.de)"
        - from: 2020-12-11
          to: 2020-12-15
          field: OsteoThai
          what: Main Joints
          who: "[David Lutt](http://lulyani.com/en/)"
        - from: 2021-05-12
          to: 2021-05-16
          field: Thai-Yoga-Massage
          what: Grundausbildung (Modul 2)
          who: "[Julia Deka](https://yolaya.de)"
        - from: 2021-09-15
          to: 2021-09-19
          field: Thai-Yoga-Massage
          what: Craniosacral Applications
          who: "[Vangelis Varis](https://www.facebook.com/vagelis.varis)"
        - from: 2021-10-28
          to: 2021-11-01
          field: Thai-Yoga-Massage
          what: Grundausbildung (Modul 1)
          who: "[Julia Deka](https://yolaya.de)"
        - from: 2021-12-08
          to: 2021-12-12
          field: OsteoThai
          what: Breathing
          who: "[David Lutt](http://lulyani.com/en/)"
        - from: 2022-03-26
          to: 2022-03-27
          field: Thai-Yoga-Massage
          what: Long, Slow, and Deep
          who: "[Till Heeg](https://www.tillthai.com)"
        - from: 2022-04-01
          to: 2022-04-03
          field: Thai-Yoga-Massage
          what: Treating the Hara
          who: "[Krishnataki](https://www.thaimassage.gr)"
        - from: 2022-05-07
          to: 2022-05-08
          field: Thai-Yoga-Massage
          what: Internal Organs
          who: "[Zoltan Gyorgyovics](https://www.zoltangyorgyovics.com)"
        - from: 2022-06-11
          to: 2022-06-14
          field: Thai-Yoga-Massage
          what: The Morjim Sequences
          who: "[Siawasch Peyman](http://www.siapeyman.at)"
        - from: 2022-06-14
          to: 2022-06-19
          field: Thai-Yoga-Massage
          what: Body Poetry Principles
          who: |
            [Till Heeg](https://www.tillthai.com),
            [Bojan Babić](https://thaiinflow.com) und
            [Vangelis Varis](https://www.facebook.com/vagelis.varis)
        - from: 2022-11-30
          to: 2022-12-04
          field: OsteoThai
          what: Internal Organs
          who: "[David Lutt](http://lulyani.com/en/)"
        - from: 2023-03-29
          to: 2023-04-02
          field: Thai-Yoga-Massage
          what: Grundausbildung (Modul 2)
          who: "[Julia Deka](https://yolaya.de)"
        - from: 2023-05-11
          to: 2023-05-14
          field: Thai-Yoga-Massage
          what: With The Feet
          who: "[Krishnataki](https://www.thaimassage.gr)"
        - from: 2023-09-28
          to: 2023-10-02
          field: Thai-Yoga-Massage
          what: Dynamic
          who: "[Bojan Babić](https://thaiinflow.com)"
        - from: 2023-12-07
          to: 2023-12-11
          field: OsteoThai
          what: Lower Back
          who: "[David Lutt](http://lulyani.com/en/)"
        - from: 2024-05-16
          to: 2024-05-20
          field: Thai-Yoga-Massage
          what: All About The Side Position
          who: "[Krishnataki](https://www.thaimassage.gr)"
        - from: 2024-12-12
          to: 2024-12-16
          field: OsteoThai
          what: Internal Organs II
          who: "[David Lutt](http://lulyani.com/en/)"
    shiatsu:
      title: Shiatsu
      anchor: shiatsu-fortbildungen
      entries:
        - from: 2020-10-17
          to: 2020-10-18
          what: Gelenke und Rotationen
          who: "[Michaela Knorr](http://kitao-freiburg.de/michaela/)"
        - from: 2020-11-21
          to: 2020-11-22
          what: Einführungskurs
          who: "[Birgit Mary](https://kitao-freiburg.de/birgit/)"
        - from: 2021-03-13
          to: 2021-03-14
          what: Meridianarbeit Niere/Blase
          who: "[Birgit Mary](https://kitao-freiburg.de/birgit/)"
        - from: 2021-07-10
          to: 2021-07-11
          what: Kyo/Jitsu
          who: "[Birgit Mary](https://kitao-freiburg.de/birgit/)"
        - from: 2021-09-09
          to: 2021-09-12
          what: Sei-ki
          who: "[René Fix](https://renefix.de) and [Alice Whieldon](https://alicewhieldon.com)"
    tantra:
      title: Tantra
      anchor: tantra-seminare
      entries:
        - from: 2021-11-19
          to: 2021-11-21
          what: Junges Tantra
          who: |
            [Michael König](https://amakido.de),
            [Rubén](https://amakido.de/wir/team-massage/ruben/) und
            [Sheala](https://amakido.de/wir/team-seminar/sheala/)
        - from: 2022-01-08
          to: 2022-01-09
          what: Tantramassage Einführungskurs
          who: "[Petra Hällfritzsch](https://www.opensenses.de)"
        - from: 2022-03-17
          to: 2022-03-20
          what: Tantra in Motion
          who: |
            [Michael König](https://amakido.de) und
            [Matís d’Arc](https://www.luhmendarc.com)
    holistic_bodywork:
      title: Holistic Bodywork
      anchor: holistic-bodywork-ausbildung
      entries:
        - from: 2022-11-02
          to: 2022-11-07
          what: Sen Sumana
          who: |
            [Pascal Beaumart](https://holistic-bodywork.org/pascal-beaumart/),
            [Raphaela Spoon](https://move-bemoved.com) und
            [Oliver Kohs](https://holistic-bodywork.org/ueber-oliver-kohs/)
        - from: 2022-11-26
          to: 2022-11-26
          field: Somatisches Coaching
          what: Verkörpertes Denken
          who: "[Lucas Forstmeyer](https://lucasforstmeyer.com)"
        - from: 2023-01-21
          to: 2023-01-21
          field: Somatisches Coaching
          what: Intentionen
          who: "[Lucas Forstmeyer](https://lucasforstmeyer.com)"
        - from: 2023-02-22
          to: 2023-02-27
          what: Sen Itha & Pingkhala
          who: |
            [Pascal Beaumart](https://holistic-bodywork.org/pascal-beaumart/),
            [Raphaela Spoon](https://move-bemoved.com) und
            [Oliver Kohs](https://holistic-bodywork.org/ueber-oliver-kohs/)
        - from: 2023-03-04
          to: 2023-03-04
          field: Somatisches Coaching
          what: Regulation & Experimente
          who: "[Lucas Forstmeyer](https://lucasforstmeyer.com)"
        - from: 2023-05-06
          to: 2023-05-06
          field: Somatisches Coaching
          what: Beziehungen
          who: "[Lucas Forstmeyer](https://lucasforstmeyer.com)"
        - from: 2023-06-03
          to: 2023-06-03
          field: Somatisches Coaching
          what: Metaphern
          who: "[Lucas Forstmeyer](https://lucasforstmeyer.com)"
        - from: 2023-06-24
          to: 2023-06-24
          field: Somatisches Coaching
          what: Haltungsziele
          who: "[Lucas Forstmeyer](https://lucasforstmeyer.com)"
        - from: 2023-06-28
          to: 2023-07-03
          what: Autonomie und Verbindung
          who: |
            [Pascal Beaumart](https://holistic-bodywork.org/pascal-beaumart/),
            [Raphaela Spoon](https://move-bemoved.com) und
            [Oliver Kohs](https://holistic-bodywork.org/ueber-oliver-kohs/)
        - from: 2023-09-09
          to: 2023-09-09
          field: Somatisches Coaching
          what: Implementierung
          who: "[Lucas Forstmeyer](https://lucasforstmeyer.com)"
        - from: 2023-10-07
          to: 2023-10-07
          field: Somatisches Coaching
          what: Innere Anteile
          who: "[Lucas Forstmeyer](https://lucasforstmeyer.com)"
        - from: 2023-10-25
          to: 2023-10-30
          field: Holistic Bodywork
          what: Vertrauen
          who: |
            [Pascal Beaumart](https://holistic-bodywork.org/pascal-beaumart/),
            [Raphaela Spoon](https://move-bemoved.com) und
            [Oliver Kohs](https://holistic-bodywork.org/ueber-oliver-kohs/)
        - from: 2023-11-04
          to: 2023-11-04
          field: Somatisches Coaching
          what: Somatische Übungen
          who: "[Lucas Forstmeyer](https://lucasforstmeyer.com)"
        - from: 2024-01-25
          to: 2024-01-30
          what: Liebe & Sexualität
          who: |
            [Pascal Beaumart](https://holistic-bodywork.org/pascal-beaumart/),
            [Raphaela Spoon](https://move-bemoved.com) und
            [Oliver Kohs](https://holistic-bodywork.org/ueber-oliver-kohs/)
    yoga:
      title: Yoga
      subtitle: |
        Zweijährige Yogalehrer:innenausbildung geleitet von
        [Jürgen Ries](http://yogazentrum-freiburg.de) und unterstützt von
        [Dr. med. Anina Penelope Keil](https://www.aninayoga-silentpowers.com/)
      anchor: yoga-ausbildung
      entries:
        - from: 2020-10-10
          to: 2021-10-11
          what: Yogasutra und Bhagavad Gita
        - from: 2020-12-19
          to: 2021-12-20
          what: Entwicklungswege und Strömungen
        - from: 2021-02-06
          to: 2021-02-07
          what: Reinigungsaspekte und -techniken
        - from: 2021-03-28
          to: 2021-03-28
          what: Upanishaden
        - from: 2021-04-25
          to: 2021-04-25
          what: Hatha Yoga Pradipika
        - from: 2021-05-08
          to: 2021-05-09
          what: Asanas
        - from: 2021-07-16
          to: 2021-07-18
          what: Pranayama
        - from: 2021-09-12
          to: 2021-09-12
          what: Übungstag
        - from: 2021-10-02
          to: 2021-10-03
          what: Kontemplation und Meditation
        - from: 2021-11-13
          to: 2021-11-14
          what: Yogatherapie
        - from: 2022-01-15
          to: 2022-01-16
          what: Pädagogik und Didaktik
        - from: 2022-03-05
          to: 2022-03-05
          what: Unterrichtgestaltung und Yoga als Profession
        - from: 2022-04-08
          to: 2022-04-10
          what: Anatomie
        - from: 2022-05-14
          to: 2022-05-15
          what: Hormonsystem
        - from: 2022-07-01
          to: 2022-07-03
          what: Energetik
        - from: 2022-07-24
          to: 2022-07-24
          what: Unterrichten Üben
        - from: 2022-09-24
          to: 2022-09-25
          what: Nadis und Nervensystem
        - from: 2022-10-16
          to: 2022-10-16
          what: Wiederholung
        - from: 2022-11-20
          to: 2022-11-20
          what: Freier Austausch
        - from: 2022-12-10
          to: 2022-12-11
          what: Chakren
        - from: 2023-04-15
          to: 2023-04-16
          what: Ayurveda
        - from: 2023-07-22
          to: 2023-07-22
          what: Abschluss
    ayurveda:
      title: Ayurveda Essentials
      subtitle: |
        Sechstägige Fortbildung bei
        [Ralf Schultz](https://somayoga-freiburg.de),
        [Sundari Ma](http://sundarima.de) und
        [Daisy Bowman](https://daisybowman.com)
      anchor: ayurveda-essentials-fortbildung
      entries:
        - from: 2021-05-29
          to: 2021-05-30
        - from: 2021-06-26
          to: 2021-06-27
        - from: 2021-07-24
          to: 2021-07-25
    others:
      title: Persönlichkeitsentwicklung
      subtitle: Selbsterfahrungsseminare zur perönlichen Reifung
      anchor: persoenlichkeitsentwicklung-seminare
      entries:
        - from: 2021-06-11
          to: 2021-06-13
          what: Holotropes Atmen
          who: |
            [Kati Wortelkamp](https://holotropatmen.de) und
            Ursula Boger
        - from: 2021-08-28
          to: 2021-09-02
          what: Held:inreise
          who: |
            [Raphael Kassner](https://www.raphaelkassner.de) und
            [Johanna Bascle](https://www.joba-ganzsein.de)
        - from: 2022-07-30
          to: 2022-07-30
          what: Einklang mit Stimme und Sein
          who: |
            [Agnes Knoop](https://www.gestaltzeit.de/team.html) und
            Vera la Fe
        - from: 2022-08-26
          to: 2022-08-28
          what: Rage Club
          who: |
            [Markus Bork](https://www.markusbork.org),
            [Eva Daubert](https://www.evadaubert.de) und
            [Oliver Arnold](https://www.oliverarnold.net)
        - from: 2022-09-08
          to: 2022-09-11
          what: GestaltZeit im Wald
          who: "[Patricia Kleber, Edgar Reinhold und Majke Kunze](https://www.gestaltzeit.de)"
        - from: 2023-08-09
          to: 2023-09-20
          what: "Adula Klinik — Fachklinik für Psychosomatik und Psychotherapie"
          who: "[Therapeutischen Gemeinschaft](https://www.dr-reisach-kliniken.de/adula-klinik.html)"
        - from: 2023-11-27
          to: 2023-12-02
          what: Family Circles
          who: |
            [Jan Roland Schöfmann](https://www.fuer-meinen-weg.de) und
            [Birke Knopp](https://www.den-wandel-begleiten.de)
        - from: 2024-02-14
          to: 2024-04-03
          what: "Heiligenfeld Klinik — Fachkrankenhaus für Psychosomatische Medizin und Psychotherapie"
          who: "[Therapeutischen Gemeinschaft](https://www.heiligenfeld.de/kliniken/fachklinik-heiligenfeld)"
  upcoming:
sacred_dance:
  title: Heiliger Tanz
  anchor: heiliger-tanz
  summary: Für mich ist Thai-Yoga-Massage ein heiliger Tanz
  description:
    Gebende und empfangende Person Tanzen miteinander einen heiligen Tanz,
    verschmelzen durch Berührung, gehen in Resonanz, behandeln einander, beflügeln
    sich gegenseitig, fließen miteinander durch Raum und Zeit, durch Gefühls-,
    Gedanken- und Körperwelten. Die Grenzen lösen sich auf und die grundlegende
    Einheit aller Lebewesen gibt sich zu erkennen.
  quote: |
    > Wir haben nur diesen gegenwärtigen Moment, nur diesen einzigartigen und
    > ewigen Augenblick, der sich vor unseren Augen öffnet und entfaltet, Tag und
    > Nacht.

    --- Aus dem Englischen nach [Jack Kornfield](https://jackkornfield.com) (1945-heute)
  boxes:
    - title: Geben
      anchor: geben
      text: |
        Für mich ist Geben Meditation in Bewegung, tiefe Einkehr, Innenschau,
        Selbstentfaltung, Verschmelzen mit den Empfindungen in meinen Händen, Vertrauen
        darin, dass meine Hände wissen wo sie wann wie gebraucht werden. Und durch
        diese achtsame, wertfreie, sehende, akzeptierende, annehmende Berührung zweier
        Lebewesen, zweier Herzen, zweier Bewusstseins, entsteht ein Raum in dem
        Heilungsprozesse auf allen Ebenen angestoßen und fortgesetzt werden können,
        wird die empfangende Person auf allen Ebenen berührt.
    - title: Empfangen
      anchor: empfangen
      text: |
        Für mich ist Empfangen Meditation in Ruhe, Hingabe, Selbsterkenntnis,
        Wiedereinzug in den Körper, Wiederentdeckung und Begeisterung des Körpers,
        Loslassen, Tiefenentspannung. Und in einem meditativen Zustand tiefer
        Entspannung löst jede Berührung eine Emotion aus, weckt Erinnerungen, verbindet
        Körperstellen mit Gefühls- und Gedankenwelten, schafft dadurch intuitives
        Verständnis und lädt zu Heilung auf allen Ebenen ein.
date: 2022-02-10T13:05:52+0100
layout: about-me
---

> Achte auf deine Gedanken, denn sie werden Worte, achte auf deine Worte, denn
> sie werden Handlungen, achte auf deine Handlungen, denn sie werden
> Gewohnheiten, achte auf deine Gewohnheiten, denn sie werden dein Charakter,
> achte auf deinen Charakter, denn er wird dein Schicksal

--- Aus dem Englischen nach Charles Reade (1814–1884)
