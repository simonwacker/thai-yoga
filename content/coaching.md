---
title: Somatisches Coaching
summary: Zeit zum Denken
date: 2023-03-16T09:05:52+0100
layout: single
table_of_contents: true
---

Meine Ausrichtung ist es heilsame Räume zu halten, in denen Menschen mit sich
in Verbindung treten und Wunschzustände in sich lebendig werden lassen.

Wenn ich Psychoedukation anbiete, rede ich dabei beispielhaft von mir (ich),
von dir (du), von mensch (er:sie) oder von es gibt Menschen (sie). Oder sage
ich "gemäß ... gilt ..." oder "... behauptet es gelte ...".

Thai-Yoga-Massage: Berührungskultur/-art/-kunst.

Somatisches Coaching: Verbal in den Körper begleiten und inneres Erleben bei
äußeren Erfahrungen lebendig werden lassen und qualitativ anreichern und
dadurch zugänglicher, reicher und voller machen. Ohne äußeres Ereignis inneren
Seinszustand kultivieren/hervorrufen.

Traumasensitiviät/-informiertheit: Eingestimmt auf das Nervensystem
interagieren, behutsam und feinfühlig für Zustandswechsel, langsam,
tröpfchenweise und pendelnd zwischen Ressourcen und Herausforderungen um nicht
zu überfordern, Ressourcen und Herausforderungen heilsam zu verweben,
Selbstkontrolle und -gewahrsein zu wahren und die eigenen Stärken und
Selbstwirksamkeit bewusst wahrzunehmen. --- Sicherheit und neue
Referenzerfahrungen.

- So langsam gehen wie der langsamste Anteil.
- Nicht auf-/zerteilen sondern zusammensetzen und ganz werden/sein. In Sprache
    statt "(entweder) oder" besser "und" verwenden und statt "aber" besser
    "und" verwenden. Oft ist vielerlei gleichzeitig wahr/da, auch scheinbar
    widersprüchliches.
- Heilen durch Sein in der Gegenwart ohne Vergangenheit zu verdrängen. Immer
    mehr in Gegenwart verweilen.
- Indem ich durch offene Fragen und deren Antworten mehr über das Thema
    erfahre, entsteht daraus von selbst, was am Hilfreichsten für das System
    ist. --- statt zu glauben ich als Begleiter:in wüsste was es braucht
    (Retterrolle).
- Es kann erst einen langen Monolog des:der Protagonist:in benötigen bevor
    Bereitschaft dafür da ist in seinen:ihren Körper zu gehen. Monologisieren
    und dabei gehört zu werden kann für Menschen wichtig sein. Als
    zuhörende/bezeugende Person halte ich den Raum ohne mich zu involvieren.
    Ich kann den Monolog nutzen um eine Landkarte des:der Protagonist:in zu
    bauen und um herauszufinden wie deren System funktioniert. Beim Zuhören
    achte ich auch mich selbst. Was geht für mein System gut und was nicht? Bin
    ich danach beispielsweise zu voll oder geladen? Kann ich Abstand halten
    / mich abgrenzen und gleichzeitig verbunden sein? Was ist ein Rahmen, der
    für mich funktioniert? Dazu benötigt es Übung und Erfahrung. Auch mir als
    begleitende Person muss es gut gehen damit ich gut begleiten kann.
- Wenn ich mich irre, dann lerne ich und der:die Protagonist:in daraus.
- Ich greife Bewegungsimpulse auf und vervollständige sie. Dadurch erreiche ich
    den:die Protagonist:in tiefer als wenn ich Bewegungsimpulse von außen
    vorgebe. Begleite ich mich selbst, so greife ich meine eigenen inneren
    Impulse auf. Ich variiere die Geschwindigkeit, das Ausmaß und die
    Intensität von Bewegungen.
- Statt zu versuchen etwas zu wissen oder zu verändern, gebe ich als Frage
    rein "Was ist der kleinstmögliche nächste Schritt?". Ich versuche
    beispielsweise nicht einer:m Protagonist:in ihren Schmerz zu verringern
    indem ich von außen etwas vermute sondern frage sie nach ihrem
    Erfahrungswissen.
- "Und wenn all das, was möchtest du, dass geschieht?"
- Rückfragen/Validieren: Nach einer Übung frage ich nach wie sich das Erleben
    jetzt ist.
- Ich finde verbindende Übergänge zwischen Massage, Coaching, Experimenten und
    Regulation. Ich verwebe/verknüpfe/verbinde alles zu einem stimmigen Ganzen.

## Verkörpertes Denken

ich oder du: Therapeut:in, Begleiter:in
er:sie: Protagonist:in

Somatisches Coaching/Prozessbegleitung ist Zeit zum ganzhirnigen verkörperten
Denken um eine umfassende und neue Perspektive auf Herausforderungen,
Ressourcen, Wünsche und Anteilen einzunehmen, die davon ausgeht wie es ist und
wie ich bin statt wie ich es gerne hätte und wie ich gerne wäre. Diese Art zu
denken bedeutet, dass direkt mit Körperempfindungen als Kommunikationsmittel
der Körperintelligenz oder der rechten Gehirnhälfte gearbeitet wird statt nur
abstrakt zu analysieren. Dabei sind beide Hälften synchronisiert. Der Körper
spricht beispielsweise in Form des Bauchgefühls auf kryptische Weise stets zu
jedem Menschen und seine Aussagen stehen bei mangelhafter Synchronität im
Widerspruch zu Denkkonzepten oder Herzgefühlen. Synchronisierung bedeutet, dass
die Landkarte stets an die sich wandelnde Landschaft angepasst wird (Anpassen
der inneren Annahmen / virtuellen Realität an die äußeren Begebenheiten
/ tatsächliche Realität) und umgekehrt macht die Landkarte Vorhersagen über
noch dunkle Flecken auf der Landschaft (Voraussagen über Zukunft oder
Unbekanntes).

Der explizite Auftrag beim somatischen Coaching ist die Arbeit an einem Thema
durch Erfoschen und der implizite die Entwicklung der Körperwahrnehmung durch
Üben.

Wie hilft Körpertherapie bei Herausforderungen und Wünschen im Leben?
Protagonist:in kommt mit Herausforderungen, die er/sie bewältigen möchte, oder
mit Wünschen, die er/sie sich erfüllen möchte. Die bisherigen Strategien, die
auf Basis der inneren Landkarte versucht wurden, waren nicht zielführend
(führten nicht zu einer Lösung). Durch Wechsel von der Landkarte auf die
Landschaft, das heißt durch Einbeziehung der Körperintelligenz,

- verändert sich die Herausforderung oder der Wunsch, wodurch sie sich entweder
  von selbst auflösen oder sich neue Lösungsstrategien offenbaren;
- verändert sich der Standpunkt des/der Protagonist:in, von dem aus die
  Herausforderung oder der Wunsch nicht mehr da oder wichtig ist oder wodurch
  sich neue Lösungsstrategien offenbaren.

Impulse des Körpers sind direkter, umfassender, kreativer und neu und kommen in
der gegenwärtigen Kultur oft zu kurz. Protagonist:innen, die zu uns kommen,
haben ihre Herausforderung oder ihren Wunsch meist analytisch durchdacht und
dabei keine befriedigende Lösung gefunden. Möglicherweise waren Lösungsansätze
bisher unbefriedigend, da sie von einem Wunschwelt- und -selbstbild ausgingen
(von einer fehlerhaften und ungenauen Landkarte) statt von der wirklichen Welt
und dem wirklichen Selbst (der Landschaft).

In der Körperintelligenz erleben wir Ganzes. Wenn sich ein Teil integriert,
wenn er ganz wird, sich in Ganzes verwandelt, im Ganzen aufgeht, dann ist
Erkenntnis geschehen.

Wer sind der/die Meister:in und sein/ihre Gesandte:r? Rechte
Gehirnhälfte/Hemisphäre: Der/Die Meister:in (körperliche Intelligenz) ist
körperlich, implizit, diffus, ahnungsvoll, interessiert, umfassend, direkt,
nuanciert, integrativ, lebendig, verkörpert, vernetzt, staunend --- die
Landschaft. Linke Gehirnhälfte/Hemisphäre: Der/Die Gesandte (sprachliche
Intelligenz) ist verbal/sprachlich, explizit, klar, konzeptionell, abstrakt,
handlungsorientiert, polarisierend (in Polen denkend), dualisierend (in
Dualitäten denkend), schwarz-weiß --- die Landkarte.

Beiden Gehirnhälften sind bei allen (Gehirn)Aktivitäten beteiligt, lediglich
deren Ausrichtung/Vorgehensweisen ist/sind verschieden.

Die rechte Hälfte tastet/sucht/(über)fliegt die ganze Umgebung ab und verliert
das vordergründige Ziel nicht aus den Augen. Sie ist lebendig, interessiert
sich für Phänomene, kommuniziert in Metaphern, begeistert sich für die
Einzigartigkeit von allem, betrachtet alles organisch und als sich
(weiter/fort)entwickelnd. Sie eröffnet (ist das Tor zu) neue Sichtweisen und
neues Lebensgefühl. In ihr steht alles in Beziehung, in Wechselwirkung, im
Austausch, ist alles gleichzeitig und gleichortig da (Zeit und Raum sind bloße
Konzepte), ist alles eins --- Einheit.

Die linke Hälfte ist handlungsbezogen, zielgerichtet und fokussiert. Sie ist
leblos, vereinfacht alles, konzeptionalisiert alles, kategorisiert alles,
instrumentalisiert alles, sieht alles als Mittel zum Zweck und ermöglicht uns
dadurch mechanisch die Welt zu navigieren. In ihr ist jedes Individuum, jede
Herausforderung und jeder Wunsch durch Aufteilen in deren Bestandteile besser
greifbar und das Ganze ist nicht mehr als die Teile --- Dualität.

Aha-Momente integrieren körperliche in sprachlich Intelligenz (Erkenntnisse in
Landschaft in Landkarte) und schaffen dadurch ein neues Verständnis der
Wirklichkeit.

An den Grenzen des konzeptionellen Verständnisses ist Unklarheit und
Unsicherheit (im Possibility Management ist dies der Rand der Box).
Einbeziehung der Körperintelligenz, der Landschaft, die nicht wie die Landkarte
an den Grenzen des Bekannten endet, kann Klarheit und Sicherheit
schaffen/geben. Manche Menschen bewegen sich stets im sicheren und verstandenen
Raum. Dies mag wohlig sein, ist jedoch einschränkend und sobald
unvorhergesehene Ereignisse jene aus diesem Raum hinauskatapultieren sind sie
um so überforderter/verlorener. Die eigenen Grenzen zu kennen und zu in
sicherem Rahmen zu erweitern macht flexibel, offen und frei (im Possibility
Management wird dies "Expand the Box" genannt).

Clean Language hat eine absichtlich hypnotische tranceartige Sprachstruktur und
ist metaphernlos und annahmelos um nicht zu manipulieren, insbesondere um
nichts einzupflanzen. Beispielsweise "Und ist da noch etwas, das gerade
geschieht, in oder um deinen Körper?". Sie lässt Protagonist:innen so wie sie
sind. Ermöglicht es deren Innenwelt zu erforschen ohne Ideen hineinzugeben und
bleibt dadurch in der Erfahrung des Gegenübers sauber. Es ist ein einfacher
Ansatz um mit komplizierten Menschen zu arbeiten und dabei meine
Interpretationen herauszuhalten. Der/Die Protagonist:in gibt den Takt, den Tanz
vor. Der/Die Begleiter:in geht nur mit.

Vermeide Teilsätze wie "Du hast gesagt/bemerkt/...", da dies den/die
Protagonist:in zwingt sich selbst von außen mit den gespiegelten Aussagen
auseinanderzusetzen und sie zu revalidieren/reevaluieren, was aus aus dem
direkten Erleben, der direkten Erfahrung, der Innenschau bringt/reißt. Aber bei
herausfordernden Erfahrungen kann dieser Abstand auch sinnvoll/hilfreich sein
und du kannst Teilsätze bewusst einsetzen um diesen Abstand zu schaffen.

Du kannst auch Worte wie "Ich", "überall" und "dann" entfalten.

Indem ich mich selbst beschränke mache ich es mir einfach. Ich entwickle innere
Landkarte/Modell von dem Erzählten (der Innenwelt, der inneren Landschaft,
des/der Protagonist:in) ohne ihn/sie im Detail verstehen zu wollen. Ich
beschränke mich auf das Erzählte. Das eigene innere Modell verwende ich als
Orientierung/Inspiration ohne es für seine/ihre Wahrheit zu halten. Es hilft
mir mich in der Innenwelt des/der Protagonist:in zu orientieren. Die Metapher
der inneren Landschaft enthält Höhe, Tiefe und Weite, Berge und Täler, Wandel
über die Zeit, Eigenleben und vieles mehr.

Beim Begleiten halte ich mich an die intrinsische Logik und die genauen Worte
des/der Protagonist:in. Die Begleitung macht durch Aufmerksamkeitslenkung in
ihm/ihr sein/ihr Thema lebendig auf ihre Weise und mit ihrer Weisheit. Er/Sie
ordnet sich selbst.

"Wie ist das gerade (für dich)?" Der Teil in Klammern ändert Frage fundamental.
Er schafft Abstand zur Erfahrung.

Beziehung zu den fünf Phasen des Holistic Bodyworks: 1- Ankommen, Einstieg -2-
Entfalten -3- Vertiefen -4- Veränderung reifen 5- Integrieren. (TODO
Einzelheiten!)

`e` Erfahrung, `q` Qualität, `i` Intention

1. Wahrnehmen und Anerkennen "Ahhh, `e` und `e'`" oder "Ahhh, `q` und `e`."
1. Aufmerksamkeit lenken "Und wenn `e`"
1. Entfalten
   - Basis: "Und was für eine Art von `e` ist das?" und "Und gibt es noch
     etwas über `e`?" und "Wo ist `e`?" (und "Hat `e` eine Form oder
     Farbe?").
   - Intention: "Und möchte `e` etwas?" und "Und was möchte `e`, das geschieht?"
   - Geben: "Und kann `e` `i`?"
   - Ermöglichen: "Und kannst du `e` `i` geben?"
   - Integrieren: "Und wenn `e` `i` möchte und `e'` `i'` möchte, was möchtest
     du?"

Fragen sind Einladungen (und keine Herausforderungen), offen, expansiv, in Raum
sprechend, betont wie Aussagen.

Durch Entfalten mit sauberen Fragen fertigst du in dir eine Landkarte der
Innenwelt des/der Protagonist:in an ohne Manipulation, insbesondere ohne eigene
Bewertungen und Intentionen.

Warum metaphernlos? Metaphern sind bildhafte Analogien und enthalten oft
Annahmen, Interpretationen, Wertungen über die Wirklichkeit und schränken die
Vorstellung ein, da in der Analogie beispielsweise nur bestimmte
Lösungsstrategien möglich/glaubhaft/natürlich sind. Beispielsweise enthält die
Analogie einer Weggabelung die Information es gäbe nur zwei (oder endlich
viele) Möglichkeiten. Die Analogie einer einer Waage die Information es gäbe
nur zwei Seiten, diese seien vergleichbar und eine Seite wiege mehr als oder
weniger als oder gleich viel wie eine andere Seite. Und die Metapher "steht auf
der Kippe" impliziert, dass es zwei einander ausschließende (und
widersprüchliche) Möglichkeiten gäbe.

Wie unterscheiden sich Coaching und Therapie? Beide haben verschiedene
Intentionen. Therapie erforscht in Tief. Coaching unterstützt im Alltag.
Therapie heilt Probleme. Coaching hat positive Ausrichtung (implementiert
Wunsch).

_Übung_ zur Unterstützung der Kommunikation beider Gehirnhälften: "Was bemerkst
du, das du magst?" Aus der Antwort eine Erfahrung auswählen und diese
entfalten.

- Welche Erfahrung wähle ich aus / greife ich auf? Jene, die bedeutsam scheint
  und die die meiste somatische oder emotionale Ladung trägt. Dies ist
  beispielsweise bemerkbar anhand somatischer Marker der Körpersprache oder
  somatischer Aussagen wie Metaphern. Das lernst du organisch durch Üben. Die
  Wahrnehmung von Ladung geschieht vor-/unterbewusst.
- Was bedeutet es eine Erfahrung zu entfalten? Es bedeutet diese mit Qualitäten
  anzureichern, indem jede neue Frage Bezug auf die ursprüngliche Erfahrung
  nimmt statt auf eine der Qualitäten.
- Mit welchem Laut erkenne ich an? "Ahhh" erkennt neutral an. "Aha" oder "Oh,
    wow" drückt zusätzlich Überraschung, Bewertung oder Analyse aus. Auf
    Prosodie beim Anerkennen achten.
- Wie navigiere ich Erfahrungen und Qualitäten? Durch Aneinanderreihung mit
  "und" verknüpft in umgekehrter Reihenfolge. Beispielsweise fragst du zur
  Erfahrung `e` "Und was für eine Art von `e` ist das?" und erhältst unter
  anderem die Qualität `q`. Diese spiegelst/anerkennst du mit "Ahhh, `q`."
  und navigierst zurück zu `e` mit "Und `e`." und kannst dann fragen "Und
  gibt es noch etwas über `e`?".
- Was tun wenn eine Erfahrung selbst sich wandelt/transformiert/weitet/umformt?
  Dann bei dem Neuen bleiben, mit der Veränderung weiter gehen und dieses
  entfalten. Veränderung heißt, dass sich etwas im System neu organisiert hat
  (neue Innenwelt) --- ein wichtiger Moment in einer Session. Gebe diesem
  hinreichend Raum zu sinken, sich zu integrieren, der Protagonist:in
  umfassend bewusst zu werden.
- Wenn ich eine Erfahrung hinreichend entfaltet habe, soll ich dann eine andere
  Erfahrung aus der ursprünglichen Antwort auswählen? Lieber abermals die
  Ursprungsfrage stellen und abermals aus der Antwort wie oben erläutert
  auswählen. Der Grund ist, dass einst dagewesene Erfahrungen nach dem
  Entfalten einer anderen Erfahrung eventuell nicht mehr da sind oder in
  anderer Form.
- Was bedeutet es wenn ich energetisch wahrnehme, dass sich die Protagonist:in
  aus ihrem Körper entfernt, dass sie abdriftet? In diesem Fall kann es sein,
  dass er/sie dissoziiert, das heißt sich von ihrem Erleben abspaltet und in
  den Nervenzustand Starre kommt, was sich durch weniger Lebendigkeit
  ausdrückt. Oder es kann sein, dass er/sie in einen anderen
  Bewusstseinszustand übergeht, in dem das Erleben anders ist, was sich durch
  mehr Lebendigkeit ausdrückt, und in welchem durch den inneren
  Perspektivwechsel neue Erkenntnisse offenbaren können.
- Wie viel Raum gebe ich?
- Wie gehe ich mit Leere im Sinne von mangelhafter Resonanz mit einem Hauch von
    Langeweile/Desinteresse um?

_Übung_: Dreimal die Frage "(Und ist da noch etwas), was du bemerkst, das du
magst?" stellen und stets danach aus der Antwort eine Erfahrung auswählen und
diese entfalten.

- Wenn ich bereits mindestens zwei Erfahrungen entfaltet habe und eine vorher
  entfaltete noch weiter entfalten möchte, wie navigiere ich dorthin? Indem
  du bei der Anerkennung der Antwort auf die zuletzt gestellte Frage die
  Qualität benennst, mit "und" verknüpft die zugehörige Erfahrung, mit "und"
  verknüpft die gewünschte vorherige Erfahrung und dann eine Frage zu der
  letztgenannten Erfahrung stellst. Du kannst dir das Entfalten wie das
  Wachsen eines Walds vorstellen bei dem mit jeder neuen Erfahrung ein Stamm
  wächst und mit jeder ihrer Qualitäten ein Ast an den Stamm und sofern du
  auch eine Qualität entfaltest ein Ast an den Ast. Wenn du dich irgendwo an
  einem Ast befindest und woanders hin möchtest, dann hangelst du dich Ast
  für Ast Richtung Erde bis zu einem gemeinsamen Ursprung des Orts, an den
  du hin möchtest und von dort hangelst du dich wieder Richtung Baumkrone,
  möglicherweise nachdem du zu einem alten oder neuen Stamm gesprungen bist.
- Kann ich mehrere Erfahrungen gemeinsam entfalten? Ja, einfach indem du sie
  alle anerkennst "Ahhh, ..." und dich in der folgenden Frage auf alle
  beziehst "Und ...".
- Kann ich dem/der Protagonist:in die Wahl lassen welche Erfahrung sie
  entfalten möchte? Ja, dies ist beispielsweise sinnvoll, wenn eine
  wohltuende ("resourceful") und eine herausfordernde Erfahrung da ist und du
  den/die Protagonist:in nicht in eine der beiden Richtungen lenken möchtest.
  In diesem Fall kannst du beide anerkennen und dich in deiner Frage auf
  beide beziehen.
- Wie kann ich mir das alles merken? Auf das Wesentliche beschränken/reduzieren
  und Notizen machen. Ein wesentliches Wort einer vergangenen Aussage des/der
  Protagonist:in genügt um eine ganze Welt in ihm/ihr lebendig werden zu
  lassen. Greife deswegen so wenig Worte als möglich auf (insbesondere beim
  Üben).
- Warum entfalte ich drei Erfahrungen/Empfindungen? Rasch vielerlei zu
  entfalten hilft den Blick zu weiten statt sich auf eine Erfahrung zu
  fixieren. Dadurch verbindet sich spezielles mit dem großen Ganzen im
  Moment und der/die Protagonist:in ist gleichzeitig mit Einzelheiten und dem
  Ganzen. Er/Sie lernt dadurch auch, dass in einem Moment verschiedene und
  möglicherweise widersprüchliche innere Anteile in ihm/ihr lebendig sind und
  in ihm/ihr trotz anscheinender Widersprüchlichkeit integriert sind.
- Wie komme ich zu einem Abschluss? Beispielsweise indem du alle Erfahrungen
  samt Qualitäten rekapitulierst mit "und" verknüpft und daran die
  Frage anschließt "Und wenn all das, ist es okay hier einen Abschluss zu
  finden?".
- Wann ist es sinnvoll einen Abschluss zu finden? Nachdem eine Zustandsänderung
  des Bewusstseins, des Kontexts oder der Erfahrung stattfand, dieser Raum
  gegeben und zur Ursprungserfahrung zurück verbunden wurde. Ein solche
  Änderung zeigt an, dass ein innerer Wandel stattfand, der im besten Fall
  eine neue Perspektive auf die ursprüngliche Herausforderung/Wunsch
  ermöglicht und nun Zeit zur Integration benötigt.

Durch das Einbeziehen des Körpers wechseln wir explizit die Ebene. Wir bleiben
mit einer Erfahrung länger als erwartet/gewohnt.

[Somatische Marker](https://www.resilienz-akademie.com/somatische-marker/) sind
körperliche Empfindungen, die unterbewusstes Erfahrungswissen mitteilen,
welches im Körper verinnerlicht wurde und welches uns darüber informiert ob die
wir das Erleben lieber vermeiden möchten oder uns ihm annähern. Der/Die
Protagonist:in offenbart somatische Marker verbal durch

- Worte zu Körperteilen/-bereichen/-orten wie Fuß, Bein, Kopf und so weiter
- Worte zu Körperempfindungen/-wahrnehmungen wie warm oder kalt
- Worte zu Gefühlswahrnehmungen wie ängstlich, wütend, freudvoll, traurig,
  beschämt
- Metaphern wie "Ameisen krabbeln unter meiner Haut"

und körpersprachlich durch

- Geräusche wie Seufzen oder Gähnen (diese sind oft Ausdruck körperlicher
  Empfindungen, für die wir keine Worte haben)
- Gesten wie das Zeigen/Deuten auf eine Körperstelle
- Mimik wie das Hochziehen der Augenbrauen

_Übung_: "Denke an eine Ressource. Und während du an die Ressource denkst, was
geschieht in oder um deinen Körper?". Das Entfalten einer Ressource ohne sie zu
benennen, also rein somatisch, lädt Offenheit bei der/dem Therapeut:in ein.

- Was ist eine Ressource? Eine Ressource ist etwas das ich habe und das jetzt
  schon hilfreich oder gut ist.
- Warum fragen wir nach dem was um den Körper geschieht? Der Leib, also der
  lebendige Körper, geht über die Hautgrenzen hinaus. Verkörperte
  Erfahrungen/Wahrnehmungen enden nicht mit den Körpergrenzen (der Haut)
  sondern gehen darüber hinaus. Ähnlich wie bei Energiefeldern. Diese möchten
  wir auch einladen. Beispielsweise wenn du als Fußgänger knapp und
  unerwartet von einer:m Fahrradfahrer:in überholt wirst, dann kann es an der
  Seite, auf der du überholt wurdest kribbeln, warm oder kalt werden, obwohl
  du nicht berührt wurdest. Oder wenn sich eine Person zu nahe vor dich
  stellt, kannst du das als Bedrohung wahrnehmen. Oder wenn du über dir
  stehst, dich wie aus Vogelperspektive beobachtest und dich gleichzeitig
  sehr lebendig und verbunden mit dir erlebst.

## Intentionen

Metabemerkung: Mitschreiben hat Vor- und Nachteile: Es schafft Überblick. Und
es schafft Abstand, Reduktion/Beschränkung. Deswegen gibt es eine
Zusammenfassung am Ende.

Metabemerkung: Frage nicht nach dem Grund in der Vergangenheit, dem Warum,
sondern nach der Ausrichtung für die Zukunft, dem Wozu.

Bisher haben wir entfaltet was hier und jetzt ist. Nun möchten wir hinzunehmen
was einzelne Erfahrungen oder was du möchtest. "Und was möchte ..., dass
geschieht?" oder "Und möchte ... etwas?" Empfindungen haben oft Intentionen.
Was möchte die Erfahrung mit der er/sie gerade im Kontakt ist? Und was möchte
er/sie?

Wozu ist das sinnvoll? Menschen sind motivierte Systeme. Die Motivation
entscheidet darüber, was wir wahrnehmen und wie wir denken, fühlen und handeln.

Wann setze ich das ein? Wenn ich ...

- mit Teilen arbeite, die eine Ladung aus der Vergangenheit tragen und die
  damals hilfreich waren;
- (innere) Ressourcen stärken möchte;
- körperliche Herausforderungen lösen möchte, indem ich die Bedürfnisse des
    Körpers erfülle/befriedige (ihm gebe was er braucht/bedarf).

Unterschiedliche Bedürfnisse verbinden.

Protagonist:in bringt Herausforderungen und Wünsche mit. "Was möchtest du? Und
wie kann ich dich dabei unterstützen?"

Antwortmodi (Intelligenzen): Vom Kopf (sprachlich, abstrakt, konzeptionell)

_Übung_ (aus der Körpertherapie):

- Ressource wachrufen "Denke an eine Ressource."
- Drei Erfahrungen entfalten "Und während du an die Ressource denkst, was
  geschieht in oder um deinen Körper?" und "Und gibt es noch etwas, das
  gerade geschieht, in oder um deinen Körper?"
- Intentionen ergründen "Und möchte `e` etwas?" und "Und was möchte `e`, das
  geschieht?" und "Und möchte `e` (noch) etwas?" und "Und was möchtest du(,
  das geschieht)?"
- Geben "Und kann `e` `i`?" und ermöglichen "Und kannst du `i`?" wie
  beispielsweise "Und kannst du eines von beidem jetzt tun?" als Übergang vom
  Entfalten zum Experimentieren, weg vom verbalen Entfalten und hin zu
  Bewegung, Berührung oder was stimmig ist.
- Benötigen "Und was benötigt `e` um `i`?" und woher "Und woher kann`b`
  kommen?" (Manchmal benötigt es noch etwas um eine Intention zu
  verwirklichen.)
- Integrieren "Und wenn `e1` `i1` möchte und `e2` `i2` möchte und ..., was
  möchtest du(, das geschieht)?" (Die Frage nach dem "du" gibt/führt einen neuen
  Bezug von außen rein/ein. Sie eröffnet eine neue Ebene, jene des großen
  Ganzen in dem alles geschieht. Sie lädt ein ein inneres Ganzes, eine Einheit,
  zu bilden, die all das anerkennt. Sie bietet einen größeren Rahmen an, in dem
  Inklusion stattfinden kann.) und geben "Und kannst du den Erfahrungen das
  geben?"
- Inkludieren "Und wenn all das, was möchtest du(, das geschieht)?"
- (Verkörpern "Und wenn du ..., was geschieht dann?")

"möchte ... sich entfalten!", "kann ... sich entfalten?" und "wenn sich ...
entfaltet was geschieht dann?"

Aus Geben und Ermöglichen kann ein Erfüllungsraum entstehen, der eine neue
Erfahrung schenkt, wie eine Massage oder Halt der Schultern. Durch Integrieren
wird kleinen Impulsen/Intentionen von Teilen Leben eingehaucht und
Selbstwirksamkeit erfahren. Manche wissen/haben klar was Teile möchten, jedoch
nicht was sie selbst möchten, in welchem Fall die Integrationsfrage
herausfordernd ist und wenn eine Antwort gefunden wird um so erkenntnisreicher.

Was ist dieses "du"? Je nach Protagonist:in und Sitzung kann es etwas anderes
sein, beispielsweise ein größerer umfassenderer Teil oder das wahre Selbst.

Übung:

- "Denke an eine Herausforderung."
- "Und erzähle mir davon."
- "Und während du mir von deiner Herausforderung erzählst, was geschieht in
    oder um deinen Körper?"
- Entfalten mit Intention am Ende
- "Und wenn all das, was möchtest du, das geschieht?"
- Entfalten

Wo gehe ich weiter in die Tiefe? Ich frage den:dir Protagonist:in: "Möchtest du
eine davon tiefer erforschen?" oder "Zieht dich eine davon an?"

Auf die Frage nach der Intention einer Erfahrung kann es sein, dass

- die Erfahrung selbst etwas tun möchte, in welchem Fall die Folgefrage ob sie
  das kann sinnvoll ist.
- die Erfahrung etwas haben möchte, in welchem Fall die Folgefrage ob der/die
  Protagonist:in ihr das ermöglichen kann sinnvoll ist.

Auf die Frage nach dem Ermöglichen kann es sein, dass

- er/sie das nicht kann, in welchem Fall die Folgefrage was benötigt wird, was
  sich wandeln sollte oder was geschehen muss damit er/sie kann sinnvoll ist?
- er/sie das _so_ nicht kann und auch nicht weiß was benötigt wird, in welchem
  Fall es sinnvoll sein kann anderes spielerisch und experimentierfreudig
  auszuprobieren.

Als Therapeut:in darf ich meine Aufmerksamkeit schulen zu chunken. Das passiert
vorbewusst. Es ist hilfreich um Erfahrungen zu entfalten.

- Was mache ich, wenn der/die Protagonist:in keinen Zugang zum Körper fühlt
  oder immer wieder vom Körper in den Kopf kommt? Wie hole ich ihn/sie vom
  Kopf in den Körper? Leichtfüßig Kopfgeschichten lauschen, aufgeladene
  Punkte davon aufgreifen und in den Körper bringen mit "Und während `p1`,
  `p2` und `p3`, was geschieht gerade in oder um deinen Körper?"
- Was mache ich, wenn der/die Protagonist:in Widerstände/Abneigungen gegen
  Clean Language hat? Wie reduziere ich Reibung von ihr/ihm an der
  möglicherweise gestelzt wirkenden Technik? Der Technik eine Psychoedukation
  voranstellen. Erläutern was wir machen, warum wir das machen, wofür das
  wichtig ist, was das theoretische/wissenschaftliche Fundament ist.
- Wie gehe ich damit um, wenn die Wahrnehmungen zu einer Ressource
  herausfordernd statt nährend sind? Das Herausfordernde mit "Ahhh, ..."
  anerkennen und überleiten zu nährender Empfindung durch "Und was geschieht
  gerade noch in deinem Körper, das du magst?"
- Wie gehe ich mit Verschlossenheit oder Negativausrichtung um? Diese Teile
  anerkennen, Ressourcen finden, entfalten und verankern und zwischen
  herausfordernden und lichten Teilen pendeln.
- Darf ich als Therapeut:in in Resonanz, beispielsweise mitfühlend, mit dem/der
  Protagonist:in sein? Ja, das wird sogar angestrebt. Wenn ich mit mir und du
  mit dir verbunden bist, dann sind auch wir verbunden, das heißt in
  Resonanz. Damit du als Therapeut:in davon nicht stark mitgenommen wirst,
  beispielsweise in Form von Mitleid, ist es wichtig für dich die Fähigkeit
  zu entwickeln dich abzugrenzen, durch Klarheit darüber was meine und was
  deine innere Landkarte ist und durch Mitschwingen auf einer anderen Ebene
  als der mitleidenden.
- Wie arbeite ich mit dieser Technik an konkretem Thema? Durch Überleitung von
  konzeptionellem zu somatische Coaching mit der Frage "Und während du an
  [Herausforderung/Entscheidung/Wunsch] denkst, was geschieht in oder um
  deinen Körper?"

Felt Sense (aus dem Focusing) ist eine Körperempfindung (eine verkörperte
Erfahrung), die inneres Wissen mitteilt, also eine Mittelung der
Körperintelligenz über Körperwahrnehmung. Sie ist ein inneres Gespür, das alles
umfasst, was die Körperintelligenz über etwas weiß und dies auf einmal
präsentiert/offenbart. Sie entzieht sich oft der Versprachlichung und offenbart
sich statt dessen als Empfindung, Geste, Miene oder Laut. In sprachlicher Form
drückt sie sich in Form innerer Bilder/Metaphern aus. Sie liefert sowohl
Informationen als auch Lösungen/Strategien. Ein Felt Sense ist nicht einfach da
sondern benötigt Zeit und Aufmerksamkeit um sich zu entfalten. Es ist oft
herausfordernd ihn zu verstehen.

Erforsche frei, leichtgewichtig, minimalistisch und spielerisch (explore
lightly) ohne zu bewerten, ohne zu interpretieren, ohne zu lenken, nicht
ergebnisorientiert. Gebe allem gleichwertig Raum, insbesondere wichtigen
Ressourcen. Skizziere, strukturiere und sortiere nur für dich und verwechsle
die Landkarte, die du dir von der Innenwelt des/der Protagonist:in machst weder
mit dessen/deren inneren Landkarte noch mit dessen/deren innerer Landschaft.
Lade ihn/sie ein in neue Vermischungen von Erfahrungen zu spüren und ungeahnte
Beziehungen zu entdecken.

Innenwelt durch Wiederholen von Erfahrungen und Qualitäten und Beziehungen und
Intentionen und Zeitzusammenhängen navigieren und dadurch den Rahmen je nach
Bedarf groß/weit oder klein/eng machen. Versuche tendenziell den Raum weit zu
lassen statt ihn zu schließen. Schließen tust du ihn mit "Mich interessiert
... Lass uns das beleuchten." und immer weiter öffnen mit "Mich interessiert
... Ist es für dich okay das zu beleuchten?", "Mich interessiert ...
Interessiert dich das auch?", "Mich interessiert ... Was interessiert dich?"
bis hin zu "Was interessiert dich?".

Wie du dich ausdrückst (sendest) macht einen Unterschied darin wie das Gesagte
empfangen wird.

- Stimme: Spiegeln, Klangfarbe, Tonhöhe, Lautstärke
- Gesten: Spiegelst du Gesten indem du sei bei dir oder bei ihm/ihr zeigst?
- Tempo: Etwas langsamer als das Gegenüber
- Sprachstruktur: Hypnotisch, tranceartig

Explizit den Körper einbeziehen. Explizit die Ebene wechseln. Mehr als Körper
versus Sprache ist das Bewusstsein.

Die Verwendung von "und" fügt Erfahrungen immer wieder zusammen, wodurch ein
Einheitsbewusstsein/-erfahrung entsteht und dadurch können sich neue
Einsichten/Erkenntnisse formen und beispielsweise vorher scheinbar
widersprüchliches gleichzeitig wahr sein. Im Alltag geschieht oft das
Gegenteil: Trennung.

Ich nehme immer wieder Rückbezug, verbinde, inkludiere, indem ich Erfahrungen
reiteriere.

Ich bleibe lange bei Erfahrungen und gebe Pausen. Ich erkenne alles an und
entfalte lebendiges. Ich nutze alles gleichwertig und lasse den:die
Protagonist:in entscheiden. Ich bleibe offen und neutral. Die Landkarte, die
ich von dem:der Protagonist:in gebildet habe dient nur zur Orientierung und
kann zu falschen Schlussfolgerungen führen.

## Zeit

Du erforschst intelligente Wandel unterworfene Prozesse, keine Dinge sondern
selbstwirksame Menschen, in einem intelligenten System. Dabei hältst du die
Zeit und den Kontext im Blick und betrachtest die ganze Herausforderung als
kontinuierlichen Prozess statt nur einen Ausschnitt der Herausforderung als
Ding. Mit den folgenden Fragen entfaltest du die zeitliche Komponente von
Erfahrungsprozessen:

- Vergangenheit: "Und was geschieht (da)vor?" oder "Und wenn `e`, was geschieht
  davor?"
- Zukunft: "Und was geschieht (da)nach?" oder "Und wenn `e`, was geschieht
  danach?"
- Herkunft: "Und woher kommt?" als Frage nach transpersönlicher positiver
    Kraft/Ressource zur Verwurzelung dieser. Sie kann unter anderem zur Antwort
    haben "Oh, diese Kraft bin ich" (Rückverbindung zu mir selbst) oder "Oh,
    diese Kraft ist unendlich." (Verbindung an was großes).

_Übung_: Eine Ressource erinnern, damit einhergehende Erfahrungen wie gewohnt
entfalten und zusätzlich die Zeitkomponente einbeziehen.

Die Zeitfragen sind absichtlich in Präsenz um im Hier und Jetzt in der
Zeitsequenz/-dimension zu reisen. Präsenz lädt einen anderen
Bewusstseinszustand ein als Vergangenheit und adressiert eine andere innere
Instanz.

Beispiel: Was geschieht kurz bevor ein Glaubenssatz aufkommt?

Die Frage nach der Zukunft lädt den/die Protagonist:in ein die
Sequenz/Zeitfolge in die Zukunft hinein zu erweitern. Erstaunlicherweise ist
dies oft möglich obschon die zukünftige Erfahrung offensichtlich noch nicht
stattgefunden hat.

Bereits während der/die Protagonist:in über eine Erfahrung berichtet ist diese
Wandel unterworfen. Eine Erfahrung existiert nur in der Gegenwart und diese
schreitet unaufhaltsam fort. Deswegen ist sie im Moment des Hineinspürens schon
eine andere als während die Frage danach gestellt ward und wandelt sich über
die Zeit des Hineinspürens fortwährend.

Wann ist es sinnvoll Zeitfragen einzubauen? Wenn etwas zu fehlen scheint, wenn
Veränderung stattfand, wenn Ressource und Selbst umfassender entfaltet werden
möchte und wenn die Herausforderung besser kennenzulernen ist (möglich durch
Erforschung der Sequenz/Ereignisse/Erfahrungen, die mit Herausforderung einher
gingen und gehen werden).

Wie entdecke ich Veränderung? Indem du dein Augenmerk legst auf

- Formveränderung
- Beziehungserkenntnis
- Sichtweisenwandel

Zeitliche Integration "Und wenn all das, was geschieht dann?"

_Übung_: Zwei Erfahrungen im Moment und über die Zeit entfalten.

## Beziehungen

_Übung_: Entfalte Beziehungen mit "Und gibt es eine Beziehung zwischen x und
y?" und "Und wenn x, was geschieht (dann) mit y?".

- Benannte Beziehungen können als Erfahrungen entfaltet werden.
- Bedeutsame Qualitäten können als Erfahrungen entfaltet werden.

## Regulation & Experimente

...

## Problem-Lösungsversuch-Wunschzustand-Modell (PLZ-Modell)

_Bisher_: Zum Einstieg sage ich "Denke an ein/eine Problem/Ressource." und dann
"Und während du an ... denkst, was geschieht in oder um deinen Körper?". _Neu_:
Was erforschen wir in Sitzungen? Wie steigen wir ein? _Ausrichtung_: Wir
möchten auf eine Ressource hinarbeiten.

Fallbeispiel: Frau, 32. Ich frage "Was möchtest du mit mir erforschen?" Sie
antwortet "Überreaktion in Partner:inschaft". Das ist ein Problem und kein
Wunschzustand. Ich frage "Und was möchtest du, das damit geschieht?". Sie
antwortet "Er hört mir nie zu." Das ist ein Problem. Ich wiederhole meine
Frage. Sie sagt "Ich will weniger Streit." Das ist ein Lösungsversuch. Ich
frage "Und wenn weniger Streit, was geschieht dann?" Sie antwortet "Dann
entsteht Intimität." Das ist ein Wunschzustand. Den gilt es nun zu entfalten.

Was ist der Ausdruck eines Problems? Er:sie nennt eine Situation in der
Gegenwart oder Zukunft, von der er:sie behaupte, dass er:sie sie nicht mag ohne
zu sagen, dass er:sie sie anders möchte. Beispielsweise "Meine Partner:inschaft
frustriert mich." oder "Er nörgelt an mir herum."

Was ist der Ausdruck eines Wunsch(zustands)? Er:sie nennt einen Wunsch, einen
Willen, ein Bedürfnis oder ein Begehren für eine neue positive
Erfahrung/Erleben, Handlungsweise oder Zustand ohne Bezug auf ein Problem.
Beispielsweise "Ich wünsche mir mehr Intimität in meiner Partner:inschaft."
oder "Ich möchte, dass er nicht an mir herumnörgelt."

Was ist der Ausdruck eines Lösungsversuchs? Er:sie nennt einen Wunsch durch
Bezug auf ein Problem. Oft in der Form von "Ich will, dass mein Problem weniger
wird oder ich will besser damit umgehen." Beispielsweise "Ich möchte, dass
meine Partner:inschaft weniger frustrierend ist." oder "Ich möchte
Anerkennung."

Ich versuche eine subjektive Aussage zu finden. Wünscht sich er:sie "Ich möchte
eine respektvolle Beziehung führen.", so frage ich "Wie ist die Beziehung für
dich?" um zu eruieren wie er:sie Respektlosigkeit erlebt oder "Und wenn du an
respektvoll denkst, was geschieht in oder um deinen Körper?" um zu eruieren wie
er:sie Respekt erlebt.

Ich steige ein mit der Frage "Was möchtest du mit mir erforschen?" und
wiederhole je nach Antwort so lange die folgenden Fragen bis ein Wunschzustand
gefunden ist und entfaltet werden kann.

- "Und wenn _Problem_, was möchtest du, dass damit geschieht?"
- "Und wenn _Lösungsversuch_, was geschieht dann?"
- "Und wenn _Wunschzustand_, ist da noch etwas über das?"

Ich entfalte den Wunschzustand und erforsche dessen Konsequenzen. Falls der
Wunschzustand aus einem Problem oder einem Lösungsversuch entstanden ist, nehme
ich später darauf Rückbezug. Oft zeigt es:er sich in neuem Gewand.

Durch die Erforschung des Wunschzustands, kann sich das System des:der
Protagonist:in neu organisieren/sortieren/ein neues Ganzes bilden und dadurch
das Problem in neuem Licht erscheinen lassen.

Wenn ich eine ungeschickte Frage stelle, dann nehme ich sie authentisch zurück
indem ich sage "Warte, ich möchte eine andere Frage stellen."

Wenn die Antworten zu viel Ladung tragen, dann wähle ich eine andere Technik.

Wenn ich wieder zu einem Problem komme (P1 --> L --> P2), dann fasse ich
zusammen indem ich frage "Da ist _P1_, du möchtest, dass _L_ damit geschieht
und dann geschieht _P2_. Und wenn all das, was möchtest du das damit
geschieht?" Dadurch frage ich was mit dem Problem des Spiralisierens um
Probleme geschehen soll.

Wenn ich immer wieder zu dem Problem zurückkomme, dann trete ich einen Schritt
zurück, frage "Wozu dient dir dieses Problem? Was ist deine Hoffnung darin? Was
möchtest du damit erreichen?" und setze einen neuen Rahmen, der die
Problemspirale durchbricht, beispielsweise indem ich mittels Psychoedukation
erläutere, warum ich einen Wunschzustand suche.

## Metaphern

Buchempfehlung: "Strangers to Ourselves: Discovering the Adaptive Unconscious"
von Timothy D. Wilson

Metaphern ermöglichen die Kommunikation zwischen der linken, bewussten,
expliziten und sprachlichen Hemisphäre und der rechten, unbewussten, impliziten
und einfachen (da körperlich) rechten Hemisphäre. Diese Kommunikation
ermöglicht Veränderung. Die Hemisphären bringen zwei Intelligenzen, das
Bewusstsein und das Un-Bewusstsein. Wir nennen sie nicht Unter- oder
Über-Bewusstsein, da beide nicht in einer Hierarchie stehen sondern gleich
notwendig und bedeutungsvoll sind und einander bedürfen und unterstützen. Es
ist sinnvoll das Unbewusstsein in Coachings einzubeziehen, da bewusst etwa 400
Bits pro Sekunde verarbeitet werden (die Spitze des Eisbergs, die über der
Wasseroberfläche sichtbar ist) und unbewusst etwa 11 Millionen Bits pro Sekunde
(die Basis des Eisbergs, die unter der Wasseroberfläche verborgen liegt). Das
sind etwa 27.500 mal mehr Bits pro Sekunde, die das Unbewusstsein im Vergleich
zum Bewusstsein verarbeitet.

Fahrplan (für diesen Abschnitt): Warum? Wie? Wie geil!

### Kommunikation mit dem Unbewusstsein

Viele Verhaltensweisen in der Außen- und Innenwelt sind unbewusst und wurden
bewusst erlernt. Das Unbewusstsein ist das automatisierte Verhalten, Denken,
Fühlen und Neurozeptieren (von "Neuroception"). Sobald das Unbewusste bewusst
gemacht wird, ist es etwas anderes. Je bewusster etwas ist und je mehr im
Expliziten, um so einnehmender und heraus- oder überfordernder ist es. Etwas
unbewusst/implizit zu lassen ist oft sinnvoll und lebensnotwendig.

Bewusstseinswechsel: Ich coache so, dass das Unbewusste eingeladen ist ins
Bewusst zu werden. Ich lade es nur ein, da das Unbewusste schwer von innen zu
halten und von außen zu kontrollieren ist und deswegen zu
Überforderung/-wältigung führen kann. Ich arbeite titriert und pendelnd mit den
Offenbarungen des Unbewusstseins, damit keine Überflutung/Retraumatisierung
geschieht.

Felt Sense: Mit der Frage "Und während du darüber nachdenkst, was geschieht in
oder um deinen Körper?" lade ich das Unbewusstsein sich über den Körper zu
offenbaren/mitzuteilen --- eine verkörperte Erfahrung.

Wenn sich der:die Protagonist:in auf die rechte Hemisphäre einlässt, dann
verliert er:sie das eigentliche Ziel aus den Augen. Deswegen ist dafür ein
klarer Rahmen wichtig und dass unter Verwendung der linken Hemisphäre die
rechte einbezogen wird. Dabei hält die linke Hemisphäre das Ziel im Auge. Eine
Herausforderung, die hierbei oft auftritt, ist, dass die beiden Hemisphären
verschiedenes wollen und darüber im Konflikt stehen. Wie können sie in Einklang
gebracht werden oder wie kann das miteinander vereinbart werden?

Übung (Ankommen): Ich leite eine Erdungsübung an und entfalte dann mit der
Eingangsfrage "Und was geschieht gerade, in oder um, deinen Körper?". Dabei
verwende ich die Sprachstruktur 1. Anerkennen mit "Ahhh, `x` und `y`", 2. ...
und die Frage "Und wenn `x`, wo ist `x`?".

Ich begleite minimalistisch in Sprache, Mimik und Gestik und spiegele minimal.
Die Spiegelung der Körpersprache ist oft eingängiger für das Unbewusstsein als
Worte.

Durch die Auswahl der Fragen, die ich stelle, gebe ich etwas von außen rein.
Welche Fragen förderlich sind benötigt Übung und die Auswahl geschieht
implizit/unbewusst/intuitiv und nicht konzeptionell/kopfgesteuert. Eine
Daumenregel ist, dass ich mich auf Wünsche und positiv geladenes ausrichte.

Menschen haben verschiedene Zugänge zu Wahrnehmung, Erfahrung, Erkenntnis und
so weiter. Manche erleben eher visuell, andere olfaktorisch, andere
geschmacklich, andere auditiv und andere körperspürig. Als Coach ist es wichtig
für mich darüber Selbsterkenntnis zu haben: Was ist mein Zugang? Was fällt mir
leicht? Was ist natürlich für mich? Diese verschiedenen Zugänge bedeuten auch,
dass ich meine Perspektiven nicht auf jene des:der Protagonist:in übertragen
kann.

### Metaphern

Buchempfehlung: "Metaphors We Live By" von George Lakoff und Mark Johnson

Tiefere Themen sind oft nur über Metaphern greifbar. Sprachlich werden
Metaphern wie Sätzen wie "Das ist wie ...", "Das ist vergleichbar mit ...",
"Das erinnert mich an ..." oder "Das ist so wie ..." eingeleitet.

Warum sind Metaphern so wichtig? Alles was Menschen verstehen basiert auf
Metaphern. Wir verstehen die Welt durch Übersetzen in Metaphern. Über Metaphern
verwandeln wir abstrakte Konzepte in körperliche Kognitionen. Beispielsweise
beschreiben Menschen Zuneigung als Wärme, wichtig als groß, Themen als leicht
oder schwer, das Leben an sich als Kampf, Spiel oder Abenteuer. Wir verstehen
die Welt nur durch Metaphern, besonders die Innenwelt von Menschen.

Was ist eine Metapher? Eine Metapher ist das Erleben und Verstehen einer Sache
in Bezug auf oder durch eine andere Sache, also eine Übersetzung.
Beispielsweise sind Münzen handfeste/greifbare Metaphern für einen Kaufwert.
Als Mensch kann ich Münzen vor mir auslegen und mit Personen aus meinem Leben
verknüpfen. Wenn ich mir nun die Frage stelle, welche ich wegwerfen möchte,
dann ist die Antwort nach der Verknüpfung eine ganz andere als zuvor.

Aus welchen Bereichen entlehnen Menschen Metaphern?

- Körper: "Mir ist warm ums Herz."
- Physik: "Ich stehe darüber."
- Maschinen: "Es läuft wie geschmiert."
- Lebewesen: "Er:Sie hat ein dickes Fell."

Welche anderen Bereiche kennst du?

Übung (Diskussion): Diskutiert in Kleingruppen eigene Ideen über Metaphern.
Bemerkt ihr wie wesentlich Metaphern für Kommunikation sind?

Jedes Konzept basiert auf Metaphern / ist metaphorisch, das heißt, es ist in
einer körperlichen Erfahrung fundiert. Jeder Begriff/Wort entstand einst aus
einer körperlichen Erfahrung.

Warum verwende ich Metaphern in Coachings? Weil sie mächtig sind. Deswegen
tauchen sie in Ansätzen wie Hypnose, Journaling, Gestalttherapie und
Held:inreise auf.

Für mich sind besonders autogene Metaphern von Bedeutung. Das sind jene
Metaphern, die der:die Protagonist:in für seine:ihre Innenwelt bereits hat oder
dafür entwickelt. Diese Metaphern eröffnen den Zugang zu einem größeren Wissen,
jenem des Unbewusstseins (ich würde das gerne Unwissen nennen). Solche
Metaphern entwickeln manchmal eine eigene Dynamik und deren Erforschung kann zu
plötzlichen erlebten Einsichten führen. Sie beziehen ganze
rechts-hemisphärische Phänomene mit ein und behalten alles im Kontext.

In Sitzungen greife ich Metaphern auf und entwickle/entfalte/erforsche sie mit
Clean Language.

Beispiel: Thema (explizit und sprachlich): Die Protagonistin ist beruflich in
einer Chefinrolle. Sie beschreibt, dass sie in dieser Rolle antreibt.
Stattdessen möchte sie motivieren. Ich frage wie das sei zu motivieren.
Metapher (implizit und bildhaft): Sie antwortet es sei wie Licht als
Orientierung und sie würden gezogen statt von ihr angeschoben zu werden. In der
weiteren Entfaltung sagt sie es sei so als würde Licht sie (die Protagonistin)
rufen. Hier entwickelt die Metapher eine eigene Dynamik, sie ruft.

Damit autogene Metaphern sich zeigen/offenbaren/hervorscheinen, lade ich einen
alternativen Bewusstseinszustand ein, eine Art Trance. In dieser ist anderes
möglich als im Alltagsbewusstsein. Die hypnotische Sprachstruktur von Clean
Language lädt diesen Zustand ein.

Übung (Metaphern erforschen):

1. Ich sage "Denke an etwas, das dir wichtig ist."
1. Ich sage "Erzähle mir davon." Vom Gesagten greife ich die Metaphern auf
   (statt wie bislang somatische Körperwahrnehmungen).
1. Ich erforsche die Metaphern mit "Ah, `x` & `y` & `z`. Und wie ist `x`?"

In einer Sitzung beziehe ich Körperwahrnehmungen und Metaphern ein beides
verwebend.

Nutze ich den "Felt Sense", so nutze ich dadurch auch Metaphern. Der
Unterschied zur direkten Arbeit mit Metaphern ist worauf ich den Fokus lege.
Körperwahrnehmungen und Metaphern können nur miteinander erforscht werden.

Idee: Indem ich Symbole, Beziehungen, Zeit, Intentionen und so weiter
entwickle, baue ich das Bewusstsein für eine innere Landschaft auf und
stabilisiere diese.

Neue Frage: "Das ist wie was?"

Ich kann Metaphern dafür nutzen Abstand zu schaffen. Dazu arbeite ich statt mit
einem Erinnerungsinhalt mit einer Metapher. Dadurch kann sich die explizite
Erinnerung wandeln.

Sobald sich der:die Protagonist:in in einem alternativen Bewusstseinszustand
befindet, dient die minimale Sprache von Clean Language dazu, dass er:sie in
seiner:ihrer inneren Landschaft bleiben kann. Bis dieser Bewusstseinszustand
erreicht ist, kann die besondere Sprache befremdlich wirken.

Wie entwickle ich Metaphern weiter? Ich entwickle eine Metaphernlandschaft: Ein
Ganzes aus mehreren Symbolen. Durch die Entfaltung dieser Landschaft wird sie
immer stabiler. Die Landschaft inkludiert Körper, Bilder, Positives, Negatives
und so weiter mit "Und". Die linke Hemisphäre sieht die Einzelheiten und die rechte
sieht das Ganze (mehr als die Teile).

Beispiel (Fortsetzung): Ich entfalte mit ihr die Metaphernlandschaft. Sie sagt
dann entstehe Wind und das Wetter ändere sich. Auf die Frage ob da noch etwas
über all das sei, antwortet sie, dass sie in sich ruhen könne.

Symbole sind ganzheitliche Phänomene: Visuell, auditiv, sozial, emotional und
energetisch. Und sie sind statische Bilder oder dynamische Filme.

Als Coach:in muss ich nichts verstehen oder interpretieren. Ich frage nach, ich
folge meiner Intuition und ich inkludiere/verbinde/verganzheitliche mit "Und".

Wenn beim Entfalten Zyklen entstehen, dann spiegele ich alles alles mit "und"
verknüpft und frage dann "Und wenn all das, was möchtest du, das(s) geschieht?"
Dadurch lade ich eine übergeordnete Perspektive jenseits der Verstrickung ein,
transzendiere sie. Alternativ könnte ich nach Intentionen oder der Zeit davor
fragen.

Übung (die "wie was"-Frage):

1. Ich lade ein "Denke an etwas, das dir wichtig ist."
1. Ich frage nach einer Metapher dafür "Und das ist ... wie was?"
   (beispielsweise "Dieser Ort, an dem du dich gleichzeitig wohl fühlst und
   konzentrieren kannst, ist wie was?")

Wenn ich für eine Metapher mehrere Symbole entfaltet habe (analog zur
Anreicherung einer Erfahrung mit mehreren Körperwahrnehmungen) und noch weitere
Symbole einladen möchte, dann navigiere ich zurück und führe zusammen indem ich
die Symbole mit "und" verknüpft spiegele und frage "Und wenn all das, ist da
noch etwas über `m`?" Beispielsweise die Symbole Rand, Wasser, Spiegelung für
die Metapher Pfütze.

Wenn ich in eine Sackgasse gerate, dann navigiere ich zurück oder ich stelle
die gleiche Frage nochmals oder ich ermächtige den:die Protagonist:in mit der
Frage "Ist da etwas was dich anzieht / du erforschen möchtest?"

Wenn ich spiegele, spiegele ich das Wort "ich" als "du" und umgekehrt oder
spiegele ich wortwörtlich "ich" mit "ich" und "du" mit "du"? Beides ist
möglich. Was ist für mich stimmig?

Wie finde ich aus einem verkopften Prozess/Entwicklung/Situation heraus? Indem
ich zurück gehe/navigiere und dann eine andere Richtung einschlage / eine
andere Frage stelle. Um erst gar nicht hinein zu geraten, stelle ich Fragen so,
dass sie der intrinsischen Logik der Metapher gerecht werden (ähnlich wie ich
Körperwahrnehmungen wortwörtlich spiegele, damit der:dir Protagonist:in meine
Aussage nicht revalidieren muss im Sinne von "Habe ich das so gemeint?")
Beispielsweise frage ich bei der Metapher "`x` steht `y` im Weg" nicht nach
"Weg" mit "Und was für eine Art von Weg ist das?"

Glaubenssätze sind rechts-hemisphärische Metaphern, das heißt, ganzheitliche
Analogien dafür wie ich bin.

Die Bahn von einer Metapher zu einer Körperempfindung und umgekehrt erkunde ich
durch die Fragen "Und wenn `m`, was geschieht dann, in oder um, deinen Körper?"
und "Und `e`, `e` (ist) wie was?"

Durch das Inkludieren mit "und" kann aus einzelnen Qualitäten wie "gelb" und
"leuchten" eine Metapher wie "Licht aufgehen" oder "Sonnenschein" entstehen.

Als Caoch:in bereite ich den Boden für unerwartet Veränderungen --- das ist
meine innere Grundhaltung. Diese Veränderung kann ich nicht vorhersehen. Ich
kann nur im Vertrauen darauf sein, dass sie kommen wird.

Tiefe Veränderungen/Erfahrungen entstehen oft am Ende von Sitzungen.

Kreativität entsteht aus der Kommunikation zwischen der linken und der rechte
Hemisphäre, aus der Arbeit mit einem Thema und dem Loslassen aller Erwartungen,
aus Anstrengung und Pause, aus bewusster Aktivität und unbewusster Passivität,
aus ausgerichtetem Handeln und gleichmütigem Vertrauen in den Lauf der Welt. In
Pausen kommen Erkenntnisse aus dem Unbewusstsein. Die linke Hemisphäre bewegt
sich fokussiert in der bekannten eingeschränkten Landkarte und die rechte
Hemisphäre transzendiert diese, sie lebt in der Landschaft und setzt dort
Erfahrungen auf neue Weise zusammen, nimmt neue Sichtweisen/Perspektiven ein,
sieht das Ganze, vereint scheinbare Widersprüche (was auf der Landkarte
widersprüchlich zu sein scheint, ist es in der Landschaft oft nicht).

Aus Ganzem/Ganzheit entsteht Veränderung. Dahin gelangen wir durch Inkludieren
mit "und" und durch die Erkundung der Landschaft und Entwicklung der Landkarte.
Wir verfeinern/vergrößern die Landkarte immer weiter, bis unmögliches möglich
wird. Veränderung ist das, was uns unserem Wunsch näher bringt.

Wenn das Ganze, die neue Landkarte, stabil ist, dann entsteht eine Einsicht,
ein Aha-Erlebnis, ein Aha-Effekt und plötzlich erscheint alles neu, erstrahlt
in neuem Licht. Und vor diesem Aha kommt oft eine Stille, eine innere Ruhe, in
der sich das System neu sortiert.

Nach einem Aha-Erlebnis frage ich "Und was geschieht dann?" und verbinde es mit
Symbolen der Landschaft zurück (wie haben sich diese gewandelt?) mit "Und wenn
`a`, was geschieht dann mit `s1` und `s2` und `s3`?"

In einer Bilderbuchsitzung wie im obigen Beispiel wird erst ein Thema genannte,
dann die Landschaft erkundet, dann Metaphern dafür gefunden, dann entsteht ein
Aha-Erlebnis und dann lasse ich diese Veränderung reifen, indem ich nach dem
Wandel in den Metaphern frage, dann in der Landschaft und dann im Thema. Der
Reifungsprozess der Veränderung nach dem Aha-Erlebnis ist der Großteil der
Sitzung.

## Entwicklungsstadien

Prozess & Fähigkeit: Angepasst begleiten

Entwicklungsstadien der Eigenkörperwahrnehmung:

1. Keine Körperwahrnehmung (oder nur abstrakt beschrieben): Für ihn:sie ist
   sein:ihr Körper so unsicher, dass seine Wahrnehmung keine Ressource ist und
   ein unbewusst schlummerndes Trauma aufwecken kann.
1. Manche Körperbereiche wahrnehmbar: Er:Sie nimmt beispielsweise nur ihre
   linke Schulter und deren Qualitäten wahr.
1. Eine Empfindungen im Körper verfolgbar: Er:Sie kann beispielsweise die
   das Gespür der einströmenden Atemluft von den Nasenlöchern bis in die Lunge
   verfolgen oder einer wandernden Empfindung im Verdauungstrakt folgen.
1. Mehrere Empfindungen gleichzeitig im Körper verfolgbar: Er:Sie kann
   beispielsweise gleichzeitig den Atem spüren und wandernde Spannungsmuster im
   Beckenboden.
1. Ganzer Körper als Einheit wahrnehmbar: Er:Sie nimmt ihren Körper umfassend
   wahr.

Entwicklungsstadien transzendieren und einbeziehen: Bereich, Empfindungen und
Einheit gleichzeitig wahrnehmen, wobei einzelne hervorstechen.

Wie beziehe ich Entwicklungsstadien ein? Mit interaktiven somatischen Übungen
(siehe Skript 1.2).

1. Rahmen setzen
1. Übung anleiten
1. Wahrnehmung entfalten

Dabei wechsel ich zwischen verschiedenen Sprachmodi.

Wenn der Körper keine Ressource ist, dann kann es regulierend sein über etwas
im Außen zu reden wie das Wetter.

Übung:

1. Übung beschreiben
1. Kurzes kraftvolles energetisierendes Ein- und Ausatmen anleiten mit
   anweisender Sprache
1. Entfalten einleiten "Und während du auf diese Weise atmest, was geschieht
   dann, in oder um deinen Körper?"
1. Verlängertes Ausatmen anleiten mit die Aufmerksamkeit lenkender Sprache
1. Entfalten einleiten "Und während du auf diese Weise atmest, was geschieht
   dann, in oder um deinen Körper?"

Wenn er:sie dissoziiert, dann setze ich den Rahmen eng und leite stoßweises
Ausatmen an um ihn:sie zu energetisieren und zu beleben und von Starre/Kollaps
nach Kampf/Flucht zu begleiten vorzugsweise in seine:ihre Wut. Wenn er:sie
stark dissoziiert, dann arbeite ich besser mit seiner:ihrer Muskulatur statt
mit seiner:ihrer Atmung., beispielsweise lasse ich mich von ihm:ihr wegdrücken.

Einatmen hilft ihm:ihr seinen:ihren Körper zu füllen und zu spüren.

Welche Übungsweise in welchem Entwicklungsstadium?

- von unbewusst zu peripher: Simple Körperwahrnehmung, möglicherweise mit Berührung
- von peripher zu ungewohnt: Geleitete somatische Übung
- von ungewohnt zu kompetent: Interaktive somatische Übung
- von kompetent zu selbstgeleitet: Clean somatisches Coaching
- selbstgeleitet: Selbstcoaching

Ich setze die Rahmen geeignet/passend durch die Auswahl der Werkzeuge
--- beispielsweise Übungen, Fragen, Berührung ---, der Sprache ---
beispielsweise Fragen, Anleiten, Optionen --- und der Dauer --- beispielsweise
kurz & explizit oder länger & offen.

Ich pendele zwischen geleiteten Übungen und "Clean Language"-Entfalten mit
verschiedenen Fokussen.

Ein und dieselbe Übung kann ich durch verschiedene Rahmen für unterschiedliche
Bedürfnisse anpassen. Wenn ich nicht weiß welcher Rahmen passt, dann starte ich
mit einem eher weiten Rahmen und mache ihn bei Bedarf enger oder weiter.

Eine interaktive Übung zeichnet sich dadurch aus, dass ich fließend zwischen
der Ausübung einer Handlung und der Entfaltung ihrer Wirkung wechsel.

Übung:

1. Übung beschreiben und fragen "Ist das okay für dich?"
1. Eine klare wiederholbare Bewegung anweisend anleiten.
1. Entfalten einleiten mit "Und während du dies tust, was geschieht dann, in
   oder um deinen Körper?"
1. Einen körperlichen Bewegungsimpuls aus den vorhergegangenen Schritten
   aufgreifen und ihn:sie bitten diesen langsam zu wiederholen mit "Und ist es
   okay ... verlangsamt zu wiederholen?"
1. Entfalten einleiten mit "Und während du dies tust, was geschieht dann, in
   oder um deinen Körper?"

Interaktive Übungen biete ich zur Regulation an, wenn mir dies
sinnvoll/zielführend erscheint. Für den Aufbau von Ressourcen nehme ich einen
anderen Rahmen. Beispielsweise frage ich "Was tut dir gerade gut?" oder ich
biete eine Übung an und stelle dann die Was-Entfaltungsfrage.

Ich leite das an was ich selbst gut finde, da ich dann auf eine Weise anleite,
die ihn:sie besser abholt.

Geschäftsidee: "Encounter Sessions" zum Provozieren und Lernen mit
Konflikten/Reibungen umzugehen und Grenzen zu setzen.

## Selbstkörperwahrnehmung

Die Selbstkörperwahrnehmung ist als Grundfähigkeit verschieden ausgeprägt. Sie
kann geschult und erlernt werden. Wir unterscheiden zwischen folgenden
prägnanten Entwicklungsstufen, die diskrete Punkte eines kontinuierlichen
Entwicklungsprozesses sind und unterschiedliche Körperteile können gleichzeitig
an unterschiedlichen Stufen sein und abhängig vom Nervensystemzustand und
anderen inneren Zuständen:

1. unbewusst: Körperteil nicht wahrnehmbar. Kein bewusstes Abbild im Gehirn.
   (Fremdkörper) "Was ist das?"
1. peripher: Körperteil im Kontakt nach außen wahrnehmbar. Bewusstes Abbild
   der Peripherie im Gehirn. (Umhüllung/Körpergrenzen) "Oh, ein Boden!"
1. ungewohnt: Körperteil als massives Ganzes wahrnehmbar. Bewusstes Abbild
   der Peripherie und des Inneren. (Außen/Rand und Innen/Füllung) "Aha, mein
   Bein!"
1. kompetent: Mehrere Körperteile gleichzeitig wahrnehmbar und
   differenzierbar. (Symphonie/Orchester von Empfindungen) "Wow, schöne Musik!"
1. selbstgeleitet: Körper als Ganzes wahrnehmbar (Ich) "Ja, das bin ich!"

Bei der Wahrnehmung von Empfindungen ist je nach Entwicklungsstufe
unterscheidbar welche Qualitäten sie haben (Qualitäten; beispielsweise wo genau
diese sind oder wie intensiv sie sind), wie sie sich über die Zeit verändern
(Zeit; beispielsweise wohin sie wandern), wie sie in Beziehung zueinander
stehen (Beziehungen), warum sie da sind / was sie möchten (Intentionen).

Passe Übungen an die Entwicklungsstufe an. Mache den Rahmen eng bei niedrigen
Stufen und weit bei höheren. Über die Zeit gelangt der/die Protagonist:in von
Unbewusstheit zu Selbstermächtigung.

- Von unbewusst zu peripher: Frage nach einfachen Körperwahrnehmungen, klar,
  klein und kurz.
- Von peripher zu ungewohnt: Leite eine somatische Übung an.
- Von ungewohnt zu kompetent: Begleite eine interaktive somatische Übung.
- Von kompetent zu selbstgeleitet: Coache somatisch mit Clean Language.
- Ab selbstgeleitet: Halte Raum für Selbst-Coaching.

Setze die Rahmen passend: Tools, Sprache, Dauer (TODO Konsultiere Aufnahme.)

_Übung_: Leite einen Body Scan mit den drei Bereichen Füßen, Becken und
Schultern an und gehe mit "Und wenn all das, was geschieht in oder um deinen
Körper?" über zum Entfalten.

Deine Grundhaltung ist Interesse für den/die Protagonist:in. Keine Gier nach
Neuem (Neu-Gierde) sondern bei ihm/ihr sein (inter-esse; lateinisch für "dabei
sein").

Bei der Anleitung eines Body Scans entwickelst du die Kompetenz einen Rahmen zu
halten ohne das verbale Rückmeldung auf das Gesagt kommt. Wie ist das für dich?
Was nimmst du nonverbal wahr? Wie ist es, wenn du beim Body Scan selbst deine
Sinne nach innen ziehst? Merkt dein Gegenüber, wenn du beim Anleiten auf diese
Weise mit dir selbst verbunden bist?

Bei Clean Language sind die Fragen offen, der Rahmen maximal weit und du hältst
eigenes heraus. Dies benötigt eine hohes Maß an Selbst- und Körperwahrnehmung
des/der Protagonist:in. Ist dies nicht gegeben, so ist ein engerer Rahmen mit
konkreteren Anweisungen sinnvoll. Der notwendige Rahmen ist abhängig von
Fähigkeiten und Vorlieben. Verenge und weite den Rahmen je nach Notwendigkeit
und Sinnhaftigkeit. Arbeite dabei im Konsens.

Rahmenweite:

- Anweisen: "Mache einen F-Laut bei der Ausatmung" oder "Bleib mit dem ..."
- Mini-Vereinbarung: "Verlängere deine Ausatmung indem du entweder einen F-Laut
  machst oder deine Kehle leicht verschließt oder einen leisen Brummton
  erzeugst"
- Aufmerksamkeit lenken: "Lenke deine Aufmerksamkeit auf eine lange Ausatmung"
  oder "Kannst du einen Weg finden deine Ausatmung zu verlängern?" oder "Wie
  kannst du interessiert bleiben an dem was geschieht?"

_Übung_ (Übergänge zwischen Üben und Entfalten): Leite eine interaktive
somatische Übung an und sei dabei sprachlich flexibel. Beschreibe zunächst die
Übung selbst und frage ob der/die Protagonist:in bereit ist diese
durchzuführen. Im ersten Teil "Anweisen" weist du an kräftig mit dem linken und
danach mit dem rechten Fuß in den Boden zu drücken mit Nachspürphase. Danach
weist du an dies mit einem der Füße zu wiederholen, wobei der/die
Protagonist:in selbst entscheidet mit welchem. Anschließend entfaltest du die
Erfahrung. Im zweiten Teil "Aufmerksamkeit lenken" lädst du ein eine
Laufbewegung auszuführen mit Nachspürphase. Anschließend entfaltest du die
Erfahrung.

Du kannst als Therapeut:in den Rahmen enger machen mit Anweisungen,
Mini-Vereinbarungen oder Aufmerksamkeitslenkung. Und du kannst den/die
Protagonist:in bitten dies zu tun mit Fragen wie "Was interessiert dich?" oder
"Was möchtest du weiter erforschen?".

_Übung_: Wie eben, wobei der erste Teil eine klare wiederholbare Bewegung ist
und der zweite Teil einen Impuls des/der Protagonist:in aufgreift und ihn/sie
diesen langsam wiederholen lässt.

Der Körper des/der Protagonist:in ist stets intelligenter als ich bezüglich
seiner/ihrer selbst. Folge diesem!

Wann führe ich eine solche Übung durch? Am Anfang von Sitzungen bevor sich eine
Eigendynamik entwickelt hat. Indem Körperübungen bereits zu Beginn etabliert
werden, fühlen sie sich im späteren Verlauf natürlicher an.

_Übung_ (zum Ankommen): Leite sanftes Atmen an und entfalte die Erfahrung.

Übungen können durchgeführt werden zum Ankommen, Regulieren, Experimentieren
oder Nähren. Die innere Haltung und der Rahmen legen die Intention einer Übung
fest, das heißt sie legen fest wie diese Übung wirkt. Dies liegt daran, dass
die Nervensysteme Lebewesen miteinander kommunizieren. Deswegen ist es für dich
als Therapeut:in wertvoll deine Nervensystemzustände gut zu kennen und gut in
Selbstregulation zu sein.

Wann ist welche Intention angebracht?

Neuroception: Bin ich gerade sicher? Vagus: 80% efferent (Informationen vom
Körper ins Gehirn) und 20% afferent (vom Gehirn in den Körper). Sicherheit
hängt von Körperinformationen ab.

Natürliche Werkzeuge des Körpers zum Herstellen von Sicherheit sind:
Orientieren, Halt, Bewegen, berührt Werden, Erden, Atmen.

Atmung ist effektiv, da vagale Nervenfasern um die Lunge, den Rachen und TODO
liegen.

Welche Sprache, Stimme, innere Haltung, ... dient der Regulation?

Regulation aus rot in grün mobilisiert/aktiviert beim Weg über orange. Wut
benötigt Gegenüber. Dabei sind klare Rollen vorteilhaft. Wut ist Kontaktangebot
und möchte Raum. Klaren Rahmen abstecken und gemeinsam darin sein.
Ungleiche/asymmetrische Beziehung zwischen Coach:in und Protagonist:in hat Vor-
und Nachteile.

_Übung_ (Regulation): Ausatmen verlängern. Entfalten. Impulse aufgreifen und
diese verlangsamen.

Frage ob es okay ist mit der momentanen Nervensystemaktivierung zu bleiben und
nur wenn dies nicht der Fall ist biete Regulation an.

Entspannung zeigt sich durch autonome Reaktionen wie Gähnen, Schlucken,
Magengrummeln, Verdauungsgluckern.

Dem Halten Halt anbieten kann die Erfahrung vertiefen und dadurch zu größerem
Verständnis führen im Sinne von "Achso, du machst das um mir zu zeigen, dass
..." oder "Oh, du machst das weil ..." oder "Aha, du machst das um mich vor ...
zu schützen". Beispielsweise wenn der/die Protagonist:in die Schultern nach
oben zieht, kann ich als Therapeut:in diese von außen halten.

Wenn du reguliert bist, dann kannst du mit Stille gut sein, fein wahrnehmen und
fein darauf eingehen.

Rollen: Wie nahe lasse ich Menschen an mich heran? Kreise je nach Rolle kleiner
oder größer ziehen.

## Mottoziele

Aus dem Sein entsteht die Handlung.

Übung (Ankommen): "Was wünschst du dir zum Ankommen?"

Habe ich Klarheit, so handle ich implizit. Suche ich etwas, so wandelt sich
meine gesamte Wahrnehmung des Raums und ich habe andere Handlungsimpulse ---
ich leben in einer ganz anderen Welt. Kurz: Meine Welt ändert sich mit meinen
Zielen. Und die Bewertung meiner Welt hängt von meinen Zielen ab.

Alles ist ein Prozess und ist kontextabhängig, auch Handlungsimpulse.

Meine Ziele sortieren meine Welt: Was ist wichtig? Was sehe ich nicht einmal?
Was kann ich filtern? Was hilft? Was ist im Weg?

Mein explizites Bewusstsein verarbeitet etwa 40 Bit pro Sekunde und mein
implizites Unbewusstsein etwa 11 Millionen Bit pro Sekunde.

Das ist un-bewusst.

Ohne Ziele gibt es weder Bedeutung noch Wahrnehmung.

Ausrichtung/Wunsch --> Absicht --> Wahrgebung --> roter Faden --> Wertnehmung

Beispiel (Gefühle): Dopamin erzeugt Frustration mit dem Eindruck gleich da zu
sein: Eine positive Erwartung. Beispielsweise die Vorfreude auf ein Geschenk
ist schöner als der anschließende Besitz des Geschenkten. Wenn ich den Eindruck
habe meinem Ziel näher zu kommen, dann schüttet mein Körper Dopamin aus. Sobald
ich mein Ziel erreiche, stoppt die Ausschüttung. Das höchste Hoch erlebe ich
kurz vor Erreichen meines Ziels. Sobald ich es erreicht habe, tritt
Ernüchterung ein bis ein neues Ziel gefunden ist. Mein Dopaminsystem kann mir
den Eindruck vermitteln "im Flow" zu sein. Es zeigt mir an ob ich vorwärts
komme, was ich willentlich tun kann und ob mein Leben einen Sinn hat. Es ist
eines der beliebtesten Systeme. Und es gibt andere wertvolle (Hormon)systeme.

Phänomenologie: Zuerst nehme ich wahr auf welche Weisen ich mit meiner Umwelt
interagieren kann. Erst später finde ich Worte dazu. Gehe ich durch eine Gruppe
von Menschen, so sind diese zunächst Lücken oder Hindernisse und erst wenn eine
Interaktion stattfinden soll, so werden sie zu Menschen. Ich nehme erst wert
und dann wahr: Wertnehmung findet vor Wahrnehmung statt. Das Fühlen und das
Gefühl sind kein Endergebnis eines kognitiven Prozesses, sondern das, was am
Anfang jedes Wahrnehmungs- und Erkenntnisprozesses steht "Im Anfang war das
Gefühl!" "Alles primäre Verhalten zur Welt überhaupt […] ist eben nicht ein
“vorstelliges”, ein Verhalten des Wahrnehmens, sondern […] primär ein
emotionales und wertnehmendes Verhalten" (M. Scheler, Formalismus, GW II, 206).

Was ist ein Ziel?

- Ergebnisziel: "Das möchte ich." --- Ergebnis in der Zukunft
- Handlungsziel: "Das will ich tun." --- Verhalten in der Zukunft
- Mottoziel: "Wie möchte/muss ich sein?" oder "Da möchte ich hin." --- Haltung
    im Hier und Jetzt

"Mottoziele zielen nicht auf einen Endzustand in der Zukunft, sondern auf das
Hier und Jetzt in der Gegenwart. Sie sind nicht konkret und spezifisch, sondern
allgemein und bildhaft formuliert. Und sie beziehen sich nicht auf das
Verhalten, sondern auf die Haltung  eines Menschen." (Maja Storch und Tanya
Faude-Koivisto, [Ressourcen aktivieren mit
Mottozielen](https://www.majastorch.de/wp-content/uploads/2020/04/24_Storch_FaudeKoivisto.pdf))

"Ressourcenaktivierung findet dann statt, so unsere Definition im Rahmen des
Zürcher Ressourcen Modells, wenn unbewusste Bedürfnisse und bewusste Motive zur
Deckung gebracht wurden." (Maja Storch und Tanya Faude-Koivisto, Ressourcen
aktivieren mit Mottozielen)

Ich lebe im Hier und Jetzt, dieser Raum und dieser Augenblick. Ich habe eine
Zukunft. "Welche Zukunft möchte ich?"

Clean Setup: Wie gelange ich von einem Ziel zu zielführenden Handlungen?

1. "Denke an ein Projekt, einen Workshop oder dieses Coaching."
1. "Damit dies wirklich hilfreich für dich ist, ist es wie was?" --> `m1` -->
   Entfalten
1. "Damit dies so sein kann (`m1`), musst du sein wie (was)?" --> `m2` -->
   Entfalten
1. "Damit dies so sein kann (`m1`) und du so sein kannst (`m2`), brauchst du
   was (von mir)?" --> Entfalten

Frage ich nach dem Wie, so frage ich nach der Art und Weise. Frage ich nach dem
Was, so frage ich nach einer Metapher, das heißt, meine Frage ist
Metapher-orientiert. Das Was am Ende betont auch die Aussage.

Übung (Clean Setup): Mit dieser Coaching-Fortbildung als "dies". Pro Schritt
stehen nur drei Minuten zur Verfügung pro Schritt um oberflächlicher zu sein
und dadurch andere Lernerfahrung zu machen. Bei Bedarf baue ich das PLZ-Modell
ein um von Antworten zu Wunschlandschaften zu finden.

Das Clean Setup kann ich zum Einstieg in ein Gruppenmeeting nutzen. Und ich
kann es zum Beginn einer Sitzung nutzen um die Seinsebene einzuladen oder um
eine Ausrichtung oder ein Mottoziel zu ergründen oder um die Seinsebene als
Ressource nach einer hypothetischen erfolgreichen Zielerreichung aufzubauen.

Coching-Perspektive: Ergebnisziele alleine reichen nicht. Im klassischen
Coaching suche ich ein zielführendes Verhaltensziel wie ein SMART-Ziel.
Verhaltensziele priorisieren das bewusste Verhalten und nutzen das
Unbewusstsein nicht wie Wertnehmung, Wahrgebung, Weltsortierung und so weiter.
Dies sorgt oft dafür, dass es mir schwer fällt meine Verhaltensziele
durchzuführen und dass ich sie früh abbreche.

Das [Zürcher Ressourcen Modell (ZRM)](https://zrm.ch) ist ein
Selbstmanagement-Training, dass Mottoziele über Ergebniszielen über
Verhaltensziele stellt. Ein Mottoziel frage nicht "Was muss ich tun?"
sondern "Wie muss ich sein?". Aus dem Sein entsteht die Handlung, ganz natürlich
und leicht. Ich handle aus meiner Haltung heraus. Wie bin ich während ich
handle? "People like us do things like this." Vorbilder sind kraftvoll: So sein
wie sie.

Ich erkenne meine Ziele an und lebe damit, statt erzwungen ziellos zu sein.

Übung:

1. "Denke an ein Projekt in deinem Leben."
1. "Wenn dieses Projekt ideal für dich läuft, dann ist das wie was?"
1. Mit dem PLZ-Modell Wunschzustand finden und entfalten.
1. Mit dem Clean Setup Mottoziel finden und entfalten.

"Wie kann ich oder diese Coaching-Sitzung dich dabei unterstützen das zu sein?"
ODER gezielt Mottoziel in dein Leben zu integrieren.

"Ich bin so" verändert Wahrnehmung, Gefühle und Handlung. "Was kannst du tun um
mehr so zu sein?" Du kannst dein Handeln nutzen um Seinszustände zu
manifestieren. Du kannst dein Bewusstsein nutzen um dein Unbewusstsein zu
aktivieren und für deine Mottoziele zu nutzen. Das kannst du tun durch
Verkörperung, Gemenschaft, Coaching und Erinnerung. Dein Handeln muss nicht
zielorientiert sein, sondern einfach dazu dienen einen neuen Seinszustand zu
etablieren. Beispielsweise kannst du Symbole, Bücher, Motive und so weiter in
deiner Wohnung platzieren, die täglich sichtbar sind und dadurch unbewusst
deinen Seinszustand beeinflussen. "Wie möchte ich sein?"

Wenn er:sie seinen:ihren Grundzustand als Okay erlebt, dann frage ich "Willst
du daran etwas verändern?"

Wie unterscheiden sich "Clean Setup" und das PLZ-Modell? Mit dem PLZ-Modell
finde ich einen Wunschzustand. Für diesen Wunschzustand finde ich eine
Metapher. Mit dem "Clean Setup" finde ich dafür ein Mottoziel. Die
entsprechende Haltung integriere ich in mein Leben.

Wunschzustände und Mottoziele sind ressourcenorientiert

Die "wie was"-Frage stelle ich nur einmal um nur eine Metapher zu
haben/entwickeln. Bei konsekutiven "wie was"-Fragen würde ausgehend von einer
bereits gefundenen Metapher eine neue entstehen. Das würde Verwirrung fördern.
Stattdessen bringe ich eine gefundene Metapher in den Körper.

## Implementierung/Integration

### Mottoziele

Landkarte: 1. Besonderen Raum eröffnen; 2. Wunsch/Ziel verkörpern; 3.
Mottoziel; 4. Implementieren

Was ist Coaching und wozu braucht es das? Es geht um die Entwicklung von
Menschen. Es geht darum, sie auf dem Weg zu ihrem Ziel oder ihrer Veränderung
zu begleiten. Dazu klären wir was das Ziel ist, überwinden die
Hürden/Herausforderungen da hin und finden einen gangbaren Weg. Als Coach bin
ich ein Wegbegleitungsexperte, das heißt, ich helfe und gehe wieder.

Zwei-Welten-Problem: Alltag vs. Sitzungen. Der Alltag ist komplex. Eine Sitzung
ist eine besondere Welt: Sie ist einfach/simpel, sie findet in einem anderen
Bewusstseinszustand statt und sie zieht die Körperintelligenz ein. Wie kann
er:sie ein Puzzleteil aus der Sitzung mit in seinen:ihren Alltag
bringen/mitnehmen?

Übung (den besonderen Raum eröffnen): Ich frage "Und was geschieht gerade, in
oder um, deinen Körper?" und entfalte seine:ihre Erfahrungen im Moment. Dabei
lege ich den Fokus auf sein:ihr Erleben statt auf Konzepte. Ich bleibe offen
und interessiert statt zu werten und zu interpretieren. Ich greife genau die
gesagten Worte auf.

Übung (ein Ziel entwickeln/verkörpern/erfahren):

1. Ich leite ihn:sie zum Ankommen dabei an sanft zu atmen.
1. Ich bitte ihn:sie an ein Projekt zu denken.
1. Ich frage "Und wenn dein Projekt genau so ist/läuft, wie du willst, ist es
   ... wie?" um ihn:sie Qualitäten für seinen:ihren Projekterfolg finden zu
   lassen.
1. Ich frage "Und während du daran denkst / damit verbunden bist, was geschieht
   gerade, in oder um deinen Körper?" um ihn:sie seinen:ihren Projekterfolg im
   Hier und Jetzt erleben zu lassen.

Interessante Frage: "Und wie verkörperst du dieses Gefühl?"

- Die Antwort nach dem Wie offenbart die eigentliche Motivation hinter dem
    Projekt und ist ein "so kann sich das anfühlen". Aus diesem Gefühl, kann es
    einfacher sein konkrete Handlungsschritte abzuleiten.
- Manche Menschen gehen von sich aus und kommen von dort zu einem
    (Lebens)projekt. Andere finden über ein Projekt zu sich. Je nachdem ist ein
    anderes Vorgehen notwendig.
- Die Erforschung/Entfaltung macht den Wunsch oder das Ziel lebendig in diesem
    Raum und in diesem Augenblick/Moment.

Auf die Frage "Wie, wenn so, wie du willst?" kommt ein Ergebnisziel als
Antwort: "So möchte ich, dass die Welt ist." Beispielsweise: "Ich möchte frei
meiner Neugierde folgen" oder "Unsere Firma wird zu einem Leuchtturm" oder
"Kann ich meine Familie versorgen". Offen ist was der:die Protagonist:in und
der:die Coach:in mit solch einem Ziel machen können, wenn es gefunden ist. In
der Metapher oder dem Bild zu einem Ergebnisziel steckt oft dessen Bedeutung.

Wie bringe ich ein Ergebnisziel in den Alltag? Meist indem ich ein
SMART-Verhaltensziel entwickle, dessen Umsetzung das Ergebnis herbeiführen
kann. Solche Ziele sind anstrengend und psychologisch knifflig. Sie führen oft
zu Druck, Selbstzweifel und Überforderung. Stattdessen kann ich ein Mottoziel
entwickeln, welches eine Haltung als Ziel hat statt eine Handlung: "Wie muss
ich sein um das Ergebnisziel zu erreichen?"

Mottoziel: "Ich bin so" verkörpern inspiriert Denken,
Verhaltensweise/Handlungen, Gefühlswelt und Wahrnehmungen.

Beispiele für Mottos/Haltungen:

- "Ich reiche kleinen Unternehmen meine Hand und ruhe in dem Willen meiner
    Kompetenz."
- "Ich weiß, dies wird ein Erfolg."
- "Ich stehe meinen Boden." oder "Ich bin ein unbeweglicher/standfester Baum."

Aus diesen Mottos heraus begegne ich Herausforderungen anders als ohne.

Mottoziele verbinden Sitzungs- und Alltagswelten. Sie führen zu Handlungen im
Alltag. Und sie bieten reichhaltige psychologische Möglichkeiten wie das
Mindset oder die Arbeit mit scheinbar widersprüchlichen Teilen.

Übung (Mottoziel entwickeln): "Damit dieses Projekt so sein kann musst du wie
sein (somatische Frage) / musst du sein wie was (Metaphernfrage)?"

Aus dem Sein entsteht Handeln. Wenn ich eine Rolle übernehme verhalte ich mich
entsprechend. Das Kohärenzprinzip besagt: Ich verhalte mich entsprechend dem
was ich glaube wer ich bin. Und das Priming-/Bahnungsprinzip besagt: Was ich
glaube wer ich bin ist stark dadurch beeinflusst woran ich gerade denke oder
davor gedacht habe. Bin ich mit dem Erleben oder der Metapher aus meinem
Mottoziel verbunden, so führt das zu bestimmten Verhaltensweisen. Ein
Verhalten, das zuvor schwer schien, ist/erscheint plötzlich einfach.

Ich baue immer wieder Mini-Psychoedukationen ein um ihm:ihr den Grund für mein
Vorgehen/Fragen zu erläutern wie "Hier kann es hilfreich sein ein Bild / eine
Metapher zu finden." oder "Für einen erfolgreichen Prozess entwickeln wir ein
Mottoziel, das heißt, eine innere Haltung, die dein Ziel im Außen begünstigt."
Darauf arbeite ich beständig, hartnäckig, offen, liebevoll und weich hin, wie
wenn ich einem kleinen Kind oder einem Welpen etwas erlebbar/beibringen möchte.

Arten von Sprache: Lernschritt: "Was ist meins und was nicht? Was ist meine
Wahrnehmung und was geschieht wirklich?" Clean Language hält eigene
Ansichten/Impulse/Manipulationen raus. Ich als Coach:in muss nichts wissen. Ich
begleite ohne etwas mit dem Erfahrenen zu machen. Ich bleibe offen. Als
Coach:in ist es sinnvoll sowohl anweisend/anleitend als auch Clean zu arbeiten
und alles dazwischen wie "Internal Family Systems"-Arbeit. Metaphern, die aus
dem System des:der Protagonist:in entstehen sind tiefer und funktionieren
besser als solche, die ich von außen hinein gebe. Ein Modell wie IFS macht
manches einfacher und anderes schwerer und kann dazu führen, dass wichtiges
übersehen wird, da die IFS-Brille dafür einen blinden Fleck hat. Für die
Entwicklung eines Ziels ist die Teilearbeit ungeschickt. Als Coach:in wähle ich
für jede Aufgabe ein sinnvolles Modell. Es ist wichtig für mich zu lernen: Wann
frage/begleite ich wie und wie wechsel ich fluide zwischen diesen verschiedenen
Arten.

### Implementieren/Integrieren

Implementieren bedeutet etwas aus der Sitzung in den Alltag zu bringen / im
Alltag anzuwenden: Was kann er:sie mitnehmen und wie kann ich das vereinfachen
und unterstützen? Dies benötigt eine direktere Sprache als Clean Language.

Was kann ich tun um im Alltag mehr so zu sein? Dabei bezieht sich "so" auf das
im Mottoziel entwickelte "Ich bin so".

- Priming/Bahnung: Unbewusst immer wieder an etwas erinnert werden oder in den
    Wunschzustand versetzt werden. Es kommt regelmäßig etwas ins Bewusstsein
    ohne das dafür etwas getan werden muss außer einmal. Beispiele: Bilder an
    der Wand, Düfte im Raum, Smartphone-Hintergründe, Aussagen an Spiegeln,
    fester Meditationsplatz, fester ungestörter Arbeitsplatz und so weiter. Da
    Geruchsinformationen ungefiltert ins Gehirn gehen, eignet sich Duft gut für
    Priming.
- Verkörperung: Bewegung, Berührung oder Körperhaltung, die an das Mottoziel
    erinnert, das heißt, die wachruft wie ich sein möchte. Das kann ein Ritual
    sein. Beispielsweise vor dem Schreiben um den Schreibtisch gehen.
- Handlungen: Bewusst herausfordernde und machbare Handlungen vollziehen, die
    die Veränderung begünstigen / näher bringen.
- Angewohnheiten: Minimale Veränderungen, die die große Veränderung begünstigt
    / näher bringt. "Wenn ..., dann ..."

Für die Implementierung ist ein anderer Modus notwendig. Dies drückt sich in
einer anderen Sprache in der Sitzung aus. Diese Sprache setzt ganz konkrete
enge Rahmen.

Als Coach:in leite ich die Implementierungsphase in der Sitzung mit einer
Psychoedukation ein: "Hier kann es helfen ..." und biete
Implementierungsmöglichkeiten an "Hier sind ein paar Möglichkeiten ...". Dann
lade ich ihn:sie ein eine konkrete Umsetzung finden: "Wie könntest du das tun?"
und "Was sind deine Ideen um das umzusetzen?".

Gute Momente um etwas ins Leben zu bringen sind Initialzündungen und wenn etwas
in Bewegung gekommen ist. In diesen Momenten lohnt es sich Primings zu finden.

Übung: Verkörperung finden, die 30 Sekunden dauert.

Wann nutzt du die Bewegung? Wie kannst du dich an die Bewegung erinnern? Wie
kann die Bewegung mit Dingen im Alltag verbunden/verquickt werden? "Was
geschieht dann (mit)?" (das Wort "mit" schafft eine Rückverbindung)

Iconics sind eine Mischung aus Vorstellung/Visualisierung und
Vorstellungskraft. Beispielsweise wenn ich gelassener sein möchte, dann stelle
ich mir vor, dass meine Beine im Meer schwimmen; oder wenn ich mit abgrenzen
möchte, dann imaginiere ich eine Blase oder Grenzen um mich herum. Iconics sind
verkörperte Erfahrungen gepaart mit visuellen Vorstellungen. Beispielsweise wie
in traditionellen Meditationstechniken. Über Iconics habe ich die volle
Kontrolle. Es ist etwas, das ich aktiv tun kann, das niemand anderes mitbekommt
und das mein Erleben wandelt/ändert.

Eine Veränderung im Leben zu etablieren, benötigt ein neues Verhalten/Handeln.
Ein Mottoziel kann automatisch zu neuem Handeln führen. Meist benötigt es
zusätzlich Überwindung zu schwierigen Handlungen.

"Was möchtest du? Wer musst du dafür sein? Welches Handeln zeigt dir, dass du
auf gutem Weg zum Ziel bist?"

Manchmal ist die leichteste Art mich zu überzeugen, dass ich mein Ziel
erreiche, wenn ich schwierige Schritte in die richtige Richtung tue. Dies führt
zu Commitment/Bindunge und zu "scheint mir etwas wert zu sein" im
Unbewusstsein, da ich bereits etwas investiert habe. Diese Schritte sollten
groß und herausfordernd sein. "Welchen Schritt kannst du gehen?" und "Was
brauchst du um diesen Schritt gehen zu können?"

Wie kann ich aktive Bewertungsschemata meine Systems (aus)nutzen? Mein
Unbewusstsein nimmt etwas wichtig, wenn ich dafür herausfordernde Handlungen
tue.

## Integration

Zusammenhangsloses Stichwort: State Dependent Memory

Zwei-Welten-Problem: Die Komplexität im Alltag ist größer als in Sitzungen.
Sitzungen sind eine besondere Welt. Welche Erkenntnisse, welches kleine und
konkrete Puzzlestück, kann der:die Protagonist:in mitnehmen? Wie kann ich sie
dabei unterstützen? Die Unterstützung bei der Integration benötigt einen
anderen Modus als jener der Entfaltung/Erforschung mit Clean Language, genauer
eine andere Kommunikation und einen anderen zwischenmenschlichen Umgang.

Die Komplexität des Alltags überschreitet oft die Erkenntnisse aus Sitzungen.
Der Alltag hat einen großen Sog in Form von Gewohnheiten, Rollen und so weiter.
Deswegen ist eine zielgerichtete Integrationsphase in Sitzungen notwendig.

Wo habe ich Integration bereits erlebt? Magie, Rituale, Affirmationen,
Psychotherapie und so weiter.

Wie entsteht nachhaltige Veränderung? Indem ich ein Thema bis zum Kern verstehe
oder indem ich kleine Alltagsänderungen umsetze? Das sind grundlegend
verschiedene Philosophien/Weltbilder. Oft werden kleine Erkenntnisse
unterschätzt und große Altris/Abtrs überschätzt. In Sitzung entsteht das
Verständnis über ein Thema. Und im Alltag müssen kleine Puzzelstücke umgesetzt
werden.

Fahrplan gemäß Handout 2.5:

1. Positive Veränderung `x`: "Was bemerkst du jetzt?" oder "Was ist jetzt
   anders in Bezug auf deinen Wunsch oder deine Herausforderung?" oder "Was hat
   sich gewandelt?" oder Rückbezug nehmen mit "Wenn ..., was geschieht mit
   ...?"
1. Mitnahme-Intention `y`: "Und wenn `x`, möchtest du davon etwas mitnehmen?"
   oder "Was davon möchtest du in deinen Alltag mitnehmen?"
1. Notwendige Bedingung `z`: "Und was braucht `y` damit es geschieht oder
   geschehen kann?" oder "Und was brauchst du um `y` mitnehmen zu können?" oder
   "Wie kannst du das schaffen?" oder "Und kann ...?" oder "Und kannst du
   ...?". Eine notwendige Bedingung bahnt die Umsetzung vor.
1. Handlungen:
   - Ideen von Protagonist:in: "Und was kannst du tun, um `y` oder `z`?"
   - Gemeinsame Ideen: "Was hat während der Sitzung geholfen, um `y` oder `z`?"
       oder "Was hat Veränderung/Wandel herbeigeführt?" oder "Was ist geschehen
       kurz bevor `y`?"
   - Ideen von mir, dem:der Coach:in: "Ist es Okay, wenn ich ein paar Ideen
       einbringe?"

Psychoedukation: Ich erläutere im Vorhinein die vier Phasen zur Integration.
Ich mache Moduswechsel/Übergang von Clean zu Integration im Verlauf der Sitzung
explizit / bringe sie in Kontakt. Die verschiedenen Modi sind
Körperintelligenz/symbolisches Forschen, Integration, Übung, Mini-Vereinbarung
und Psychoedukation.

Selbstermächtigend ist, wenn ich das was ich in einer Sitzung finde mir selbst
außerhalb in meinem Leben schaffe.

Übung:

1. Die Antwort auf die Frage "Was machst du in deinem Leben, das dir gut tut?"
   entfalten.
1. Psychoedukation anbieten.
1. Eine Mitnahme-Intention `y` herauskristallisieren.
1. Psychoedukation anbieten "Es kann sinnvoll sein zu erörtern wie du `y` mehr
   in deinen Alltag bringen kannst. Möchtest du das mit mir herausfinden?"
1. Notwendige Bedingung `z` finden.
1. Handlungen finden.

Je kleinschrittiger und niedrigschwelliger Handlungen sind um so
wahrscheinlicher ist deren Umsetzung. Beispielsweise möchte ich Atemarbeit in
mein Leben integrieren, indem ich immer wieder "Ocean Breath" im Alltag
praktiziere, was mir hilft in den Moment zu kommen und mein Tun zu
verlangsamen.

Kommen immer wieder neue Themen auf, dann diese entweder vertagen oder
kleinschrittiger machen. Ich vertage ein Thema beispielsweise indem ich es
einen Teil nenne, ihm einen Namen gebe, ihn irgendwo platziere und dann nicht
weiter betrachte.

Bei dem Versuch konkret zu werden, kommt es oft zu inneren Knoten und
Widerständen. Wie kann der Rahmen gemeinsam so gestaltet werden, dass etwas
konkretes am Ende entsteht, das mitgenommen werden kann?

Intention vorher abstecken: Geht es in der Sitzung darum, dass der:dir
Protagonist:in sich offen mitteilt oder darum dass er:sie etwas konkretes
mitnimmt?

Wann versuche ich ein Integration? In Sitzungen, in denen sich etwas in ihm:ihr
wandelt. Ich gebe dem:der Protagonist:in dadurch Verantwortung, dass ich
ihn:sie einlade etwas selbständig im Alltag umzusetzen. In der darauffolgenden
Sitzung frage ich danach. Veränderungen kommen nur in sein:ihr Leben, wenn
er:sie diese aktiv umsetzt.

Am Ende einer Sitzung sollte er:sie in einem Zustand sein, in der er:sie in den
Alltag entlassen werden kann. Die Integrationsphase betrachtet ihn:sie vor dem
Ende als selbstwirksames Wesen/Individuum, das Erkenntnisse aus der Sitzung
aktiv in sein:ihr Leben bringen kann in Form kleiner konkreter
Handlungsschritte. Die Handlungsschritte sind zusätzlich zu der passiven
Mitnahme innerer Veränderungen/Perspektivwechsel, die während der Sitzung
geschahen.

In der Integrationsphase ist er:sie auf sich zurückgeworfen: Jetzt hängst es
von ihm:ihr ab. Eine Zwischenintegration oder natürliche Integration während
der Sitzung geschieht durch längeres bewusstes Sein mit dem neuen inneren
Zustand.

Übung (Verkörperung): Ich frage "Was möchtest du mehr in deinem Leben haben?".
Auf die Antwort biete ich Psychoedukation an "Eine Sache, die hilfreich sein
kann sind kleine Verkörperungsübungen, die wach rufen, was du mehr haben
möchtest." und frage danach "Wie wäre es gemeinsam danach zu suchen?"

## Ganze Sitzung

Problemlandschaft --> PlZ --> Wunschlandschaft --> Rückbezug

1. Problemlandschaft: "Was möchtest du erforschen?" (welches Problem) -->
   Problem --> "Und während du an _Problem_ denkst, was geschieht in oder um
   deinen Körper?" --> verkörperte Landschaft
1. PLZ-Modell --> Wunschzustand
1. Wunschlandschaft: Ich erforsche den Wunschzustand im Hier und Jetzt, lasse
   ihn wach werden und hole ihn in den Körper. Dazu frage ich "Und wenn
   _Wunschzustand_, gibt es noch etwas über das?" --> verkörperte Landschaft
1. Rückbezug: "Und wenn all das (_Wunschlandschaft_), was geschieht mit
   _Problem(landschaft)_?"

Wenn ein "Dammbruch" in Form eines Wortschwalls passiert, dann kann das
bedeuten, dass gerade ein innerer Perspektivwechsel geschehen ist, die
Innenwelt sich neu sortiert und ich kann fragen "Und wenn all das, was
geschieht dann, in oder um deinen Körper?" Das ist auch eine gute Frage, wenn
ich die Orientierung verloren habe oder einfach zu viele Worte gesprochen
wurden, die ich nicht erinnern kann.

Wirkt das Problem groß und der Wunschausschnitt klein, so mag sich das unstimmig
anfühlen. Ich bleibe dennoch offen. Ich weiß nicht was bedeutsam ist. Ein
kleiner Ausschnitt kann ein Rattenschwanz von etwas großem sein.

Wenn der:die Protagonist:in durch eine meiner Fragen vom Körper in den Kopf
kommt, dann navigiere ich mehrere Schritte zurück und versuche einen anderen
Weg.

Wenn bei der Erforschung der Wunschlandschaft ein Problem aufkommt, dann
erkenne ich dieses an, beleuchte es kurz und begleite zurück zur
Wunscherforschung.

Damit die Sitzung nicht zu konzeptionell/begrifflich/sprachlich/philosophisch
wird, verankere ich gehörtes immer wieder im Körper. Damit bringe ich das
Erfahren/Erleben des:der Protagonist:in konkret in seine:ihre Innenwelt und
behalte es psychoaktiv in dem Sinne, dass es viszeral wahrgenommen wird. Ich
begleite psychoaktiv statt philosophisch, durch meine Art in Kontakt zu gehen
und Fragen zu stellen. Mein Ziel dabei ist es Raum für Kommunikation zwischen
dem:der Protagonist:in und dem Problem zu schaffen.

## Ganze Sitzung mit (interaktiven somatischen) Übungen

Problem --> PLZ --> arbeitsfähige Ausrichtung --> Psychoedukation -->
Interaktive somatische Übung(en) --> Wunschlandschaft --> Rückbezug

Grundannahme: Wenn der:die Protagonist:in der Verkörperung ihres Wunschzustands
näher kommt, dann ist das hilfreich.

Übung (Verkörperung): Struktur für Edukation zum Einstieg in eine Übung

1. Geteilte Interpretation/Realität: "... Stimmst du zu?"
1. Fragen ob Edukation willkommen ist: "Möchtest du eine Perspektive dazu
   hören?"
1. Theorie erläutern: "Im ... Modell gilt ..." oder "Für die Technik ... gilt
   ..."
1. Meinung erfragen: "Und mit dem ... Wie ist das für dich?"
1. Resonanz erfragen: "Möchtest du das weiter erforschen?"
1. Übung anleiten

Wenn die erste Übung zur Verkörperung des Wunschzustands nicht funktioniert,
dann eine andere versuchen/anleiten.

Eine Übung ist ein Türöffner in einen anderen Zustand.

Ich kann Übungsimpulse von außen hinein geben gemäß meiner Wahrgebung und
meines (Erfahrungs)wissens. Und ich kann Übungsimpulse des:der Protagonist:in
aufgreifen. Ersteres kann zu neuer Referenzerfahrung führen und letzteres zu
tiefer Erkenntnis. Beispielsweise kann ich eine Bewegung, eine Geste, eine Mime
oder eine Haltung, die beim Entfalten aufkam, langsamer oder schneller
ausführen lassen und mit einer speziellen Art zu atmen verknüpfen.

Ist der Impuls des:der Protagonist:in verbal und abstrakt, beispielsweise
"rühren", so kann ich fragen "Hast du eine Idee, wie du mit einem Körperteil
rühren kannst?"

## Teilearbeit

Bekanntes Beispiel: Arbeit mit inneren Kindanteilen.

Was ist dabei gefährlich/problematisch? Es kann zu einer Trennung, Teilung,
Fragmentierung und Polarisierung im System kommen. Es wird das vernachlässigt,
was alles halten kann. Das System in sich kann immer
getrennter/fragmentierter/zusammenhangsloser werden.

Ich vermeide es mich mit einem meiner inneren Anteile zu identifizieren, indem
ich gleichzeitig mit mehreren Teilen bin, indem ich Abstand zu diesem Teil
finde, beispielsweise durch Platzierung im Raum, indem ich einen
Perspektivwechsel durchführe, beispielsweise durch Änderung meiner
Blickrichtung oder Stehen auf einen Stuhl oder Liegen auf den Boden, und indem
ich über den Teil schreibe.

## Somatische Übungen (Pendeln zwischen Clean und Anleiten)

Was ist Coaching? Wofür kommen Protagonist:innen? Sie möchten ein Problem
lösen, ihre Ressourcen erweitern, ihre Fähigkeiten entwickeln, ihre Wünsche
verwirklichen und sie schaffen das nicht alleine, vielleicht weil innere
Anteile einander im Weg stehen (Teilearbeit ist der Übergang von Coaching zu
Therapie). Als Coach:in begleite ich den:die Protagonist:in dabei neue
Perspektiven einzunehmen und neue Handlungen zu etablieren.

Ich beziehe eine größere Weisheit mit ein, die Körperintelligenz oder die
rechte Hemisphäre des:der Protagonist:in über verkörperte Erfahrung, dem
sogenannten "Felt Sense". Das ist ein inneres Gespür, das alles umfasst, was
die Körperintelligenz über etwas weiß und dieses Wissen präsentiert es auf
einmal. Um diese verdichtete Information kognitive verstehen zu können
entfalten wir den "Felt Sense" nach und nach mit Clean Language. Dabei
entwickel ich die Grundfähigkeit der Eigenkörperwahrnehmung des:der
Protagonist:in, eine implizite und notwendige Aufgabe, wegen der
Protagonist:innen eigentlich nicht kommen.

Wie hole ich Protagonist:innen ab, die keinen Zugang zu ihrer Innenwelt haben?
Solche Menschen können keine Entscheidungen mehr treffen, da diese aus dem
Körper kommt, bewusst oder unbewusst. Da ihnen der Zugang zum Bauchgefühl
fehlt, bekommen sie für sich relevantes nicht mit, wie ob sie ihre Situation
gerade als sicher oder unsicher erleben, die sogenannte Neurozeption.

Wenn Protagonist:innen an den Rand ihrer Sprache kommen, wenn sie in ihrer
Selbstmitteilung stocken oder stolpern und wenn sie interessiert daran bleiben
was da ist, dann entsteht Erkenntnis, sortiert/organisiert sich die innere
Landkarte neu --- ein Aha-Erlebnis.

Ich setze den Rahmen so klein, dass der:die Protagonist:in sich auf die
verkörperte Erfahrung einlassen kann (statt sich vor Angst davor nicht hinein
zu trauen). Für viele ist das eine neue Art sich zu erfahren / wahrzunehmen.
Langfristig eröffnet sie ein Tor zu mehr Informationen durch Körperimpulse,
Körpergespüre, innere Bilder und so weiter und damit zu Veränderung. Am Ende
des Weltbildes, also am Rand der bekannten Welt oder der inneren Landkarte,
kommt eine neue verkörperte Erfahrung, eine Erkenntnis aus der Landschaft, die
das Weltbild verwandelt oder die innere Landkarte erweitert. Das ist ein
holistischer/ganzheitlicher Erkenntnisprozess.

Wie stecke ich den Rahmen ab? Ich navigiere zwischen den Extremen Folgen und
Führen. Beim Folgen ist der Rahmen weit, indem ich offene "cleane" Fragen
stelle. Beim Führen ist der Rahmen eng, indem ich konkrete Anweisungen gebe
/ anleite. Diese beiden Grundfähigkeiten benötige ich für einen flüssigen und
natürlichen Behandlungsverlauf. Ist die Innenwahrnehmung wenig ausgebildet,
dann kommt Folgen rasch an seine Grenzen.

Folgen (bottom up): Ich lausche dem Körper des:der Protagonist:in, sammele
Informationen aus seinem:ihrem System, gebe der Körperintelligenz Raum sich
mitzuteilen und gebe so wenig als möglich von außen hinein.

Führen (top down): Ich setze dem Körper des:der Protagonist:in einen klaren
Rahmen, gebe Informationen in sein:ihr System, gebe Impulse beispielsweise mit
den Worten "Das könnte helfen ..." und probiere mit ihm:ihr aus ob es hilft.

Psycho-Edukation: Was mache ich? Ich greife Impulse auf. Dadurch lade ich Ideen
des:der Protagonist:in ein.

Übung (interaktive somatische Übung --- die Übergänge macht sie interaktiv):
Ich führe durch Anleiten einer Atemübung, Yogaübung oder anderes und eröffne
danach den Raum zum Spüren / In-Sich-Ruhen und entfalte dann die
Eigenwahrnehmung mit "Und wenn du in dir ruhst, was geschieht dann, in oder um,
deinen Körper?" Danach beginne ich von vorne. So pendel ich zwischen üben und
entfalten. Je nach Fähigkeiten des:der Protagonist:in ist die Entfaltungsphase
länger oder kürzer.

Beispiel (Übung zur Regulation): Ich entfalte den Status quo mit der
Eingangsfrage "Wie ist es jetzt?" Dann leite ich eine Übung an. Abschließend
entfalte ich den Zustand mit "Wie ist es jetzt?"

Eine Übung kann alles sein, vom Spüren einer Hand auf einer Schulter bis zur
Durchführung einer postisometrischen Relaxation (PIR-Technik).

Pendel ich spielerisch und rasch, so bringt das Leichtigkeit. Stecke ich den
Übungsrahmen klar ab, so schenkt das Geborgenheit. Kommt beim Üben oder
Entfalten etwas bei dem:der Protagonist:in auf, beispielsweise eine Bewegung,
so greife ich seinen:ihren Impuls auf und gestalte daraus eine Übung.

Beispielrahmen: Ich leite in jeder Sitzung zu Beginn oder zu Beginn einer
Behandlungssequenz eine Handvoll Übungen an. Dies kann dem:der Protagonist:in
helfen anzukommen. Mir gibt es Informationen über seine:ihre Tagesform und
darüber was er:sie gut kann und was herausfordernd ist.

Bei einer geübten Protagonist:in entfalte ich während er:sie eine Übung
ausführt. Das ist herausfordernd, da zwei Sachen gleichzeitig zu tun sind. Bei
ungeübten, entfalte ich im Anschluss und pendele zwischen Üben und Entfalten.
Ein Vorteil beim Entfalten während der Ausführung ist, dass ich die Übung in
Echtzeit anpassen kann um die Erfahrung zu vertiefen.

Wann entscheide ich mich für Folgen und wann für Führen?

1. Der:Die Protagonist:in bringt ein Thema: Ein Problem, einen Wunsch oder eine
   Frage.
1. Als Coach:in entscheide ich mich für eine Ausrichtung: Das nächste Ziel oder
   der nächste Schritt. Beispielsweise Problementfaltung oder PLZ-Anwendung
   oder der Energie folgen.
1. Wir setzen es gemeinsam um: Mit einem genauen Setting und einer passenden
   Sprache. Beispielsweise Folgen oder Führen.

Gehe ich mit der Energie des:der Protagonist:in mit, so greife ich seine:ihre
Impulse auf. In seiner:ihrer Energie steckt etwas, aus dem was zu lernen ist.
Wandelt sich die Energie durch die Durchführung einer von seinem:ihrem Impuls
inspirierten Übung, so lade ich ein das Ursprungsproblem mit dieser neuen
Energie abermals zu betrachten. Wie stellt es sich jetzt dar?

Eine Ausrichtung ist wie ein Vektor: Ständig wähle ich den nächsten Vektor, der
uns mit dem gegenwärtigen Wind näher zum Ziel bringt, wie die Zickzackfahrt
eines Segelschiffs. Die perfekte Vereinbarung bringt uns direkt zum Ziel und
ist sehr schwer zu finden. Dem Prozess vertrauen mit arbeitsfähigen
Mini-Vereinabrungen (Ausrichtungen) bringt uns nicht direkt zum Ziel und ist
einfacher umzusetzen. Beides zu können ist wichtig und beides hat seinen Platz.
Als Coach:in richte ich mich am nächsten Schritt aus, der für mich hilfreich
scheint.

Übung:

1. Rahmen erklären. "Okay?"
1. In anweisender Sprache bitte ich den:die Protagonist:in Kraft in den Boden
   zu geben.
1. Ich entfalte mit "Und was geschieht gerade in oder um deinen Körper?" und
   "Und jetzt, was geschieht ...?" und "Und nach dieser Übung, was geschieht
   ...?"
1. In einladender Sprache bitte ich den:die Protagonist:in Laufbewegungen
   durchzuführen.
1. Wie zuvor entfalte ich.

Wie entscheide ich welche Impulse des/der Protagonist:in ich aufgreife oder
welche Übungen ich aus mir heraus vorschlage? Meine Intentionen für Übungen
werden informiert durch das was ich wahrnehme und wie ich es interpretiere.
Durch Erfahrung bilde ich meine Intuition aus. Ich habe bereits bestimmte
Interpretationsmuster, die ich mir bewusst mache um mich von ihnen nicht
verführen zu lassen. Intentionen für Übungen sind beispielsweise Ankommen,
Regulieren, Experimentieren und Nähren.

Übung (Regulation): Ich fasse die Intention "Regulieren" als
Ausrichtungsvektor.

1. Ich setze den Rahmen und frage "Okay?"
1. In anweisender Sprache leite ich eine klare sich wiederholende Bewegung an.
1. Ich entfalte dabei aufkommendes und schätze daraus den autonomen
   Nervensystemzustand ein.
1. Je nach Zustand leite ich in einem passenden Sprachmodus eine Bewegung an.
1. Abermals entfalte ich und schätze den Zustand ein.

Übung (Impuls):

1. Ich setze den Rahmen und frage "Okay?"
1. Ich bitte ihn:sie einen Wunsch oder eine Herausforderung zu beschreiben.
1. Ich greife einen dabei aufgekommenen Bewegungsimpuls auf und bitte darum
   diesen langsam zu wiederholen.

Ich kann nicht wissen wie etwas auf ihn:sie wirkt. Statt zu raten frage ich
einfach nach: "Und wie ist das für dich?"

Gehe ich mit dissoziierten Teilen von ihm:ihr in Verbindung, so kann das
heilsam sein.

Entwicklungsmodell bezüglich Körperwahrnehmung (inspiriert von [Wendy Sullivan
und Paul Field](https://www.cleanchange.co.uk/)): Prozess und Fähigkeit des:der
Protagonist:in von nur im Kopf bis ganzheitliche umfassende Wahrnehmung. Indem
ich passende Rahmen setze begleite ich angepasst an das Entwicklungsstadium. Wo
befindet sich der:die Protagonist:in auf der Skala von sie nimmt sich gar nicht
wahr, sie nimmt einen Bereich wahr, sie kann einer sich wandelnden Empfindung
über die Zeit hinweg folgen, sie kann das mit mehreren Empfindungen
gleichzeitig, bis hin zu sie nimmt ihren ganzen Körper (und Geist) gleichzeitig
wahr.

Ich habe zur Verfügung die

- Werkzeuge: Übungen anleiten, Fragen stellen und berühren;
- Sprache: Anleitend, Optionen gebend und offen fragend;
- Dauer: kurz & explizit oder lang & offen.

## Schwellenübergänge & Initiationen

Thresholds & Riten; Lebensveränderungen, Transformationen

Übergänge sind vorhersehbar. Umbrüche sind unvorhergesehen wie Unfälle und
Katastrophen. Retrospektiv können Umbrüche als Übergänge betrachtet werden. Oft
ist unbekannt wie Übergänge aussehen und wie das Leben danach ist.

Wie gestalte ich eine Sitzung über Schwellenübergänge?

Übergänge sind normal in dem Sinne, dass sie ständig stattfinden, von kleinen
bis zu großen. Beispiele dazu sind Geburten, Kindergartenbeginn und -ende,
Einschulung und Schulende, Pubertätsbeginn und -ende, Berufsbeginn und -ende,
Menopause, Renteneinstieg, Tod und so weiter. Damals wurden Übergänge
zeremoniell/rituell begangen und gefeiert. Heute geschehen sie nebenher. Das
macht sie schwer zu (be)greifen.

Austausch: Welche Übergänge habe ich erlebt? Gab es dabei
Riten/Rituale/Zeremonien?

Manche Übergänge von damals/früher werden heute noch zelebriert ohne welche zu
sein im Erleben wie Kommunion, Konfirmation oder Volljährigkeit. Dies sind zwar
offiziell Übergänge, sie verändern jedoch die Lebensrealität nicht.
Beispielsweise wechseln Menschen bei Erreichen der Volljährigkeit im
Allgemeinen nicht aus ihrem Elternhaus aus oder haben dies bereits getan.
Umgekehrt gibt es Übergänge, die als solche erlebt werden und die nicht
zelebriert werden wie der Beginn der Periode oder der Menopause oder die
monatlichen Blutungen.

Übergänge, die die Lebenswirklichkeit verändern gehen tiefer als andere wie ein
Studienbeginn mit Umzug.

Übung (Übergang): Was für ein Übergang liegt vor dir? Platziere diesen im Raum.
Nutze deine Eigenbewegung um ihn zu verkörpern. Bilde einen Zeitstrahl, indem
du einen Zeitpunkt davor und einen danach im Raum platzierst. Erlebe über
Bewegung durch den Raum den Übergang hier und jetzt in dir.

1. Ich setze den Rahmen, indem ich die Übung erkläre.
1. Ich sage "Ich lade dich ein an einen Übergang zu denken, der vor dir
   liegt, einen Namen dafür zu finden und ihn mir mitzuteilen." Auf die Antwort
   `n` sage ich "Markiere `n` mit einem Zettel mit Namen oder einem passenden
   Gegenstand auf dem Boden."
1. Ich sage "Finde einen Zeitpunkt davor." und "Stelle dich dort hin." und
   "Finde einen Namen dafür." und "Markiere ihn auf dem Boden". Dasselbe
   wiederhole ich für einen Zeitpunkt danach.
1. Ich lade nacheinander ein "Begebe dich ..."

   - "... zu dem Zeitpunkt davor."
   - "... zum Übergang."
   - "Gehe über den Übergang."
   - "... zu dem Zeitpunkt danach."
   - "Bewege dich frei durch den Raum." (beispielsweise entlang des Zeitstrahls
       oder mit Abstand dazu ihm zu oder abgewandt)

   In jeder Position frage ich "Was geschieht gerade?" und "Was weißt du von
   hier?" (kognitiv) und "Was ist anders?" und "Was ist gleich?". In den
   einzelnen Positionen lade ich bei Bedarf dazu ein mit der Blickrichtung zu
   spielen.

Die vorangegangen Übung enthält Aspekte eines "Clean Space"-Prozesses.

Was definieren wir und was nicht? Es benötigt Raummarkierungen zur
Orientierung. Oben definieren wir einen Zeitpunkt davor, die Schwelle und einen
Zeitpunkt danach.

Bis der Raum sich entfaltet und Erkenntnisse offenbart benötigt es mehr
Vorbereitungszeit und Vorlaufzeit als Prozesse ohne Raum.

Ein möglichst neutraler Raum ist sinnvoll, da das Definieren von Zeitpunkten
alles was im Raum ist mit Bedeutung auflädt/belädt.

Ein Höhenwechsel durch Stehen auf einen Stuhl oder eine Leiter bringt eine
tiefere verkörperte Erfahrung. Dies kann sinnvoll sein um einen
Perspektivwechsel durchzuführen oder um die Metapher "Berg erklimmen" oder
ähnliches zu erleben.

Als Markierungen dienen selbstbeschriebene Blätter verschiedener Größen
(neutral) oder wesenhafte Figuren (anthropologisierend) oder Nutzgegenstände
(sinnbildlich) oder Bauten aus verschiedenen Dingen (bedeutungsgebend).

Das Räumlich erleichtert Externalisierung und Abstandnehmen. Beispielsweise,
wenn in einem Prozess die Einnahme einer neuen Perspektive herausfordernd ist,
dann kann ich einladen dies räumlich zu tun. Dabei kann auch zu viel Distanz
entstehen.

Die Arbeit mit dem Raum kann so spielerisch werden, dass sie an Tiefe verliert.

Ähnliche Methoden werden unter anderem in der Pesso-Therapie (Pesso Boyden
System Psychomotor) und in der Gestalt-Therapie verwendet.

## Polyvagaltheorie

Aus Modul 2

_Übung_ (Das eigene Nervensystem erfahren):

1. "Wo möchtest du sein? Und wo möchtest du, dass ich bin?"
1. Setting erklären: "Ich möchte mit dir zwei Erlebnisse nacheinander
   erforschen, eines mit "Social Engagement" (grüner) Nervensystemaktivierung
   und eines mit "Kampf oder Flucht" (orangener) Aktivierung, für beide jeweils
   ein Wort, eine Geste oder eine Mime finden, und danach deren Beziehung
   erforschen und zwischen beiden pendeln. Ist das okay für dich?"
1. Rahmen setzen: "Dafür unterhalten wir uns verbal und ich nutze Clean
   Language als Werkzeug."
1. In Achtsamkeit und voll Interesse begleiten
1. Experiment durchführen TODO Welches Experiment?

Grün ist ventrovagale, orange ist sympathische und rot dorsovagale Aktivierung.
Lerne zwischen diesen Aktivierungsniveaus zu unterscheiden.

Vertraue in den Prozess
