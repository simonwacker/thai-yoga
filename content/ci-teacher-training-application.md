---
title: CI Teacher Training
layout: single
---

# Motivation

I experience Contact Improvisation as profoundly healing through its physical, social, emotional, mental, and energetic challenges and joys. It gives me space to express myself through my body, to bring that unique expression into contact with other people, to experience the expressions of others, to communicate with each other through our bodies, to find some common flow with or without physical contact, and to express my boundaries and accept those of others. For me it is inspiring and humbling, simple and challenging at the same time. It gives me space to experience interpersonal contact in a more primal, natural, and immediate way beyond the verbal contact so omnipresent in day-to-day life. I would like to hold such spaces for others to have similarly healing experiences, have a good time in their bodies together with others, and experience what being human also means besides the everyday hustles. I once held such a space in a clinical context, I dearly enjoyed the experience and got very generous and motivating feedback from the participants. Since that gratifying evening I regularly observe the thought emerging that I'd like to hold such spaces. With my limited experience in dancing in general though, I don't feel like just doing it, as I don't know if I'd be able to hold a safe space and give proper guidance where necessary. That's why I'm looking for some sort of training that hopefully prepares me for my wish to come true.
